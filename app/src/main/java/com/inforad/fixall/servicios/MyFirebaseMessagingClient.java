package com.inforad.fixall.servicios;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.inforad.fixall.R;
import com.inforad.fixall.channel.NotificacionHelper;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.Mensaje;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.reciver.AceptReciver;
import com.inforad.fixall.reciver.mensajeReciver;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

public class MyFirebaseMessagingClient extends FirebaseMessagingService {

    private static final int NOTIFICATION_CODE = 100;
    public static final String NOTIFICATION_REPLAY = "Notification_replay";

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Map<String, String> data = remoteMessage.getData();
        String titulo = data.get("titulo");
        String contenido = data.get("contenido");


        if (titulo != null){
            switch (titulo){
                case "CODIGO DE VERIFICACIÓN":
                    showNotification(titulo, contenido);
                    break;
                case " NUEVO MENSAJE ":
                    showNotificationMensaje(data);
                    break;
                case "NUEVA OFERTA":
                    showNotificationOferta(data);
                    break;
                case "OFERTA ACEPTADA":
                    String imgEmpresario = data.get("imagen");
                    showNotificacionIcon(titulo, contenido, imgEmpresario);
                    //showNotificationAceptarOferta(titulo, contenido, imgEmpresario);
                    break;
                case "BUSQUEDA":
                    String imgEmp = data.get("imagen");
                    showNotificacionIcon(titulo, contenido, imgEmp);
                    //showNotificacionBusqueda(titulo, contenido, imgEmp);
                    break;
                case "CALIFICACIÓN RECIBIDA":
                    String i = data.get("imagen");
                    showNotificacionIcon(titulo, contenido, i);
                    break;
                case "PROBLEMA CON EL SERVICIO":
                    String im = data.get("imagen");
                    showNotificacionIcon(titulo, contenido, im);
                    break;
                case "SERVICIO PAGADO":
                    String img = data.get("imagen");
                    showNotificacionIcon(titulo, contenido, img);
                    break;
            }
        }
    }

    private void showNotificacionIcon(String titulo, String contenido, String img){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Picasso.with(getApplicationContext()).load(img).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bmImg, Picasso.LoadedFrom from) {
                        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
                        NotificationCompat.Builder builder = notificacionHelper.getNotificationIcon(titulo, contenido, bmImg);
                        Random random = new Random();
                        int n = random.nextInt(10000);
                        notificacionHelper.getManager().notify(n, builder.build());
                    }
                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {}
                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {}
                });
            }
        });
    }

    private void showNotificationOferta(Map<String, String> data) {
        String titulo = data.get("titulo");
        String contenido = data.get("contenido");
        String ofertasJson = data.get("ofertas");
        String imgProfesional = data.get("imgProfesional");
        Gson gson = new Gson();
        Oferta[] ofertas = gson.fromJson(ofertasJson, Oferta[].class);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Picasso.with(getApplicationContext()).load(imgProfesional).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmapProfesional, Picasso.LoadedFrom from) {
                        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
                        NotificationCompat.Builder builder = notificacionHelper.getNotificationOferta(ofertas, titulo, contenido, bitmapProfesional);
                        Random random = new Random();
                        int n = random.nextInt(10000);
                        notificacionHelper.getManager().notify(n, builder.build());
                    }
                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {}

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {}
                });
            }
        });

    }

    private void showNotificationAceptarOferta(String titulo, String contenido, String imgEmpresario){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Picasso.with(getApplicationContext()).load(imgEmpresario).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bmEmpresario, Picasso.LoadedFrom from) {
                        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
                        NotificationCompat.Builder builder = notificacionHelper.getNotificationAceptarOferta(titulo, contenido, bmEmpresario);
                        Random random = new Random();
                        int n = random.nextInt(10000);
                        notificacionHelper.getManager().notify(n, builder.build());
                    }
                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {}
                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {}
                });
            }
        });
    }

    private void showNotificacionBusqueda(String titulo, String contenido, String imgEmpresario) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Picasso.with(getApplicationContext()).load(imgEmpresario).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bmEmpresario, Picasso.LoadedFrom from) {
                        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
                        NotificationCompat.Builder builder = notificacionHelper.getNotificationAceptarOferta(titulo, contenido, bmEmpresario);
                        Random random = new Random();
                        int n = random.nextInt(10000);
                        notificacionHelper.getManager().notify(n, builder.build());
                    }
                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {}
                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {}
                });
            }
        });
    }

    private void showNotification(String titulo, String contenido){
        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
        NotificationCompat.Builder builder = notificacionHelper.getNotification(titulo, contenido);
        Random random = new Random();
        int n = random.nextInt(10000);
        notificacionHelper.getManager().notify(n, builder.build());
    }

    private void showNotificationMensaje(Map<String, String> data){
        String titulo = data.get("titulo");
        String contenido = data.get("contenido");
        String aliasEnvia = data.get("aliasEnvia");
        String aliasRecive = data.get("aliasRecive");
        String imgEnvia = data.get("imgEnvia");
        String mensajesJson = data.get("mensajes");
        String idEnvia = data.get("mensajes");
        String idRecive = data.get("idRecive");
        String idChat = data.get("idChat");
        int idNotificacion = Integer.parseInt(data.get("idNotificacion"));
        Intent intent = new Intent(this, mensajeReciver.class);
        intent.putExtra("idEnvia", idEnvia);
        intent.putExtra("idRecive", idRecive);
        intent.putExtra("idChat", idChat);
        intent.putExtra("idNotificacion", idNotificacion);
        intent.putExtra("aliasEnvia", aliasEnvia);
        intent.putExtra("imgEnvia", imgEnvia);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        RemoteInput remoteInput = new RemoteInput.Builder(NOTIFICATION_REPLAY).setLabel("Tu Mensaje...").build();
        NotificationCompat.Action accion = new NotificationCompat.Action.Builder(R.drawable.logo, "Responder", pendingIntent).addRemoteInput(remoteInput).build();
        Gson gson = new Gson();
        Mensaje[] mensajes = gson.fromJson(mensajesJson, Mensaje[].class);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Picasso.with(getApplicationContext()).load(imgEnvia).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmapEnvia, Picasso.LoadedFrom from) {
                        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
                        NotificationCompat.Builder builder = notificacionHelper.getNotificationMensaje(mensajes, aliasEnvia, aliasRecive, bitmapEnvia, accion);
                        notificacionHelper.getManager().notify(idNotificacion, builder.build());
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
            }
        });
    }

/*
    private void verNotificacionSimple(String titulo, String contenido){
        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
        NotificationCompat.Builder builder = notificacionHelper.getNotificationSimple(titulo, contenido);
        Random random = new Random();
        int n = random.nextInt(10000);
        notificacionHelper.getManager().notify(n, builder.build());
    }

    private void showNotification(String titulo, String contenido) {

        PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
        Uri sonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
        NotificationCompat.Builder builder = null;
        Random random = new Random();
        int n = random.nextInt(10000);
        try {
            builder = notificacionHelper.getNotificationOldApi(titulo, contenido, intent, sonido);
            notificacionHelper.getManager().notify(n, builder.build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showNotificationAccion(String titulo, String contenido, String idUsuario, String imagen) {

        Intent aceptarIntent = new Intent(this, AceptReciver.class);
        aceptarIntent.putExtra("idUsuario", idUsuario);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, NOTIFICATION_CODE, aceptarIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Action AceptarAccion = new NotificationCompat.Action.Builder(R.mipmap.ic_launcher, "ACEPTAR", pendingIntent).build();

        //PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
        Uri sonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
        NotificationCompat.Builder builder = null;
        try {
            builder = notificacionHelper.getNotificationOldApiAccion(titulo, contenido, sonido, AceptarAccion, imagen);
            notificacionHelper.getManager().notify(2, builder.build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotificationApiOreo(String titulo, String contenido, String imagen) throws IOException {
        PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
        Uri sonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
        Notification.Builder builder = notificacionHelper.getNotification(titulo, contenido, intent, sonido);
        notificacionHelper.getManager().notify(1, builder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotificationApiOreoAccion(String titulo, String contenido, String idUsuario, String imagen) {
        Intent aceptarIntent = new Intent(this, AceptReciver.class);
        aceptarIntent.putExtra("idUsuario", idUsuario);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, NOTIFICATION_CODE, aceptarIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Action AceptarAccion = new Notification.Action.Builder(R.mipmap.ic_launcher, "ACEPTAR", pendingIntent).build();

        //PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
        Uri sonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificacionHelper notificacionHelper = new NotificacionHelper(getBaseContext());
        Notification.Builder builder = null;
        try {
            builder = notificacionHelper.getNotificationAccion(titulo, contenido, sonido, AceptarAccion, imagen);
            notificacionHelper.getManager().notify(2, builder.build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 */
}
