package com.inforad.fixall;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.airbnb.lottie.LottieAnimationView;
import com.inforad.fixall.procesos.procesador;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

public class avatarMujer extends AppCompatActivity {
    private GifImageView mAnimacion;
    private Button btnContinuar;
    private ConstraintLayout Fondo;
    private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_avatar_mujer);
        Iniciar();

        mProcesador.caracteristicasPlanConstraint(Fondo, avatarMujer.this);

        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(avatarMujer.this, viewTipoUsuario.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    private void Iniciar() {
        mAnimacion = findViewById(R.id.givAvatar);
        btnContinuar = findViewById(R.id.bContinuar);
        Fondo = findViewById(R.id.clFondo);

        mProcesador = new procesador();
    }
}