package com.inforad.fixall.retrofit;

import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMBodySimple;
import com.inforad.fixall.model.FCMResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMApiSimple {
    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAAo9QRla4:APA91bH3RWh3_3Ouectn-bK8tlzs_ZsOiZ_eX4JZ0uF6jDUn9N-uU5Y-tW_j0QUwqxxvbxlB8aF4bMNXcHBFvBnAAxgs9mPyCFHgoPCaKjrnJkbjXEEbpar69g4tysxSjE-RIomgBoSj"
    })
    @POST("fcm/send")
    Call<FCMResponse> send(@Body FCMBodySimple body);
}
