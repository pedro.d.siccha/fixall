package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewPerfilEmpresario;
import com.inforad.fixall.model.Plan;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.newConfigPerfil;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.profesional.viewTarjetas;
import com.inforad.fixall.provider.planProvider;
import com.inforad.fixall.provider.userProvider;

import pl.droidsonroids.gif.GifImageView;

public class viewSubscripcion extends AppCompatActivity {

    private Button botonFixGold, botonFixBlack, botonBasico;
    private LinearLayout FixGold, FixBlack, fixBasico;
    private String idUsuario;
    private userProvider mUserProvider;
    private FirebaseAuth mAuth;
    private ConstraintLayout Fondo;
    private procesador mProcesador;
    private planProvider mPlanProvider;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_subscripcion);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(Fondo, viewSubscripcion.this);

        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    switch (snapshot.child("plan").getValue().toString()){
                        case "BASICO":
                            fixBasico.setVisibility(View.GONE);
                            FixGold.setVisibility(View.VISIBLE);
                            FixBlack.setVisibility(View.VISIBLE);
                            break;
                        case "GOLD":
                            FixGold.setVisibility(View.GONE);
                            fixBasico.setVisibility(View.VISIBLE);
                            FixBlack.setVisibility(View.VISIBLE);
                            break;
                        case "BLACK":
                            FixBlack.setVisibility(View.GONE);
                            fixBasico.setVisibility(View.VISIBLE);
                            FixGold.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        botonFixGold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mUserProvider.actualizarPlan(idUsuario, "GOLD").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Plan plan = new Plan(idUsuario, "GOLD", mProcesador.fecActual(), mProcesador.fecUnMes(), 0.00, 6, 0.05, 12, 2);
                        mPlanProvider.registrarPlan(plan, idUsuario).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent intent = new Intent(viewSubscripcion.this, viewTipoUsuario.class);
                                startActivity(intent);
                            }
                        });
                    }
                });

            }
        });

        botonFixBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUserProvider.actualizarPlan(idUsuario, "BLACK").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Plan plan = new Plan(idUsuario, "BLACK", mProcesador.fecActual(), mProcesador.fecUnMes(), 0.00, 6, 0.05, 100, 1);
                        mPlanProvider.registrarPlan(plan, idUsuario).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent intent = new Intent(viewSubscripcion.this, viewTipoUsuario.class);
                                startActivity(intent);
                            }
                        });
                    }
                });
            }
        });

        botonBasico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUserProvider.actualizarPlan(idUsuario, "BASICO").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Plan plan = new Plan(idUsuario, "BASICO", mProcesador.fecActual(), mProcesador.fecUnMes(), 0.00, 0, 0.00, 0, 3);
                        mPlanProvider.registrarPlan(plan, idUsuario).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent intent = new Intent(viewSubscripcion.this, viewTipoUsuario.class);
                                startActivity(intent);
                            }
                        });
                    }
                });
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewSubscripcion.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mAuth = FirebaseAuth.getInstance();

        idUsuario = mAuth.getCurrentUser().getUid();

        FixGold = findViewById(R.id.llFixGold);
        FixBlack = findViewById(R.id.llFixBlack);
        fixBasico = findViewById(R.id.llFixBasico);

        botonFixGold = findViewById(R.id.btnFixGold);
        botonFixBlack = findViewById(R.id.btnFixBlack);
        botonBasico = findViewById(R.id.btnFixBasico);

        Fondo = findViewById(R.id.clFondo);

        mUserProvider = new userProvider();
        mProcesador = new procesador();
        mPlanProvider = new planProvider();
    }

}