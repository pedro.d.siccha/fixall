package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.fixall.adapters.contactosAdaptador;
import com.inforad.fixall.adapters.tarjetaAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.clubEmpresario;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewHistprial;
import com.inforad.fixall.empresario.viewListaFavorito;
import com.inforad.fixall.empresario.viewOfertasRecibidas;
import com.inforad.fixall.empresario.viewPerfilEmpresario;
import com.inforad.fixall.model.Contacto;
import com.inforad.fixall.model.TarjetaCredito;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.authProvider;

import pl.droidsonroids.gif.GifImageView;

public class viewBilletera extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private RecyclerView recyclerListaTarjetas;
    private String idUsuario;
    private Button btnAgregar;
    private tarjetaAdaptador mTarjetaAdaptador;
    private DrawerLayout drawerLayout;
    private ImageView btnAtras;
    private procesador mProcesador;
    private authProvider mAuthProvider;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_billetera);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewBilletera.this);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewBilletera.this, viewAgregarTarjeta.class);
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewBilletera.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        btnAgregar = findViewById(R.id.bAgregarTarjeta);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mProcesador = new procesador();
        mAuthProvider = new authProvider();

        recyclerListaTarjetas = findViewById(R.id.rvListaTarjeta);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaTarjetas.setLayoutManager(linearLayoutManager);

        cargarMenu();
    }

    private void cargarMenu() {
        drawerLayout = findViewById(R.id.drawer_layout);
        btnAtras = findViewById(R.id.ivAtras);

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewBilletera.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryTarjeta = mDatabase.child("Tarjeta").child(idUsuario);
        FirebaseRecyclerOptions<TarjetaCredito> options = new FirebaseRecyclerOptions.Builder<TarjetaCredito>().setQuery(queryTarjeta, TarjetaCredito.class).build();
        mTarjetaAdaptador = new tarjetaAdaptador(options, viewBilletera.this);
        recyclerListaTarjetas.setAdapter(mTarjetaAdaptador);
        mTarjetaAdaptador.startListening();
    }

}