package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.model.Invitacion;
import com.inforad.fixall.model.Iso;

import java.util.HashMap;
import java.util.Map;

public class isoProvider {
    DatabaseReference mDatabase;
    public isoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Iso");
    }

    public Task<Void> crearIso(Iso iso, String idUsuario){
        return mDatabase.child(idUsuario).child(iso.getId()).setValue(iso);
    }

    public DatabaseReference obtenerIso(String idUsuario, String idIso){
        return mDatabase.child(idUsuario).child(idIso);
    }

    public Task<Void> eliminarIso(String idUsuario, String idIso){
        return mDatabase.child(idUsuario).child(idIso).removeValue();
    }

    public Task<Void> actualizarIso(Iso iso, String idUsuario){
        Map<String, Object> map = new HashMap<>();
        map.put("id", iso.getId());
        map.put("codigo", iso.getCodigo());
        map.put("imagen", iso.getImagen());
        map.put("nombre", iso.getNombre());
        return mDatabase.child(idUsuario).child(iso.getId()).updateChildren(map);
    }

}
