package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.PalabraClave;
import com.inforad.fixall.model.UsuarioPalabra;

public class usuariopalabraclaveProvider {
    private DatabaseReference mDatabase;

    public usuariopalabraclaveProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Usuario_PalabraClave");
    }

    public Task<Void> crearUsuarioPalabraClave(UsuarioPalabra usuarioPalabra, String idUsuario, String idPalabraClave){
        return mDatabase.child(idUsuario).child(idPalabraClave).setValue(usuarioPalabra);
    }

    public DatabaseReference obtenerUsuario(String idUsuario) {
        return mDatabase.child(idUsuario);
    }
}
