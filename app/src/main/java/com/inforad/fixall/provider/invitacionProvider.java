package com.inforad.fixall.provider;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.model.Invitacion;

import java.util.HashMap;
import java.util.Map;

public class invitacionProvider {
    DatabaseReference mDatabase;

    public invitacionProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public Task<Void> enviarInvitacion(Invitacion invitacion, String nodo, String idPrincipal, String idSecundario){
        return mDatabase.child(nodo).child(idPrincipal).push().setValue(invitacion);
    }

    public Task<Void> actualizarInvitacionProfesional(String idUsuario, String idInvitacion, String estado){
        Map<String, Object> map = new HashMap<>();
        map.put("estado", estado);
        return mDatabase.child("Invitacion_Profesional").child(idUsuario).child(idInvitacion).updateChildren(map);
    }

    public Task<Void> actualizarInvitacionEmpresario(String idUsuario, String idInvitacion, String estado){
        Map<String, Object> map = new HashMap<>();
        map.put("estado", estado);
        return mDatabase.child("Invitacion_Empresario").child(idUsuario).child(idInvitacion).updateChildren(map);
    }

}
