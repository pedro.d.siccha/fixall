package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.model.ProfesionProfesional;

public class profesionProfesionalProvider {
    private DatabaseReference mDatabase;

    public profesionProfesionalProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Profesion_Profesional");
    }

    public Task<Void> crearProfesionProfesional(ProfesionProfesional profesion, String idUsuario){
        return mDatabase.child(idUsuario).push().setValue(profesion);
    }
}
