package com.inforad.fixall.provider;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.inforad.fixall.model.Token;

public class tokenProvider {
    DatabaseReference mDatabase;

    public tokenProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Tokens");
    }

    public void crear(String idUsuario){

        if (idUsuario == null){
            return;
        }

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                Token token = new Token(instanceIdResult.getToken());
                mDatabase.child(idUsuario).setValue(token);
            }
        });
    }

    public DatabaseReference obtenerToken(String idUsuario){
        return mDatabase.child(idUsuario);
    }

}
