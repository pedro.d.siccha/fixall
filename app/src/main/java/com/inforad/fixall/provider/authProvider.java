package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class authProvider {

    FirebaseAuth mAuth;

    public authProvider() {
        mAuth = FirebaseAuth.getInstance();
    }

    public Task<AuthResult> registrar(String email, String password){
        return mAuth.createUserWithEmailAndPassword(email, password);
    }

    public Task<AuthResult> login(String email, String password){
        return mAuth.signInWithEmailAndPassword(email, password);
    }

    public void cerrarSesion(){
        mAuth.signOut();
    }


}
