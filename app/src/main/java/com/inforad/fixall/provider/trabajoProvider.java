package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Iso;
import com.inforad.fixall.model.Profesion;
import com.inforad.fixall.model.Trabajo;

import java.util.HashMap;
import java.util.Map;

public class trabajoProvider {

    private DatabaseReference mDatabase;

    public trabajoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Trabajo");
    }

    public Task<Void> crearTrabajo(Trabajo trabajo, String idUsuario){
        return mDatabase.child(idUsuario).child(trabajo.getId()).setValue(trabajo);
    }

    public DatabaseReference obtenerTrabajo(String idUsuario, String idTrabajo){
        return mDatabase.child(idUsuario).child(idTrabajo);
    }

    public Task<Void> eliminarTrabajo(String idUsuario, String idTrabajo){
        return mDatabase.child(idUsuario).child(idTrabajo).removeValue();
    }

    public Task<Void> actualizarTrabajo(Trabajo trabajo, String idUsuario){
        Map<String, Object> map = new HashMap<>();
        map.put("id", trabajo.getId());
        map.put("cargo", trabajo.getCargo());
        map.put("descripcion", trabajo.getDescripcion());
        map.put("direccion", trabajo.getDireccion());
        map.put("fecFin", trabajo.getFecFin());
        map.put("fecInicio", trabajo.getFecInicio());
        map.put("imagen", trabajo.getImagen());
        map.put("nomEmpresa", trabajo.getNomEmpresa());
        map.put("tipoEmpleo", trabajo.getTipoEmpleo());
        return mDatabase.child(idUsuario).child(trabajo.getId()).updateChildren(map);
    }

}
