package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Contacto;
import com.inforad.fixall.model.Plan;

import java.util.HashMap;
import java.util.Map;

public class planProvider {
    DatabaseReference mDatabase;
    public planProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Plan");
    }

    public Task<Void> registrarPlan(Plan plan, String idUsuario){
        return mDatabase.child(idUsuario).setValue(plan);
    }

    public DatabaseReference obtenerPlan(String idUsuario){
        return mDatabase.child(idUsuario);
    }

    public Task<Void> actualizarAcceso(String idUsuario, int nuevoAcceso){
        Map<String, Object> map = new HashMap<>();
        map.put("club", nuevoAcceso);
        return mDatabase.child(idUsuario).updateChildren(map);
    }

}
