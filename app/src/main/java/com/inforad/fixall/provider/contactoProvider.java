package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Contacto;
import com.inforad.fixall.model.User;

public class contactoProvider {
    DatabaseReference mDatabase;

    public contactoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
    }

    public Task<Void> crearContacto(Contacto contacto, String idUserPrincipal, String idUserContacto){
        return mDatabase.child(idUserPrincipal).child("Contacto").child(idUserContacto).setValue(contacto);
    }

}
