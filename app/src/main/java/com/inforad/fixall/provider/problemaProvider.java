package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.model.Problema;

public class problemaProvider {
    private DatabaseReference mDatabase;

    public problemaProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Problema");
    }

    public Task<Void> crearProblema(Problema problema, String idProfesional, String idEmpresario){
        return mDatabase.child(idProfesional).child(idEmpresario).push().setValue(problema);
    }

}
