package com.inforad.fixall.provider;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class profesionalProvider {

    DatabaseReference mDatabase;

    public profesionalProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("PROFESIONAL");
    }

    public DatabaseReference obtenerDatos(String idUsuario){
        return mDatabase.child(idUsuario);
    }
}
