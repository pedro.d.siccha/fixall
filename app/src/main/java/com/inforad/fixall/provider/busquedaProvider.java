package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Busqueda;

import java.util.HashMap;
import java.util.Map;

public class busquedaProvider {
    DatabaseReference mDatabase;

    public busquedaProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Busqueda");
    }

    public Task<Void> crearBusqueda(Busqueda busqueda, String palabraClave, String idEmpresario){
        return mDatabase.child(palabraClave).child(idEmpresario).setValue(busqueda);
    }

    public DatabaseReference obtenerBusqueda(String palabraClave, String idBusqueda){
        return mDatabase.child(palabraClave).child(idBusqueda);
    }

    public Task<Void> actualizarBusqueda(String palabraClave, String idBusqueda, String idProfesional, String aliasProfesional, String imgProfesional, String dirProfesional, Double costo, String estado, String fecha){
        Map<String, Object> map = new HashMap<>();
        map.put("idProfesional", idProfesional);
        map.put("aliasProfesional", aliasProfesional);
        map.put("imgProfesional", imgProfesional);
        map.put("idDirProfesional", dirProfesional);
        map.put("costo", costo);
        map.put("estado", estado);
        map.put("horaCita", fecha);
        return mDatabase.child(palabraClave).child(idBusqueda).updateChildren(map);
    }

    public Task<Void> generarIdHistorialBusqueda(String palabraClave, String idBusqueda){
        String idPush = mDatabase.push().getKey();
        Map<String, Object> map = new HashMap<>();
        map.put("idHistorialBusqueda", idPush);
        return mDatabase.child(palabraClave).child(idBusqueda).updateChildren(map);
    }
}
