package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Profesion;

public class profesionProvider {

    DatabaseReference mDatabase;

    public profesionProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("PROFESION");
    }

    public Task<Void> crear(Profesion profesion){
        return mDatabase.setValue(profesion);
    }

}
