package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Descripcion;
import com.inforad.fixall.model.Iso;

public class descripcionProvider {

    DatabaseReference mDatabase;

    public descripcionProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Descripcion");
    }

    public Task<Void> crearDescripcion(Descripcion descripcion, String idUsuario, String id){
        return mDatabase.child(idUsuario).child(id).setValue(descripcion);
    }

}
