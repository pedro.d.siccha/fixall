package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.Chat;

public class separadorProvider {
    DatabaseReference mDatabase;

    public separadorProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("chats");
    }

    public Task<Void> enviarMensaje(Chat chat, String idEnvia, String idRecibe){

        return mDatabase.child(idEnvia + "__&__" + idRecibe).child("mensaje").push().setValue(chat);

    }

}
