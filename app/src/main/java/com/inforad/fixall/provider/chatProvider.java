package com.inforad.fixall.provider;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.model.Chat;

import java.util.ArrayList;
import java.util.Date;

public class chatProvider {

    DatabaseReference mDatabase;

    public chatProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Chats");
    }

    public void crear(Chat chat){
        mDatabase.child(chat.getIdUsuario1()).child("Users").child(chat.getIdUsuario2()).setValue(chat);
        Chat chat2 = new Chat(chat.getId(), chat.getIdUsuario2(), chat.getIdUsuario1(), chat.getIdNotificacion(), false, chat.getTimestamp());
        mDatabase.child(chat.getIdUsuario2()).child("Users").child(chat.getIdUsuario1()).setValue(chat2);
        //mDatabase.child(chat.getIdUsuario1() + chat.getIdUsuario2()).setValue(chat);
    }

    public void obtenerOrdenUsuarios(String idUsuario1, String idUsuario2, Context mContex, int idNotificacion){

        mDatabase.child(idUsuario1).child("Users").child(idUsuario2).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    //Toast.makeText(mContex, "El chat existe", Toast.LENGTH_SHORT).show();
                }else{
                    String idChat = mDatabase.push().getKey();
                    Chat chat = new Chat(idChat, idUsuario1, idUsuario2,idNotificacion, false, new Date().getTime());
                    crear(chat);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public DatabaseReference obtenerDatosChat(String idUsuario1, String idUsuario2){
        return mDatabase.child(idUsuario1).child("Users").child(idUsuario2);
    }

}
