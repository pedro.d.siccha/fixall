package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.model.Oferta;

import java.util.HashMap;
import java.util.Map;

public class ofertaProvider {
    private DatabaseReference mDatabase;

    public ofertaProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Oferta");
    }

    public Task<Void> crearOferta(Oferta oferta, String idEmpresario, String idProfesional){
        return mDatabase.child(idEmpresario).child(idProfesional).setValue(oferta);
    }

    public Task<Void> actualizarOferta(String idEmpresario, String idOferta, String estado){
        Map<String, Object> map = new HashMap<>();
        map.put("estado", estado);
        return mDatabase.child(idEmpresario).child(idOferta).updateChildren(map);
    }

    public DatabaseReference obtenerOferta(String idUsuario){
        return mDatabase.child(idUsuario);
    }

}
