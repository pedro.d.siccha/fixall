package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Flyer;

public class flyerProvider {

    private DatabaseReference mDatabase;

    public flyerProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Flyer");
    }

    public Task<Void> crearFlyer(Flyer flyer, String idFlyer){
        return mDatabase.child(idFlyer).setValue(flyer);
    }

}
