package com.inforad.fixall.provider;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.TarjetaCredito;

public class tajertaCreditoProvider {
    DatabaseReference mDatabase;

    public tajertaCreditoProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Tarjeta");
    }

    public Task<Void> crearTarjeta(TarjetaCredito tarjeta, String idUsuario, String idTarjeta){
        return mDatabase.child(idUsuario).child(idTarjeta).setValue(tarjeta);
    }

    public DatabaseReference obtenerTarjeta(String idUsuario, String idTarjeta){
        return mDatabase.child(idUsuario).child(idTarjeta);
    }

    public DatabaseReference mostrarTarjeta(String idUsuario){
        return mDatabase.child(idUsuario);
    }

}
