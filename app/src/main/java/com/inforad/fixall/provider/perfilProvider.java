package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Perfil;
import com.inforad.fixall.model.Profesion;

public class perfilProvider {

    DatabaseReference mDatabase;

    public perfilProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Perfil");
    }

    public Task<Void> crear(Perfil perfil, String idUsuario){
        return mDatabase.child(idUsuario).setValue(perfil);
    }

}
