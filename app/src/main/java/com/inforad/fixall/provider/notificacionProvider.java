package com.inforad.fixall.provider;

import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMBodySimple;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.retrofit.IFCMApi;
import com.inforad.fixall.retrofit.IFCMApiSimple;
import com.inforad.fixall.retrofit.retrofitCliente;

import retrofit2.Call;

public class notificacionProvider {
    private String url = "https://fcm.googleapis.com";

    public notificacionProvider() {
    }

    public Call<FCMResponse> sendNotification(FCMBody body){
        return retrofitCliente.getClient(url).create(IFCMApi.class).send(body);//
    }

    public Call<FCMResponse> sendNotificationSimple(FCMBodySimple bodySimple){
        return retrofitCliente.getClient(url).create(IFCMApiSimple.class).send(bodySimple);
    }

}
