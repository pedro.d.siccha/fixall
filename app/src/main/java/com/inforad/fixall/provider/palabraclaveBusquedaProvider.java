package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.PalabraClave_Busqueda;

public class palabraclaveBusquedaProvider {
    DatabaseReference mDatabase;

    public palabraclaveBusquedaProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("PalabraClave_Busqueda");
    }

    public Task<Void> crearBusqueda(PalabraClave_Busqueda busqueda, String palabraClave, String idEmpresario){
        return mDatabase.child(palabraClave).push().setValue(busqueda);
    }

}
