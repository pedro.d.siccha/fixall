package com.inforad.fixall.provider;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class GeofireProvider {

    private DatabaseReference mDatabase;
    private GeoFire mGeofire;

    public GeofireProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("direccion");
        mGeofire = new GeoFire(mDatabase);
    }

    public void guadarDireccion(String idUsuario, LatLng latLng){
        mGeofire.setLocation(idUsuario, new GeoLocation(latLng.latitude, latLng.longitude));
    }

    public void eliminarDireccion(String idUsuario){
        mGeofire.removeLocation(idUsuario);
    }

    public DatabaseReference obtenerDireccion(String idUsuario){
        return mDatabase.child(idUsuario).child("l");
    }

    public GeoQuery obtenerProfesionales(LatLng latLng, double radio){
        GeoQuery geoQuery = mGeofire.queryAtLocation(new GeoLocation(latLng.latitude, latLng.longitude), radio);
        geoQuery.removeAllListeners();
        return geoQuery;
    }

}
