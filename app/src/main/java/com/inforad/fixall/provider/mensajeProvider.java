package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.fixall.model.Iso;
import com.inforad.fixall.model.Mensaje;

public class mensajeProvider {

    DatabaseReference mDatabase;

    public mensajeProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Mensajes");
    }

    public Task<Void> crearMensaje(Mensaje mensaje){
        String id = mDatabase.push().getKey();
        mensaje.setId(id);
        return mDatabase.child(mensaje.getIdChat()).child(id).setValue(mensaje);
    }

    public Query obtenerMensajeEnvio(String idChat, String idEnvia){
        return mDatabase.child(idChat).orderByChild("idEnvia").equalTo(idEnvia);
    }

    public Query obtenerUltimoMensaje(String idChat){
        return mDatabase.child(idChat).orderByChild("timestamp");
    }

}
