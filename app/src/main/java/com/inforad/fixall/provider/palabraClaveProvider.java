package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.model.PalabraClave;

public class palabraClaveProvider {
    private DatabaseReference mDatabase;

    public palabraClaveProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("PalabraClave_Usuario");
    }

    public Task<Void> crearPalabraClave(PalabraClave palabraClave, String idUsuario, String idPalabraClave){
        return mDatabase.child(idPalabraClave).child(idUsuario).setValue(palabraClave);
    }
}
