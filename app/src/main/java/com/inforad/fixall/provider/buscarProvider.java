package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.fixall.model.Buscar;

import java.util.HashMap;
import java.util.Map;

public class buscarProvider {

    DatabaseReference mDatabase;

    public buscarProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Buscar");
    }

    public Task<Void> crear(Buscar busqueda){
        return mDatabase.child(busqueda.getIdUsuario()).child(busqueda.getId()).setValue(busqueda);
    }

    public Query obtener(String idUsuario){
        return mDatabase.child(idUsuario).orderByChild("estado").equalTo("PENDIENTE");
    }

    public Task<Void> actualizarEstado(String idUsuario, String idBusqueda){
        Map<String, Object> map = new HashMap<>();
        map.put("estado", "BAJA");
        return mDatabase.child(idUsuario).child(idBusqueda).updateChildren(map);
    }

}
