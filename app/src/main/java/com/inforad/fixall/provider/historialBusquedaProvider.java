package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.HistorialBusqueda;

import java.util.HashMap;
import java.util.Map;

public class historialBusquedaProvider {

    DatabaseReference mDatabase;

    public historialBusquedaProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("HistorialBusqueda");
    }

    public Task<Void> crearHistorial(HistorialBusqueda historial){
        return mDatabase.child(historial.getIdHistorial()).setValue(historial);
    }

    public Task<Void> actualizarCalificacionProfesional(String idHistorialBusqueda, float calificacionProfesional){
        Map<String, Object> map = new HashMap<>();
        map.put("calificacionProfesional", calificacionProfesional);
        return mDatabase.child(idHistorialBusqueda).updateChildren(map);
    }

    public Task<Void> actualizarCalificacionEmpresario(String idHistorialBusqueda, float calificacionEmpresario){
        Map<String, Object> map = new HashMap<>();
        map.put("calificacionEmpresario", calificacionEmpresario);
        return mDatabase.child(idHistorialBusqueda).updateChildren(map);
    }

    public DatabaseReference obtenerHistorial(String idHistorialBusqueda){
        return mDatabase.child(idHistorialBusqueda);
    }

}
