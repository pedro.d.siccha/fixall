package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.EstudioProfesional;

import java.util.HashMap;
import java.util.Map;

public class estudioProvider {
    private DatabaseReference mDatabase;

    public estudioProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Estudio");
    }

    public Task<Void> crearEstudio(EstudioProfesional estudio, String idUsuario){
        return mDatabase.child(idUsuario).child(estudio.getId()).setValue(estudio);
    }

    public DatabaseReference obtenerEstudio(String idUsuario, String idEstudio){
        return mDatabase.child(idUsuario).child(idEstudio);
    }

    public Task<Void> eliminarEstudio(String idUsuario, String idEstudio){
        return mDatabase.child(idUsuario).child(idEstudio).removeValue();
    }

    public Task<Void> actualizarEstudio(EstudioProfesional estudio, String idUsuario){
        Map<String, Object> map = new HashMap<>();
        map.put("id", estudio.getId());
        map.put("nombreColegio", estudio.getNombreColegio());
        map.put("grado", estudio.getGrado());
        map.put("fecInicio", estudio.getFecInicio());
        map.put("fecFin", estudio.getFecFin());
        map.put("descripcion", estudio.getDescripcion());
        return mDatabase.child(idUsuario).child(estudio.getId()).updateChildren(map);
    }

}
