package com.inforad.fixall.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.fixall.model.Direccion;

public class direccionProvider {

    private DatabaseReference mDatabase;

    public direccionProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("direccion_local");
    }

    public Task<Void> guardarDireccion(Direccion direccion, String idUsuario, String idDireccion){
        return mDatabase.child(idUsuario).child(idDireccion).setValue(direccion);
    }

    public DatabaseReference obtenerDireccion(String idUsuario){
        return mDatabase.child(idUsuario);
    }

    public Query obtenerNomDireccion(String idUsuario, String ubicacion){
        return mDatabase.child(idUsuario).orderByChild("nombre").equalTo(ubicacion);
    }

}
