package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMBodySimple;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.GeofireProvider;
import com.inforad.fixall.provider.busquedaProvider;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.tokenProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewLogin;
import com.inforad.fixall.viewSolgan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class viewPreLoad extends AppCompatActivity {

    private static int SPLASH_SCREEN = 900000;
    private TextView txtBuscar;
    private String palabraClave, telefono, idUsuario;
    private LottieAnimationView mAnimacion;
    private Button btnBuscar;
    private FirebaseAuth mAuth;
    private ConstraintLayout Fondo;
    private procesador mProcesador;
    private GifImageView btnAsistente;
    private GeofireProvider mGeofireProvider;
    private LatLng mLatLngOrigen, mLatLngDestion;
    private double latituOrigen;
    private double longitudOrigen;
    private double radio = 0.1;
    private double mExtraDestinationLng;
    private boolean mProfesionalFound = false;
    private String mIdProfesionalFound = "";
    private LatLng mProfesionalLatLng;
    private tokenProvider mTokenProvider;
    private ArrayList<String> mProfesionalLista = new ArrayList<>();
    private List<String> mTokenList = new ArrayList<>();
    private int mContador = 0;
    private notificacionProvider mNotificacionProvider;
    private busquedaProvider mBusquedaprovider;
    private userProvider mUsuarioProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_pre_load);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(Fondo, viewPreLoad.this);
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewPreLoad.this, viewPerfilEmpresario.class);
                intent.putExtra("idUsuario", idUsuario);
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
                finish();
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewPreLoad.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProcesador = new procesador();
        mTokenProvider = new tokenProvider();
        mNotificacionProvider = new notificacionProvider();
        mBusquedaprovider = new busquedaProvider();
        mUsuarioProvider = new userProvider();

        btnBuscar = findViewById(R.id.bBuscar);
        Fondo = findViewById(R.id.clFondo);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mAnimacion = findViewById(R.id.animacionCarga);
        mAnimacion.playAnimation();

        txtBuscar = findViewById(R.id.tvPalabraBuscar);

        palabraClave = getIntent().getStringExtra("palabraClave");
        telefono = getIntent().getStringExtra("telefono");
        radio = Double.parseDouble(getIntent().getStringExtra("rango"));

        txtBuscar.setText("Buscando " + palabraClave);

        mGeofireProvider = new GeofireProvider();

        mGeofireProvider.obtenerDireccion(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                latituOrigen = Double.parseDouble(snapshot.child("0").getValue().toString());
                longitudOrigen = Double.parseDouble(snapshot.child("1").getValue().toString());
                mLatLngOrigen = new LatLng(latituOrigen, longitudOrigen);
                getClosesProfesional();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }

    private void getClosesProfesional(){
        mGeofireProvider.obtenerProfesionales(mLatLngOrigen, radio).addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                Log.d("PROFESIONAL", "ID: " + key);
                mProfesionalLista.add(key);
            }

            @Override
            public void onKeyExited(String key) {

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {
                for (String id: mProfesionalLista){
                    mTokenProvider.obtenerToken(id).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            mContador = mContador + 1;
                            if (snapshot.exists()){
                                String tokens = snapshot.child("token").getValue().toString();
                                mTokenList.add(tokens);
                            }

                            if (mContador == mProfesionalLista.size()){
                                //enviarNotificacion(mTokenList, );
                                mUsuarioProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if (snapshot.exists()){
                                            String img = snapshot.child("imagen").getValue().toString();
                                            Map<String, String> map = new HashMap<>();
                                            map.put("titulo", "BUSQUEDA");
                                            map.put("contenido", "Se está solicitando un " + palabraClave + "\nDistancia: " + radio);
                                            map.put("imagen", img);
                                            FCMBodySimple fcmBodySimple = new FCMBodySimple(mTokenList, "high", "4500s", map);
                                            mNotificacionProvider.sendNotificationSimple(fcmBodySimple).enqueue(new Callback<FCMResponse>() {
                                                @Override
                                                public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                                                    if (response.body() != null){
                                                        if (response.body().getSuccess() == 1){
                                                            Toast.makeText(viewPreLoad.this, "La solicitud de busqueda se realizó correctamente.", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<FCMResponse> call, Throwable t) {

                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });

                            }


                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }

    private void enviarNotificacion(ArrayList<String> listaToken, Busqueda busqueda){
        Gson gson = new Gson();
        ArrayList<Busqueda> busquedaArrayList = new ArrayList<>();
        busquedaArrayList.add(busqueda);
        String busq = gson.toJson(busquedaArrayList);
        Map<String, String> map = new HashMap<>();
        map.put("titulo", "BUSQUEDA");
        map.put("contenido", "El usuario: " + busqueda.getAliasUsuario() + ", está solicitando un " + busqueda.getPalabraClave());
        map.put("busqueda", busq);
        FCMBodySimple fcmBodySimple = new FCMBodySimple(listaToken, "high", "4500s", map);
        mNotificacionProvider.sendNotificationSimple(fcmBodySimple).enqueue(new Callback<FCMResponse>() {
            @Override
            public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                if (response.body() != null){
                    if (response.body().getSuccess() == 1){
                        Toast.makeText(viewPreLoad.this, "La solicitud de busqueda se realizó correctamente.", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<FCMResponse> call, Throwable t) {

            }
        });
    }


}