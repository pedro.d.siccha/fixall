package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.profesionalLlegoAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

import pl.droidsonroids.gif.GifImageView;

public class viewListaProfesionalesLlego extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerListaProfesionalLlegada;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private profesionalLlegoAdaptador mProfesionalAdaptador;
    private DrawerLayout fondo;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private userProvider mUserProvider;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private procesador mProcesador;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_lista_profesionales_llego);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(fondo, viewListaProfesionalesLlego.this);
        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewListaProfesionalesLlego.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });
    }

    private void Iniciar() {
        fondo = findViewById(R.id.drawer_layout);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mUserProvider = new userProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        recyclerListaProfesionalLlegada = findViewById(R.id.recyclerListaProfesionalLlego);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaProfesionalLlegada.setLayoutManager(linearLayoutManager);

        mProcesador = new procesador();

        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewListaProfesionalesLlego.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query query = mDatabase.child("Oferta").child(idUsuario).orderByChild("estado").equalTo("LLEGO");
        FirebaseRecyclerOptions<Oferta> options = new FirebaseRecyclerOptions.Builder<Oferta>().setQuery(query, Oferta.class).build();
        mProfesionalAdaptador = new profesionalLlegoAdaptador(options, viewListaProfesionalesLlego.this);
        recyclerListaProfesionalLlegada.setAdapter(mProfesionalAdaptador);
        mProfesionalAdaptador.startListening();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicio:
                break;
            case R.id.nav_buscar:
                Intent viewBuscar = new Intent(viewListaProfesionalesLlego.this, viewPerfilEmpresario.class);
                viewBuscar.putExtra("idUsuario", idUsuario);
                viewBuscar.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(viewBuscar);
                break;
            case R.id.nav_ofertas:
                Intent viewOfertas = new Intent(viewListaProfesionalesLlego.this, viewOfertasRecibidas.class);
                startActivity(viewOfertas);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewListaProfesionalesLlego.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_historial:
                Intent viewHist = new Intent(viewListaProfesionalesLlego.this, viewHistprial.class);
                startActivity(viewHist);
                break;
            case  R.id.nav_cambio_perfilEmp:

                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewListaProfesionalesLlego.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewListaProfesionalesLlego.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewListaProfesionalesLlego.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewListaProfesionalesLlego.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;

        }

        fondo.closeDrawer(GravityCompat.START);
        return true;
    }
}