package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.model.Problema;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.problemaProvider;
import com.inforad.fixall.provider.tokenProvider;
import com.inforad.fixall.provider.userProvider;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class viewReportarProblemaE extends AppCompatActivity {

    private Button botonReportar;
    private CircleImageView imgProfesional;
    private FirebaseAuth mAuth;
    private String idUsuario, seleccion, idProfesion, idBusqueda, palabraClave, imagenUsuario;
    private TextView txtNomUsuario, txtApeUsuario;
    private RadioGroup radioOpciones;
    private RadioButton rbtnNoLlego, rbtnGrosero, rbtnTarde, rbtnOtraPersona, rbtnMalo, rbtnOtro;
    private EditText inputDescripcion;
    private problemaProvider mProblemaProvider;
    private userProvider mUserProvider;
    private notificacionProvider mNotificacion;
    private tokenProvider mTokenProvider;
    private ConstraintLayout Fondo;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private procesador mProcesador;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_reportar_problema_e);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(Fondo, viewReportarProblemaE.this);


        botonReportar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validarSeleccion();
                enviarNotificacion();

                    Problema problema = new Problema("", idBusqueda, idProfesion, idUsuario, seleccion, inputDescripcion.getText().toString());
                    mProblemaProvider.crearProblema(problema, idProfesion, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Intent intent = new Intent(viewReportarProblemaE.this, viewLLegoProfesionalE.class);
                            intent.putExtra("idProfesional", idProfesion);
                            intent.putExtra("idBusqueda", idBusqueda);
                            intent.putExtra("palabraClave", palabraClave);
                            startActivity(intent);
                            finish();
                        }
                    });
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewReportarProblemaE.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        botonReportar = findViewById(R.id.btnReportar);
        txtNomUsuario = findViewById(R.id.tvNombrePro);
        txtApeUsuario = findViewById(R.id.tvApellidoPro);
        imgProfesional = findViewById(R.id.civImgPerfilPro);
        radioOpciones = findViewById(R.id.rgOpciones);
        Fondo = findViewById(R.id.clFondo);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        rbtnNoLlego = findViewById(R.id.rbNLlego);
        rbtnGrosero = findViewById(R.id.rbGrosero);
        rbtnTarde = findViewById(R.id.rbTarde);
        rbtnOtraPersona = findViewById(R.id.rbOtraPersona);
        rbtnMalo = findViewById(R.id.rbMalo);
        rbtnOtro = findViewById(R.id.rbOtro);

        inputDescripcion = findViewById(R.id.txtDescripcionProblema);

        mProblemaProvider = new problemaProvider();
        mTokenProvider = new tokenProvider();
        mNotificacion = new notificacionProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProcesador = new procesador();
        idBusqueda = getIntent().getStringExtra("idBusqueda");
        idProfesion = getIntent().getStringExtra("idProfesional");
        palabraClave = getIntent().getStringExtra("palabraClave");

        mUserProvider = new userProvider();

        mUserProvider.obtenerUsuario(idProfesion).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    imagenUsuario = snapshot.child("imagen").getValue().toString();
                    Picasso.with(viewReportarProblemaE.this).load(imagenUsuario).into(imgProfesional);
                    txtNomUsuario.setText(snapshot.child("nombre").getValue().toString());
                    txtApeUsuario.setText(snapshot.child("apellido").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void validarSeleccion(){
        if (rbtnNoLlego.isChecked()){
            seleccion = "No Llegó";
            inputDescripcion.setVisibility(View.GONE);
        }

        if (rbtnGrosero.isChecked()){
            seleccion = "Grosero";
            inputDescripcion.setVisibility(View.GONE);
        }

        if (rbtnTarde.isChecked()){
            seleccion = "Llegó Tarde";
            inputDescripcion.setVisibility(View.GONE);
        }

        if (rbtnOtraPersona.isChecked()){
            seleccion = "Llegó Otra Persona";
            inputDescripcion.setVisibility(View.GONE);
        }

        if (rbtnMalo.isChecked()){
            seleccion = "Mal Servicio";
            inputDescripcion.setVisibility(View.GONE);
        }

        if (rbtnOtro.isChecked()){
            inputDescripcion.setVisibility(View.VISIBLE);
            seleccion = "Otro";
        }

    }

    private void enviarNotificacion(){
        mTokenProvider.obtenerToken(idProfesion).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String token = snapshot.child("token").getValue().toString();
                    Map<String, String> map = new HashMap<>();
                    map.put("titulo", "PROBLEMA CON EL SERVICIO");
                    map.put("contenido", "La oferta por " + palabraClave + ", el usuario reportó un problema en el servicio");
                    map.put("idUsuario", idUsuario);
                    map.put("imagen", imagenUsuario);
                    FCMBody fcmBody = new FCMBody(token, "high", "4500s", map);
                    mNotificacion.sendNotification(fcmBody).enqueue(new Callback<FCMResponse>() {
                        @Override
                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                            if (response.body() != null){
                                if (response.body().getSuccess() == 1){
                                    Toast.makeText(viewReportarProblemaE.this, "Oferta enviada", Toast.LENGTH_SHORT).show();
                                }else {
                                   // Toast.makeText(viewReportarProblemaE.this, "No se pudo enviar la notificación", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<FCMResponse> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}