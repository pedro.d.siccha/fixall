package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewLogin;
import com.inforad.fixall.viewSolgan;

import pl.droidsonroids.gif.GifImageView;

public class viewPagoCompletoE extends AppCompatActivity {

    private Button botonBuscar;
    private TextView txtPago;
    private String idProfesional, idBusqueda, palabraClave, costo, idUsuario;
    private LottieAnimationView mAnimacion;
    private ConstraintLayout Fondo;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private FirebaseAuth mAuth;
    private userProvider mUserProvider;
    private procesador mProcesador;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_pago_completo_e);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(Fondo, viewPagoCompletoE.this);

        botonBuscar = findViewById(R.id.btnBuscarOtroProfesional);

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewPagoCompletoE.this, viewCalificarProfesional.class);
                intent.putExtra("idProfesion",idProfesional);
                intent.putExtra("idBusqueda", idBusqueda);
                intent.putExtra("palabraClave", palabraClave);
                intent.putExtra("costo", costo);
                startActivity(intent);
                finish();
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewPagoCompletoE.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });


    }

    private void Iniciar() {

        mAnimacion = findViewById(R.id.animacionCheck);
        txtPago = findViewById(R.id.tvPago);
        Fondo = findViewById(R.id.clFondo);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mAnimacion.playAnimation();

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider = new userProvider();
        mProcesador = new procesador();
        obtenerDatos();
    }

    private void obtenerDatos() {
        idProfesional = getIntent().getStringExtra("idProfesion");
        idBusqueda = getIntent().getStringExtra("idBusqueda");
        palabraClave = getIntent().getStringExtra("palabraClave");
        costo = getIntent().getStringExtra("costo");

        txtPago.setText("S/. " + costo);

    }

}