package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.fixall.MainActivity;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.Buscar;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.provider.buscarProvider;
import com.inforad.fixall.provider.busquedaProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.utilidades.CompresorBitmapImage;
import com.inforad.fixall.utilidades.FileUtilidades;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;

import java.io.File;
import java.util.Date;

import pl.droidsonroids.gif.GifImageView;

public class viewPerfilEmpresario extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {
    private TextView txtBarra, txtContador;
    private EditText inputDescripcion, inputPalabraClave;
    private Button botonBuscar;
    private String telefonoEmpresario, idUsuario, intRango;
    private userProvider mUserProvider;
    private ImageButton btnImagen;
    private File mImagenFile;
    private final int GALLERY_REQUEST = 1;
    private ProgressDialog mProgresDialog;
    private FirebaseAuth mAuth;
    private busquedaProvider mBusqueda;
    private SeekBar jbSeecker;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private procesador mProcesador;
    private CheckBox banco;
    private authProvider mAuthProvider;
    private double mRadious = 0;
    private GifImageView btnAsistente;
    private buscarProvider mBuscarProvider;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_perfil_empresario);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewPerfilEmpresario.this);


        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(intRango) > 0){
                    generarBusqueda(inputPalabraClave.getText().toString().toUpperCase(), inputDescripcion.getText().toString(), intRango);
                }else{
                    Toast.makeText(viewPerfilEmpresario.this, "Por favor seleccione un rango valido", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirGaleria();
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewPerfilEmpresario.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(viewPerfilEmpresario.this, panelcontrolEmpresario.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void abrirGaleria() {
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImagenFile = FileUtilidades.from(this, data.getData());
                btnImagen.setImageBitmap(BitmapFactory.decodeFile(mImagenFile.getAbsolutePath()));
            }catch (Exception e){
                Log.d("Error", "Mensaje del error " + e.getMessage());
            }
        }
    }

    private void Iniciar() {
        txtContador = findViewById(R.id.tvContador);
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mProgresDialog = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mBusqueda = new busquedaProvider();
        mProcesador = new procesador();

        mUserProvider = new userProvider();
        mAuthProvider = new authProvider();
        mBuscarProvider = new buscarProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();


        jbSeecker = findViewById(R.id.seekBar);
        txtBarra = findViewById(R.id.tvRango);

        rangoPlan(0);



        jbSeecker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progreso, boolean b) {
                rangoPlan(progreso);
                intRango = String.valueOf(progreso);
                mRadious = Double.valueOf(progreso);
                
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    telefonoEmpresario = snapshot.child("telefono").getValue().toString();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        inputDescripcion = findViewById(R.id.txtBuscadorServicio);
        inputPalabraClave = findViewById(R.id.txtPalabraClave);
        btnImagen = findViewById(R.id.ibtnFotoReferencial);
        botonBuscar = findViewById(R.id.btnBuscar);

        cargarMenu();

        inputDescripcion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int tamano = s.length();
                txtContador.setText(tamano + "/150");

            }
        });

    }

    private void rangoPlan(int progreso) {

        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String plan = snapshot.child("plan").getValue().toString();
                    if (plan.equals("BASICO")){
                        jbSeecker.setMax(10);
                        txtBarra.setText(progreso + "/10 km");
                    }else if (plan.equals("GOLD")){
                        jbSeecker.setMax(100);
                        txtBarra.setText(progreso + "/100 km");
                    }else{
                        jbSeecker.setMax(100);
                        txtBarra.setText(progreso + "/100 km");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);
        //banco = findViewById(R.id.cbBanco);
        mProcesador.carcateristicaMenu(navigationView);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void generarBusqueda(String palabraClave, String descripcion, String rango){
        if (!inputPalabraClave.equals("") && mImagenFile != null){
            mProgresDialog.setMessage("Cargando la imagen, por favor espere un momento...");
            mProgresDialog.setCanceledOnTouchOutside(false);
            mProgresDialog.show();

            guardarImagen(palabraClave, descripcion, rango);
        }else {
            Toast.makeText(this, "Por favor, ingrese una palabra clave y una imagen descriptiva", Toast.LENGTH_LONG).show();
        }
    }

    private void guardarImagen(String palabraClave, String descripcion, String rango) {
        byte[] ImagenByte = CompresorBitmapImage.getImage(this, mImagenFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgBusqueda").child(mImagenFile.getName() + "_" + inputPalabraClave.getText().toString().toUpperCase() + ".jpg");
        UploadTask uploadTask = storage.putBytes(ImagenByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String imagen = uri.toString();

                            mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if (snapshot.exists()){
                                        String aliasEmpresario = snapshot.child("alias").getValue().toString();
                                        String imgEmpresario = snapshot.child("imagen").getValue().toString();

                                        Busqueda busqueda = new Busqueda();
                                        busqueda.setId(idUsuario);
                                        busqueda.setPalabraClave(palabraClave);
                                        busqueda.setDescripcion(descripcion);
                                        busqueda.setImagenDescripcion(imagen);
                                        busqueda.setEstado("PENDIENTE");
                                        busqueda.setIdUsuario(idUsuario);
                                        busqueda.setAliasUsuario(aliasEmpresario);
                                        busqueda.setImgUsuario(imgEmpresario);
                                        busqueda.setRangoBusqueda(rango);
                                        busqueda.setTimestamp(new Date().getTime());
                                        mBusqueda.crearBusqueda(busqueda, palabraClave, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                String idBuscar = mDatabase.push().getKey();
                                                Buscar buscar = new Buscar();
                                                buscar.setId(idBuscar);
                                                buscar.setPalabraClave(palabraClave);
                                                buscar.setDescripcion(descripcion);
                                                buscar.setImagenDescripcion(imagen);
                                                buscar.setEstado("PENDIENTE");
                                                buscar.setIdUsuario(idUsuario);
                                                buscar.setAliasUsuario(aliasEmpresario);
                                                buscar.setImgUsuario(imgEmpresario);
                                                buscar.setRangoBusqueda(rango);
                                                buscar.setTimestamp(new Date().getTime());
                                                mBuscarProvider.crear(buscar).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        mProgresDialog.dismiss();
                                                        Intent intent = new Intent(viewPerfilEmpresario.this, viewPreLoad.class);
                                                        intent.putExtra("palabraClave", palabraClave);
                                                        intent.putExtra("telefono", telefonoEmpresario);
                                                        intent.putExtra("rango", rango);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    });
                }else {
                    //Toast.makeText(viewPerfilEmpresario.this, "Error de imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicio:
                Intent viewInicio = new Intent(viewPerfilEmpresario.this, panelcontrolEmpresario.class);
                startActivity(viewInicio);
                break;
            case R.id.nav_buscar:
                break;
            case R.id.nav_ofertas:
                Intent viewOfertas = new Intent(viewPerfilEmpresario.this, viewOfertasRecibidas.class);
                startActivity(viewOfertas);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewPerfilEmpresario.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_historial:
                Intent viewHist = new Intent(viewPerfilEmpresario.this, viewHistprial.class);
                startActivity(viewHist);
                break;
            case  R.id.nav_cambio_perfilEmp:
                Intent modoProfesional = new Intent(viewPerfilEmpresario.this, viewPerfilP.class);
                startActivity(modoProfesional);
                break;
            case R.id.nav_banco:
                if (banco.isChecked()) {
                    //Toast.makeText(this, "BANCO", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(this, "PERSONA", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewPerfilEmpresario.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewPerfilEmpresario.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewPerfilEmpresario.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_favoritos:
                Intent viewFavorito = new Intent(viewPerfilEmpresario.this, viewListaFavorito.class);
                startActivity(viewFavorito);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewPerfilEmpresario.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                mProcesador.accesoClub(viewPerfilEmpresario.this);
                Intent viewClub = new Intent(viewPerfilEmpresario.this, clubEmpresario.class);
                startActivity(viewClub);
                break;
            case R.id.nav_logoit:
                mAuthProvider.cerrarSesion();
                Intent viewCerrar = new Intent(viewPerfilEmpresario.this, MainActivity.class);
                startActivity(viewCerrar);
                finish();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }
}