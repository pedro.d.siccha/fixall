package com.inforad.fixall.empresario;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewLogin;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

public class viewConfirmacionE extends AppCompatActivity {

    private Button botonBuscar;
    private FirebaseAuth mAuth;
    private DrawerLayout Fondo;
    private procesador mProcesador;
    private TextView txtPalabraClave, txtFecha, txtAlias;
    private CircleImageView imgProfesional;
    private String idProfesional, fecha, palabraClave, idUsuario;
    private userProvider mUserProvider;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_confirmacion_e);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(Fondo, viewConfirmacionE.this);

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewConfirmacionE.this, viewPerfilEmpresario.class);
                intent.putExtra("idUsuario", mAuth.getCurrentUser().getUid());
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewConfirmacionE.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        botonBuscar = findViewById(R.id.btnBuscarOtro);
        Fondo = findViewById(R.id.drawer_layout);
        txtPalabraClave = findViewById(R.id.tvPalabraClave);
        txtFecha = findViewById(R.id.tvFecha);
        txtAlias = findViewById(R.id.alias);
        imgProfesional = findViewById(R.id.civProfesional);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProcesador = new procesador();
        mUserProvider = new userProvider();

        cargarDatos();

        txtFecha.setText("Llegará " + fecha);
        txtPalabraClave.setText("Tu " + palabraClave);
        mUserProvider.obtenerUsuario(idProfesional).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String alias = snapshot.child("alias").getValue().toString();
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(viewConfirmacionE.this).load(img).into(imgProfesional);
                    txtAlias.setText(alias);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void cargarDatos() {
        idProfesional = getIntent().getStringExtra("idProfesional");
        fecha = getIntent().getStringExtra("fecActual");
        palabraClave = getIntent().getStringExtra("palabraClave");
    }
}