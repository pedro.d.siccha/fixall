package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.busquedaProvider;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.ofertaProvider;
import com.inforad.fixall.provider.tokenProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewLogin;
import com.inforad.fixall.viewSolgan;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class viewTarjetaP extends AppCompatActivity {

    private Button botonPago, btnEfectivo;
    private String idProfesional, idBusqueda, costo, palabraClave, imagenUsuario, idUsuario;
    private busquedaProvider mBusquedaProvider;
    private userProvider mUserProvider;
    private ofertaProvider mOfertaProvider;
    private FirebaseAuth mAuth;
    private notificacionProvider mNotificacion;
    private tokenProvider mTokenProvider;
    private ConstraintLayout fondo;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private procesador mProcesador;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_tarjeta_p);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(fondo, viewTarjetaP.this);

        botonPago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                enviarNotificacion();

                mBusquedaProvider.obtenerBusqueda(palabraClave, idBusqueda).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        final Calendar calendario = Calendar.getInstance();
                        int anio = calendario.get(Calendar.YEAR);
                        int mes = calendario.get(Calendar.MONTH);
                        int dia = calendario.get(Calendar.DAY_OF_MONTH);
                        String fecActual = String.valueOf(dia) + "/" + String.valueOf(mes + 1) + "/" + String.valueOf(anio);
                        mBusquedaProvider.actualizarBusqueda(palabraClave, idBusqueda, idProfesional, snapshot.child("aliasProfesional").getValue().toString(), snapshot.child("imgProfesional").getValue().toString(), snapshot.child("idDirProfesional").getValue().toString(), Double.parseDouble(costo), "FIN", fecActual).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                mOfertaProvider.actualizarOferta(mAuth.getCurrentUser().getUid(), idProfesional, "FIN").addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        mBusquedaProvider.generarIdHistorialBusqueda(palabraClave, idBusqueda).addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Intent intent = new Intent(viewTarjetaP.this, viewPagoCompletoE.class);
                                                intent.putExtra("idProfesion", idProfesional);
                                                intent.putExtra("idBusqueda", idBusqueda);
                                                intent.putExtra("costo", costo);
                                                intent.putExtra("palabraClave", palabraClave);
                                                startActivity(intent);
                                                finish();
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewTarjetaP.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

        btnEfectivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewTarjetaP.this, viewPagoCompletoE.class);
                intent.putExtra("idProfesion", idProfesional);
                intent.putExtra("idBusqueda", idBusqueda);
                intent.putExtra("costo", costo);
                intent.putExtra("palabraClave", palabraClave);
                startActivity(intent);
                finish();
            }
        });

    }

    private void Iniciar() {

        btnAsistente = findViewById(R.id.givAvatarVaron);
        botonPago = findViewById(R.id.btnRealizarPago);
        fondo = findViewById(R.id.clFondo);
        btnEfectivo = findViewById(R.id.btnRealizarEfectivo);



        mUserProvider = new userProvider();
        mBusquedaProvider = new busquedaProvider();
        mOfertaProvider = new ofertaProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mTokenProvider = new tokenProvider();
        mNotificacion = new notificacionProvider();
        mProcesador = new procesador();
        obtenerDatos();
    }

    private void obtenerDatos() {
        idProfesional = getIntent().getStringExtra("idProfesional");
        idBusqueda = getIntent().getStringExtra("idBusqueda");
        costo = getIntent().getStringExtra("costo");
        palabraClave = getIntent().getStringExtra("palabraClave");
        botonPago.setText("S/. " + costo);
        mUserProvider.obtenerUsuario(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                imagenUsuario = snapshot.child("imagen").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void enviarNotificacion(){
        mTokenProvider.obtenerToken(idProfesional).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String token = snapshot.child("token").getValue().toString();
                    Map<String, String> map = new HashMap<>();
                    map.put("titulo", "SERVICIO PAGADO");
                    map.put("contenido", "La oferta por " + palabraClave + ", El servicio se finalisó correctamente, y se procedio el pago");
                    map.put("imagen", imagenUsuario);
                    FCMBody fcmBody = new FCMBody(token, "high", "4500s", map);
                    mNotificacion.sendNotification(fcmBody).enqueue(new Callback<FCMResponse>() {
                        @Override
                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                            if (response.body() != null){
                                if (response.body().getSuccess() == 1){
                                    Toast.makeText(viewTarjetaP.this, "Oferta enviada", Toast.LENGTH_SHORT).show();
                                }else {
                                    //Toast.makeText(viewTarjetaP.this, "No se pudo enviar la notificación", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<FCMResponse> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void caracteristicasPlan() {
        String idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    switch (snapshot.child("plan").getValue().toString()){
                        case "BASICO":
                            cargarFondoBasico();
                            break;
                        case "GOLD":
                            cargarFondoGold();
                            break;
                        case "BLACK":
                            cargarFondoBlack();
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void cargarFondoBasico() {
        bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.fondo);
        background = new BitmapDrawable(getResources(), bitmap);
        fondo.setBackground(background);
    }

    private void cargarFondoGold() {
        bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.fondogold);
        background = new BitmapDrawable(getResources(), bitmap);
        fondo.setBackground(background);
    }

    private void cargarFondoBlack() {
        bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.fondoblack);
        background = new BitmapDrawable(getResources(), bitmap);
        fondo.setBackground(background);
    }

}