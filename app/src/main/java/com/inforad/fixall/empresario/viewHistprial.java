package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.MainActivity;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.historialAdaptado;
import com.inforad.fixall.adapters.historialBusquedaAdaptador;
import com.inforad.fixall.adapters.profesionalLlegoAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.HistorialBusqueda;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;

import pl.droidsonroids.gif.GifImageView;

public class viewHistprial extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private FirebaseAuth mAuth;
    private procesador mProcesador;
    private historialAdaptado mHistorialAdaptador;
    private DatabaseReference mDatabase;
    private RecyclerView recyclerListaHistorial;
    private String idUsuario;
    private authProvider mAuthProvider;
    private CheckBox banco;
    private GifImageView btnAsistente;
    private userProvider mUserProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_histprial);
        Inicio();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewHistprial.this);
        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewHistprial.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(getApplicationContext(), panelcontrolEmpresario.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }


    private void Inicio() {
        btnAsistente = findViewById(R.id.givAvatarVaron);
        cargarMenu();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider = new userProvider();
        recyclerListaHistorial = findViewById(R.id.rvListaHistorial);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaHistorial.setLayoutManager(linearLayoutManager);
        mProcesador = new procesador();
        mAuthProvider = new authProvider();

    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        btnMenu.setVisibility(View.GONE);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);
        //mProcesador.carcateristicaMenu(navigationView);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.nav_inicio:
                break;
            case R.id.nav_buscar:

                Intent viewBuscar = new Intent(viewHistprial.this, viewPerfilEmpresario.class);
                viewBuscar.putExtra("idUsuario", idUsuario);
                viewBuscar.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(viewBuscar);
                break;
            case R.id.nav_ofertas:
                Intent intent = new Intent(viewHistprial.this, viewOfertasRecibidas.class);
                startActivity(intent);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewHistprial.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_historial:
                Intent viewHist = new Intent(viewHistprial.this, viewHistprial.class);
                startActivity(viewHist);
                break;
            case  R.id.nav_cambio_perfilEmp:
                mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            String estado = snapshot.child("actividad").getValue().toString();
                            if (estado.equals("FinConfig")){
                                Intent modoProfesional = new Intent(viewHistprial.this, viewPerfilP.class);
                                startActivity(modoProfesional);
                            }else{
                                Intent modoProfesional = new Intent(viewHistprial.this, panelcontrolProfesional.class);
                                startActivity(modoProfesional);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                break;
            case R.id.nav_banco:

                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewHistprial.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewHistprial.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewHistprial.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_favoritos:
                Intent viewFavorito = new Intent(viewHistprial.this, viewListaFavorito.class);
                startActivity(viewFavorito);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewHistprial.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                mProcesador.accesoClub(viewHistprial.this);
                Intent viewClub = new Intent(viewHistprial.this, clubEmpresario.class);
                startActivity(viewClub);
                break;
            case R.id.nav_logoit:
                mAuthProvider.cerrarSesion();
                Intent viewCerrar = new Intent(viewHistprial.this, MainActivity.class);
                startActivity(viewCerrar);
                finish();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query query = mDatabase.child("HistorialBusqueda").orderByChild("idUsuario").equalTo(idUsuario);
        FirebaseRecyclerOptions<HistorialBusqueda> options = new FirebaseRecyclerOptions.Builder<HistorialBusqueda>().setQuery(query, HistorialBusqueda.class).build();
        mHistorialAdaptador = new historialAdaptado(options, viewHistprial.this);
        recyclerListaHistorial.setAdapter(mHistorialAdaptador);
        mHistorialAdaptador.startListening();
    }
}