package com.inforad.fixall.empresario;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.procesos.procesador;

import pl.droidsonroids.gif.GifImageView;

public class viewDetalleHistorialE extends AppCompatActivity {

    private static int SPLASH_SCREEN = 5000;
    private Button botonAtras;
    private ConstraintLayout Fondo;
    private procesador mProcesador;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_detalle_historial_e);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(Fondo, viewDetalleHistorialE.this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(viewDetalleHistorialE.this, "Espere su atencion...", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(viewDetalleHistorialE.this, viewLLegoProfesionalE.class);
                startActivity(intent);
            }
        }, SPLASH_SCREEN);

        botonAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewDetalleHistorialE.this, viewValoracionE.class);
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewDetalleHistorialE.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        botonAtras = findViewById(R.id.btnVolver);
        Fondo = findViewById(R.id.clFondo);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mProcesador = new procesador();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
    }
}