package com.inforad.fixall.empresario;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.viewTipoUsuario;

public class detalleEmpresario extends AppCompatActivity {

    private TextView txtDescripcion;
    private Button btnAceptar, btnInformacion;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private ConstraintLayout Fondo;
    private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detalle_empresario);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(Fondo, detalleEmpresario.this);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detalleEmpresario.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });

        btnInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detalleEmpresario.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(detalleEmpresario.this, viewTipoUsuario.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar() {
        btnAceptar = findViewById(R.id.bAceptar);
        btnInformacion = findViewById(R.id.bInformacion);
        txtDescripcion = findViewById(R.id.tvDescripcion);
        Fondo = findViewById(R.id.clFondo);

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProcesador = new procesador();
    }
}