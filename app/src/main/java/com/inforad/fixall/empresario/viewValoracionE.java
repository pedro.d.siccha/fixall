package com.inforad.fixall.empresario;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.inforad.fixall.R;
import com.inforad.fixall.procesos.procesador;

public class viewValoracionE extends AppCompatActivity {

    private static int SPLASH_SCREEN = 5000;
    Button botonAtras;
    private LinearLayout btnReferencia;
    private DrawerLayout Fondo;
    private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_valoracion_e);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(Fondo, viewValoracionE.this);

        btnReferencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewValoracionE.this, viewDescripcionTrabajo.class);
                startActivity(intent);
            }
        });

        botonAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewValoracionE.this, viewResultadoBusquedaE.class);
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        botonAtras = findViewById(R.id.btnVolver);
        btnReferencia = findViewById(R.id.llRefLaboral);
        Fondo = findViewById(R.id.drawer_layout);

        mProcesador = new procesador();
    }
}