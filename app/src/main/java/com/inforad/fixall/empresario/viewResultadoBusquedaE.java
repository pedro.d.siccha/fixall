package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.ofertaEnviadaAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.PalabraClave;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

public class viewResultadoBusquedaE extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private String nombre, apellido, telefono, palabraClave, idUsuario;
    private TextView txtNombre, txtApellido, txtPalabraClave;
    private RecyclerView recyclerListaBusqueda;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ofertaEnviadaAdaptador mOfertaAdaptador;
    private userProvider mUserProvider;
    private DrawerLayout fondo;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private procesador mProcesador;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_resultado_busqueda_e);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(fondo, viewResultadoBusquedaE.this);
        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewResultadoBusquedaE.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        txtNombre = findViewById(R.id.tvNombre);
        txtApellido = findViewById(R.id.tvApellido);
        txtPalabraClave = findViewById(R.id.tvPalabraClave);
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mUserProvider = new userProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        recyclerListaBusqueda = findViewById(R.id.recyclerListaResultadoBusqueda);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaBusqueda.setLayoutManager(linearLayoutManager);
        cargarDatos();
        caracteristicasPlan();
        cargarMenu();
        mProcesador = new procesador();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewResultadoBusquedaE.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    private void cargarDatos() {
        telefono = getIntent().getStringExtra("telefono");
        palabraClave = getIntent().getStringExtra("palabraClave");
        txtPalabraClave.setText("Palbra Clave: " + palabraClave);

        mUserProvider.obtenerUsuario(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void caracteristicasPlan() {
        String idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    switch (snapshot.child("plan").getValue().toString()){
                        case "BASICO":
                            cargarFondoBasico();
                            break;
                        case "GOLD":
                            cargarFondoGold();
                            break;
                        case "BLACK":
                            cargarFondoBlack();
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void cargarFondoBasico() {
        bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.fondo);
        background = new BitmapDrawable(getResources(), bitmap);
        fondo.setBackground(background);
    }

    private void cargarFondoGold() {
        bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.fondogold);
        background = new BitmapDrawable(getResources(), bitmap);
        fondo.setBackground(background);
    }

    private void cargarFondoBlack() {
        bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.fondoblack);
        background = new BitmapDrawable(getResources(), bitmap);
        fondo.setBackground(background);
    }

    @Override
    protected void onStart() {
        super.onStart();
        String idUsuario = mAuth.getCurrentUser().getUid();

        Query query = mDatabase.child("PalabraClave_Usuario").child(palabraClave);
        FirebaseRecyclerOptions<PalabraClave> options = new FirebaseRecyclerOptions.Builder<PalabraClave>().setQuery(query, PalabraClave.class).build();
        mOfertaAdaptador = new ofertaEnviadaAdaptador(options, viewResultadoBusquedaE.this);
        recyclerListaBusqueda.setAdapter(mOfertaAdaptador);
        mOfertaAdaptador.startListening();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicio:
                break;
            case R.id.nav_buscar:
                Intent viewBuscar = new Intent(viewResultadoBusquedaE.this, viewPerfilEmpresario.class);
                viewBuscar.putExtra("idUsuario", mAuth.getCurrentUser().getUid());
                viewBuscar.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(viewBuscar);
                break;
            case R.id.nav_ofertas:
                Intent viewOfertas = new Intent(viewResultadoBusquedaE.this, viewOfertasRecibidas.class);
                startActivity(viewOfertas);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewResultadoBusquedaE.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_historial:
                Intent viewHist = new Intent(viewResultadoBusquedaE.this, viewHistprial.class);
                startActivity(viewHist);
                break;
            case  R.id.nav_cambio_perfilEmp:

                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewResultadoBusquedaE.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewResultadoBusquedaE.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewResultadoBusquedaE.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewResultadoBusquedaE.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;

        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}