package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.busquedaProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewLogin;
import com.inforad.fixall.viewSolgan;
import com.inforad.fixall.viewSubscripcion;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class viewLLegoProfesionalE extends AppCompatActivity {

    private Button botonProblema, btnPagar, btnBono;
    private FirebaseAuth mAuth;
    private String idUsuario, idProfesional, idBusqueda, palabraClave, costo;
    private TextView txtNombreEmp, txtApellidoEmp, txtPalabraClave, txtAliasPro;
    private CircleImageView imgPerfilPro;
    private userProvider mUserProvider;
    private busquedaProvider mBusquedaProvider;
    private DrawerLayout fondo;
    private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_l_lego_profesional_e);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(fondo, viewLLegoProfesionalE.this);

        botonProblema.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewLLegoProfesionalE.this, viewReportarProblemaE.class);
                intent.putExtra("idProfesional", idProfesional);
                intent.putExtra("idBusqueda", idBusqueda);
                intent.putExtra("palabraClave", palabraClave);
                startActivity(intent);
            }
        });

        btnPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBusquedaProvider.obtenerBusqueda(palabraClave, idBusqueda).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            Intent intent = new Intent(viewLLegoProfesionalE.this, viewTarjetaP.class);
                            intent.putExtra("idProfesional", idProfesional);
                            intent.putExtra("idBusqueda", idBusqueda);
                            intent.putExtra("palabraClave", palabraClave);
                            intent.putExtra("costo", btnPagar.getText().toString());
                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });


            }
        });

    }

    private void Iniciar() {
        botonProblema = findViewById(R.id.btnProblemaServicio);
        txtPalabraClave = findViewById(R.id.tvPalabraClave);
        txtAliasPro = findViewById(R.id.tvAliasPro);
        imgPerfilPro = findViewById(R.id.ivImgProfesional);
        btnPagar = findViewById(R.id.bPagar);
        fondo = findViewById(R.id.drawer_layout);
        btnBono = findViewById(R.id.bBono);


        mBusquedaProvider = new busquedaProvider();
        mUserProvider = new userProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProcesador = new procesador();

        obtenerDatos();

        mBusquedaProvider.obtenerBusqueda(palabraClave, idBusqueda).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){

                    mProcesador.caracteristicasBono(btnBono, btnPagar, Double.parseDouble(snapshot.child("costo").getValue().toString()));

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void obtenerDatos() {
        idProfesional = getIntent().getStringExtra("idProfesional");
        idBusqueda = getIntent().getStringExtra("idBusqueda");
        palabraClave = getIntent().getStringExtra("palabraClave");

        txtPalabraClave.setText("Tu: " + palabraClave);
        mUserProvider.obtenerUsuario(idProfesional).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    txtAliasPro.setText(snapshot.child("alias").getValue().toString());
                    Picasso.with(viewLLegoProfesionalE.this).load(img).into(imgPerfilPro);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        mBusquedaProvider.obtenerBusqueda(palabraClave, idBusqueda).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtAliasPro.setText(snapshot.child("aliasProfesional").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}