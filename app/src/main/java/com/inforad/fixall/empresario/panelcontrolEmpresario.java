package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Layout;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.MainActivity;
import com.inforad.fixall.R;
import com.inforad.fixall.agregarDireccion;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

public class panelcontrolEmpresario extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private userProvider mUserProvider;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private TextView txtNombre;
    private CircleImageView imgPerfil;
    private Button btnCambiarModo, btnBuscar, btnHistorial, btnSolicitudes, btnDescuentos;
    private procesador mProcesador;
    private authProvider mAuthProvider;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_panelcontrol_empresario);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, panelcontrolEmpresario.this);

        btnCambiarModo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            String estado = snapshot.child("actividad").getValue().toString();
                            if (estado.equals("FinConfig")){
                                Intent intent = new Intent(panelcontrolEmpresario.this, viewPerfilP.class);
                                startActivity(intent);
                            }else{
                                Intent intent = new Intent(panelcontrolEmpresario.this, panelcontrolProfesional.class);
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(panelcontrolEmpresario.this, viewPerfilEmpresario.class);
                intent.putExtra("idUsuario", idUsuario);
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
            }
        });

        btnHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(panelcontrolEmpresario.this, viewHistprial.class);
                startActivity(intent);
            }
        });

        btnSolicitudes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(panelcontrolEmpresario.this, viewOfertasRecibidas.class);
                startActivity(intent);
            }
        });

        btnDescuentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(panelcontrolEmpresario.this, viewDescuento.class);
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(panelcontrolEmpresario.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(panelcontrolEmpresario.this, viewTipoUsuario.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar() {
        txtNombre = findViewById(R.id.tvAlias);
        btnCambiarModo = findViewById(R.id.bCambiarModo);
        btnBuscar = findViewById(R.id.bBuscar);
        btnHistorial = findViewById(R.id.bHistorial);
        btnSolicitudes = findViewById(R.id.bSolicitudesPendientes);
        btnDescuentos = findViewById(R.id.bDescuentos);
        imgPerfil = findViewById(R.id.civPerfil);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mUserProvider = new userProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProcesador = new procesador();
        mAuthProvider = new authProvider();

        cargarDatos();
        cargarMenu();

    }

    private void cargarMenu() {

        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);

        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);
        mProcesador.carcateristicaMenu(navigationView);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);

                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(panelcontrolEmpresario.this, viewTipoUsuario.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void cargarDatos() {
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    Picasso.with(panelcontrolEmpresario.this).load(snapshot.child("imagen").getValue().toString()).into(imgPerfil);
                    txtNombre.setText(snapshot.child("alias").getValue().toString());
                    String plan = snapshot.child("plan").getValue().toString();
                    if (plan.equals("BLACK")){
                        btnDescuentos.setVisibility(View.VISIBLE);
                    }else {
                        btnDescuentos.setVisibility(View.GONE);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.nav_inicio:
                break;
            case R.id.nav_buscar:

                Intent viewBuscar = new Intent(panelcontrolEmpresario.this, viewPerfilEmpresario.class);
                viewBuscar.putExtra("idUsuario", idUsuario);
                viewBuscar.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(viewBuscar);
                break;
            case R.id.nav_ofertas:
                Intent intent = new Intent(panelcontrolEmpresario.this, viewOfertasRecibidas.class);
                startActivity(intent);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(panelcontrolEmpresario.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_historial:
                Intent viewHist = new Intent(panelcontrolEmpresario.this, viewHistprial.class);
                startActivity(viewHist);
                break;
            case  R.id.nav_cambio_perfilEmp:
                mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            String estado = snapshot.child("actividad").getValue().toString();
                            if (estado.equals("FinConfig")){
                                Intent modoProfesional = new Intent(panelcontrolEmpresario.this, viewPerfilP.class);
                                startActivity(modoProfesional);
                            }else{
                                Intent modoProfesional = new Intent(panelcontrolEmpresario.this, panelcontrolProfesional.class);
                                startActivity(modoProfesional);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                break;
            case R.id.nav_banco:

                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(panelcontrolEmpresario.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(panelcontrolEmpresario.this, viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(panelcontrolEmpresario.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_favoritos:
                Intent viewFavorito = new Intent(panelcontrolEmpresario.this, viewListaFavorito.class);
                startActivity(viewFavorito);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(panelcontrolEmpresario.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                mProcesador.accesoClub(panelcontrolEmpresario.this);
                Intent viewClub = new Intent(panelcontrolEmpresario.this, clubEmpresario.class);
                startActivity(viewClub);
                break;
            case R.id.nav_logoit:
                mAuthProvider.cerrarSesion();
                Intent viewCerrar = new Intent(panelcontrolEmpresario.this, MainActivity.class);
                startActivity(viewCerrar);
                finish();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }

}