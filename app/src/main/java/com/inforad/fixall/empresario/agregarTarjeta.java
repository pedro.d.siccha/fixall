package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.TarjetaCredito;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.provider.tajertaCreditoProvider;
import com.inforad.fixall.utilidades.DatePickerFragment;
import com.inforad.fixall.viewTipoUsuario;

import pl.droidsonroids.gif.GifImageView;

public class agregarTarjeta extends AppCompatActivity {

    private String tipoUsuario, idUsuario;
    private EditText inputNumTarjeta, inputFecha, inputCodigo;
    private Button btnPagar;
    private tajertaCreditoProvider mTarjetaProvider;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private ConstraintLayout Fondo;
    private procesador mProcesador;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_agregar_tarjeta);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(Fondo, agregarTarjeta.this);

        inputFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });

        btnPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarTarjeta();
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(agregarTarjeta.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void guardarTarjeta() {
        String idTarjeta = mDatabase.push().getKey();
        TarjetaCredito tarjeta = new TarjetaCredito(idTarjeta, inputCodigo.getText().toString(), inputNumTarjeta.getText().toString(), inputFecha.getText().toString(), "ACTIVO");
        mTarjetaProvider.crearTarjeta(tarjeta, idUsuario, idTarjeta).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (tipoUsuario == "EMPRESARIO"){
                    Intent intent = new Intent(agregarTarjeta.this, panelcontrolEmpresario.class);
                    intent.putExtra("tipoUsuario", tipoUsuario);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = new Intent(agregarTarjeta.this, panelcontrolProfesional.class);
                    intent.putExtra("tipoUsuario", tipoUsuario);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void showDatePickerDialog() {

        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int anio, int mes, int dia) {
                final String selectedDate = (mes + 1) + "/" + anio;
                inputFecha.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void Iniciar() {

        btnAsistente = findViewById(R.id.givAvatarVaron);
        inputNumTarjeta = findViewById(R.id.txtNumero);
        inputFecha = findViewById(R.id.txtFecha);
        inputCodigo = findViewById(R.id.txtCodigo);
        btnPagar = findViewById(R.id.btnRealizarPago);
        Fondo = findViewById(R.id.clFondo);

        mTarjetaProvider = new tajertaCreditoProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mProcesador = new procesador();

        obtenerDatos();
    }

    private void obtenerDatos() {
        tipoUsuario = getIntent().getStringExtra("tipoUsuario");
    }
}