package com.inforad.fixall.empresario;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.procesos.procesador;

import pl.droidsonroids.gif.GifImageView;

public class viewDescripcionTrabajo extends AppCompatActivity {

    private Button btnRegresar;
    private ConstraintLayout Fondo;
    private procesador mProcesador;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_descripcion_trabajo);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(Fondo, viewDescripcionTrabajo.this);


        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewDescripcionTrabajo.this, viewValoracionE.class);
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewDescripcionTrabajo.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        btnRegresar = findViewById(R.id.btnVolver);
        Fondo = findViewById(R.id.clFondo);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mProcesador = new procesador();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
    }
}