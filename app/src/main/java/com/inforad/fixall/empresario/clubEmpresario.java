package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.usuarioAdapater;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.User;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.clubProfesional;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

import pl.droidsonroids.gif.GifImageView;

public class clubEmpresario extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ImageView btnAtras;
    private FirebaseAuth mAuth;
    private procesador mProcesador;
    private userProvider mUserProvider;
    private DatabaseReference mDatabase;
    private String idUsuario;
    private usuarioAdapater mUsuarioAdaptador;
    private RecyclerView recyclerListaUsuario;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_club_empresario);
        Inicio();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, clubEmpresario.this);

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(clubEmpresario.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Inicio() {
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mAuth = FirebaseAuth.getInstance();
        mProcesador = new procesador();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        recyclerListaUsuario = findViewById(R.id.rvPerfil);
        LinearLayoutManager linearLayoutManagerUsuario = new LinearLayoutManager(this);
        recyclerListaUsuario.setLayoutManager(linearLayoutManagerUsuario);
        mUserProvider = new userProvider();
        cargarMenu();
    }

    private void cargarMenu() {
        drawerLayout = findViewById(R.id.drawer_layout);
        btnAtras = findViewById(R.id.ivAtras);

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(clubEmpresario.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                /*
                String plan = snapshot.child("plan").getValue().toString();
                Query queryUsuario = mDatabase.child("Users").orderByChild("plan").equalTo(plan);
                FirebaseRecyclerOptions<User> options = new FirebaseRecyclerOptions.Builder<User>().setQuery(queryUsuario, User.class).build();
                mUsuarioAdaptador = new usuarioAdapater(options, clubEmpresario.this);
                recyclerListaUsuario.setAdapter(mUsuarioAdaptador);
                mUsuarioAdaptador.startListening();

                 */
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}