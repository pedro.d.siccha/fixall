package com.inforad.fixall.empresario;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.inforad.fixall.R;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.viewLogin;
import com.inforad.fixall.viewSolgan;

public class viewPagarProfesionalE extends AppCompatActivity {

    private Button botonPagar;
    private DrawerLayout Fondo;
    private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_pagar_profesional_e);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(Fondo, viewPagarProfesionalE.this);

        botonPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewPagarProfesionalE.this, viewTarjetaP.class);
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        botonPagar = findViewById(R.id.btnPagar);
        Fondo = findViewById(R.id.dlFondo);

        mProcesador = new procesador();
    }
}