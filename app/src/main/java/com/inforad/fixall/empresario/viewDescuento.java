package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.subirFlyer;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;

import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

public class viewDescuento extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private TextView txtBarra;
    private SeekBar jbSeecker;
    private Button btnBuscar;
    private String intRango;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private FirebaseAuth mAuth;
    private procesador mProcesador;
    private Spinner cbPorcentaje, cbOfertas;
    private CircleImageView imgFlyer;
    private GifImageView btnAsistente;
    private String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_descuento);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewDescuento.this);
        mProcesador.mostrarComboOfertas(cbOfertas, viewDescuento.this);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewDescuento.this, viewListaOfertas.class);
                startActivity(intent);
            }
        });

        imgFlyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewDescuento.this, subirFlyer.class);
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewDescuento.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(getApplicationContext(), panelcontrolEmpresario.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar() {
        txtBarra = findViewById(R.id.tvRango);
        jbSeecker = findViewById(R.id.seekBar);
        btnBuscar = findViewById(R.id.bBuscar);
        imgFlyer = findViewById(R.id.civFlyer);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        cbPorcentaje = findViewById(R.id.spPorcentaje);
        String[] ubicacion = {"LISTA DE PORCENTAJES DE DESCUENTO", "5%", "10%", "15%", "20%", "30%", "40%", "50%"};
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(ubicacion));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.style_spinner, arrayList);
        cbPorcentaje.setAdapter(arrayAdapter);

        cbOfertas = findViewById(R.id.spOfertas);

        mProcesador = new procesador();

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();

        jbSeecker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progreso, boolean b) {
                txtBarra.setText(progreso + "/200 m");
                intRango = String.valueOf(progreso);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewDescuento.this, panelcontrolEmpresario.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicio:
                break;
            case R.id.nav_buscar:
                Intent viewBuscar = new Intent(viewDescuento.this, viewPerfilEmpresario.class);
                viewBuscar.putExtra("idUsuario", mAuth.getCurrentUser().getUid());
                viewBuscar.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(viewBuscar);
                break;
            case R.id.nav_ofertas:
                Intent viewOfertas = new Intent(viewDescuento.this, viewOfertasRecibidas.class);
                startActivity(viewOfertas);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewDescuento.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_historial:
                Intent viewHist = new Intent(viewDescuento.this, viewHistprial.class);
                startActivity(viewHist);
                break;
            case  R.id.nav_cambio_perfilEmp:

                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewDescuento.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewDescuento.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewDescuento.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewDescuento.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;

        }

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }
}