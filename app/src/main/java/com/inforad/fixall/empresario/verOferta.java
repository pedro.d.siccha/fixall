package com.inforad.fixall.empresario;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.view.WindowManager;

import com.inforad.fixall.R;
import com.inforad.fixall.procesos.procesador;

public class verOferta extends AppCompatActivity {

    private ConstraintLayout Fondo;
    private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ver_oferta);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(Fondo, verOferta.this);
    }

    private void Iniciar() {
        Fondo = findViewById(R.id.clFondo);

        mProcesador = new procesador();
    }
}