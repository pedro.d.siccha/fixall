package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.Contacto;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.model.Mensaje;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.buscarProvider;
import com.inforad.fixall.provider.busquedaProvider;
import com.inforad.fixall.provider.chatProvider;
import com.inforad.fixall.provider.contactoProvider;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.ofertaProvider;
import com.inforad.fixall.provider.tokenProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.utilidades.DatePickerFragment;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class viewAceptarProfesionalE extends AppCompatActivity {

    private Button botonAhora, botonFecha, btnHora, bGuardar;
    private CircleImageView imgPerfilProfesional;
    private TextView txtPalabraClave, txtNombreProfesional, txtTelefonoProfesional, txtCostoProfesional, txtHoraFecha;
    private String idProfesional, idBusqueda, idUsuario, palabraClave, costo, aliasProfesional, imgProfesional, distancia;
    private FirebaseAuth mAuth;
    private userProvider mUserProvider;
    private busquedaProvider mBusquedaProvider;
    private ofertaProvider mOfertaProvider;
    private notificacionProvider mNotificacion;
    private tokenProvider mTokenProvider;
    private contactoProvider mContactoProvider;
    private DrawerLayout fondo;
    private procesador mProcesador;
    private Spinner cbDireccion;
    private GifImageView btnAsistente;
    private chatProvider mChatProvider;
    private buscarProvider mBuscarProvider;

    //Nuevo

    private static final String CERO = "0";
    private static final String DOS_PUNTOS = ":";

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la hora hora
    final int hora = c.get(Calendar.HOUR_OF_DAY);
    final int minuto = c.get(Calendar.MINUTE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_aceptar_profesional_e);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(fondo, viewAceptarProfesionalE.this);
        mProcesador.mostrarComboDireccion(cbDireccion, viewAceptarProfesionalE.this);

        botonAhora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOfertaProvider.actualizarOferta(idUsuario, idProfesional, "ACEPTADO").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        final Calendar calendario = Calendar.getInstance();
                        int anio = calendario.get(Calendar.YEAR);
                        int mes = calendario.get(Calendar.MONTH);
                        int dia = calendario.get(Calendar.DAY_OF_MONTH);
                        String fecActual = String.valueOf(dia) + "/" + String.valueOf(mes + 1) + "/" + String.valueOf(anio);
                        mBusquedaProvider.actualizarBusqueda(palabraClave, idBusqueda, idProfesional, aliasProfesional, imgProfesional, "", Double.parseDouble(costo), "ACEPTADO", fecActual).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                enviarNotificacion(fecActual);
                                Intent intent = new Intent(viewAceptarProfesionalE.this, viewConfirmacionE.class);
                                intent.putExtra("fecha", fecActual);
                                intent.putExtra("idProfesional", idProfesional);
                                intent.putExtra("palabraClave", palabraClave);
                                startActivity(intent);
                                finish();
                                /*
                                mBuscarProvider.obtener(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if (snapshot.exists()){
                                            String id = snapshot.child("id").getValue().toString();
                                            mBuscarProvider.actualizarEstado(idUsuario, id).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    enviarNotificacion(fecActual);
                                                    Intent intent = new Intent(viewAceptarProfesionalE.this, viewConfirmacionE.class);
                                                    intent.putExtra("fecha", fecActual);
                                                    intent.putExtra("idProfesional", idProfesional);
                                                    intent.putExtra("palabraClave", palabraClave);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });
                                */
                            }
                        });
                    }
                });
            }
        });

        botonFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.btnFecha:
                        showDatePickerDialog();
                        break;
                }
            }
        });

        btnHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerHora();
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewAceptarProfesionalE.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

        bGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnHora.getText().toString().isEmpty()){
                    Toast.makeText(viewAceptarProfesionalE.this, "Elegir una hora", Toast.LENGTH_SHORT).show();
                }

                if (botonFecha.getText().toString().isEmpty()){
                    Toast.makeText(viewAceptarProfesionalE.this, "Elegir una fecha", Toast.LENGTH_SHORT).show();
                }
                mOfertaProvider.actualizarOferta(idUsuario, idProfesional, "ACEPTADO").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mBusquedaProvider.actualizarBusqueda(palabraClave, idBusqueda, idProfesional, aliasProfesional, imgProfesional, "", Double.parseDouble(costo), "ACEPTADO", botonFecha.getText().toString() + " " + btnHora.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                enviarNotificacion(botonFecha.getText().toString());
                                Intent intent = new Intent(viewAceptarProfesionalE.this, viewConfirmacionE.class);
                                intent.putExtra("fecha", botonFecha.getText().toString());
                                intent.putExtra("idProfesional", idProfesional);
                                intent.putExtra("palabraClave", palabraClave);
                                startActivity(intent);
                                finish();
                                /*
                                mBuscarProvider.obtener(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if (snapshot.exists()){
                                            String id = snapshot.child("id").getValue().toString();
                                            mBuscarProvider.actualizarEstado(idUsuario, id).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    enviarNotificacion(botonFecha.getText().toString());
                                                    Intent intent = new Intent(viewAceptarProfesionalE.this, viewConfirmacionE.class);
                                                    intent.putExtra("fecha", botonFecha.getText().toString());
                                                    intent.putExtra("idProfesional", idProfesional);
                                                    intent.putExtra("palabraClave", palabraClave);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });
*/
                            }

                        });
                    }
                });
            }
        });

    }

    private void obtenerHora() {
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                btnHora.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
                txtHoraFecha.setText(btnHora.getText().toString() + " - " + botonFecha.getText().toString());
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();

    }

    private void showDatePickerDialog() {

        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int anio, int mes, int dia) {
                final String selectedDate = dia + "/" + (mes + 1) + "/" + anio;
                botonFecha.setText(selectedDate);
                txtHoraFecha.setText(btnHora.getText().toString() + " - " + selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void Iniciar() {

        imgPerfilProfesional = findViewById(R.id.ivPerfilPro);
        txtPalabraClave = findViewById(R.id.tvPalabraClave);
        txtNombreProfesional = findViewById(R.id.tvNomPro);
        txtTelefonoProfesional = findViewById(R.id.tvTelefono);
        txtHoraFecha = findViewById(R.id.tvFechaHora);
        txtCostoProfesional = findViewById(R.id.tvCosto);
        botonAhora = findViewById(R.id.btnAhora);
        botonFecha = findViewById(R.id.btnFecha);
        bGuardar = findViewById(R.id.btnGuardar);
        fondo = findViewById(R.id.drawer_layout);
        cbDireccion = findViewById(R.id.sDireccion);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        btnHora = findViewById(R.id.btnHora);

        mContactoProvider = new contactoProvider();
        mOfertaProvider = new ofertaProvider();
        mAuth = FirebaseAuth.getInstance();
        mUserProvider = new userProvider();
        mBusquedaProvider = new busquedaProvider();
        mTokenProvider = new tokenProvider();
        mNotificacion = new notificacionProvider();
        mProcesador = new procesador();
        mChatProvider = new chatProvider();
        mBuscarProvider = new buscarProvider();

        obtenerDatos();
        cargarDatos();
        crearChat();
    }

    private void obtenerDatos() {
        idProfesional = getIntent().getStringExtra("idProfesional");
        idBusqueda = getIntent().getStringExtra("idBusqueda");
        palabraClave = getIntent().getStringExtra("palabraClave");
        costo = getIntent().getStringExtra("costo");
        distancia = getIntent().getStringExtra("distancia");
        idUsuario = mAuth.getCurrentUser().getUid();
    }

    private void cargarDatos(){
        txtTelefonoProfesional.setText("Distancia: " + distancia);
        mUserProvider.obtenerUsuario(idProfesional).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    aliasProfesional = snapshot.child("alias").getValue().toString();
                    imgProfesional = snapshot.child("imagen").getValue().toString();
                    txtNombreProfesional.setText(snapshot.child("nombre").getValue().toString() + " " + snapshot.child("apellido").getValue().toString());

                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(viewAceptarProfesionalE.this).load(img).into(imgPerfilProfesional);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        txtPalabraClave.setText("Felicidades, ya tiene su " + palabraClave);
        txtCostoProfesional.setText("S/. " + costo);

    }

    private void enviarNotificacion(String fec){

        mTokenProvider.obtenerToken(idProfesional).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String token = snapshot.child("token").getValue().toString();
                    mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                String empresario = snapshot.child("alias").getValue().toString();
                                Map<String, String> map = new HashMap<>();
                                map.put("titulo", "OFERTA ACEPTADA");
                                map.put("contenido", "Su oferta para: " + palabraClave + "\nCliente: " + empresario + "\nCosto aceptado: " + costo + ". \nCita: " + fec);
                                map.put("imagen", imgProfesional);
                                FCMBody fcmBody = new FCMBody(token, "high", "4500s", map);
                                mNotificacion.sendNotification(fcmBody).enqueue(new Callback<FCMResponse>() {
                                    @Override
                                    public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                                        if (response.body() != null){
                                            if (response.body().getSuccess() == 1){

                                            }else {

                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<FCMResponse> call, Throwable t) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void crearChat() {
        Random random = new Random();
        int n = random.nextInt(10000);
        mChatProvider.obtenerOrdenUsuarios(idUsuario, idProfesional, viewAceptarProfesionalE.this, n);
    }
}