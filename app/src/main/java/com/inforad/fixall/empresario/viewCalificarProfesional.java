package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.model.HistorialBusqueda;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.busquedaProvider;
import com.inforad.fixall.provider.historialBusquedaProvider;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.tokenProvider;
import com.inforad.fixall.provider.userProvider;

//import java.security.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class viewCalificarProfesional extends AppCompatActivity{

    private CircleImageView imgPerfil;
    private TextView txtNombre;
    private Button btnEncanta, btnLike, btnDislike, btnFinalizar;
    private tokenProvider mTokenProvider;
    private notificacionProvider mNotificacion;
    private String idProfesional, palabraClave, idBusqueda, costo, valoracion = "", imagenUsuario;
    private busquedaProvider mBusquedaProvider;
    private historialBusquedaProvider mHistorialProvider;
    private DatabaseReference mDatabase;
    private DrawerLayout Fondo;
    private procesador mProcesador;
    private FirebaseAuth mAuth;
    private userProvider mUserProvider;
    private GifImageView btnAsistente;
    private String idUsuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_calificar_profesional);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(Fondo, viewCalificarProfesional.this);

        btnEncanta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(viewCalificarProfesional.this, "Me encantó el Servicio", Toast.LENGTH_SHORT).show();
                valoracion = "ME ENCANTA";
                finalizar(valoracion);
            }
        });

        btnDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(viewCalificarProfesional.this, "Disconforme con el Servicio", Toast.LENGTH_SHORT).show();
                valoracion = "DISLIKE";
                finalizar(valoracion);
            }
        });

        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(viewCalificarProfesional.this, "Buen Servicio", Toast.LENGTH_SHORT).show();
                valoracion = "LIKE";
                finalizar(valoracion);
            }
        });

        btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewCalificarProfesional.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void finalizar(String val){
        String key = mDatabase.child("POST").push().getKey();
        enviarNotificacion();

        mBusquedaProvider.generarIdHistorialBusqueda(palabraClave, idBusqueda).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                mBusquedaProvider.obtenerBusqueda(palabraClave, idBusqueda).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        String idHistorial = snapshot.child("idHistorialBusqueda").getValue().toString();
                        String palabraClave = snapshot.child("palabraClave").getValue().toString();
                        String descripcion = snapshot.child("descripcion").getValue().toString();
                        String imagenDescripcion = snapshot.child("imagenDescripcion").getValue().toString();
                        //String rangoBusqueda = snapshot.child("").getValue().toString();
                        String estado = snapshot.child("estado").getValue().toString();
                        String idUsuario = snapshot.child("idUsuario").getValue().toString();
                        String aliasUsuario = snapshot.child("aliasUsuario").getValue().toString();
                        String imgUsuario = snapshot.child("imgUsuario").getValue().toString();
                        String idDirUsuario = snapshot.child("idDirProfesional").getValue().toString();
                        String idProfesional = snapshot.child("idProfesional").getValue().toString();
                        String aliasProfesional = snapshot.child("aliasProfesional").getValue().toString();
                        String imgProfesional = snapshot.child("imgProfesional").getValue().toString();
                        String idDirProfesional = snapshot.child("idDirProfesional").getValue().toString();
                        String horaCita = snapshot.child("horaCita").getValue().toString();
                        double costo = Double.parseDouble(snapshot.child("costo").getValue().toString());
                        Date date = new Date();
                        HistorialBusqueda historial = new HistorialBusqueda(idHistorial, palabraClave, descripcion, imagenDescripcion, "", estado, idUsuario, aliasUsuario, imgUsuario, idDirUsuario, idProfesional, aliasProfesional, imgProfesional, idDirProfesional, valoracion, horaCita, costo, val, "", date.getTime());
                        mHistorialProvider.crearHistorial(historial).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent intent = new Intent(viewCalificarProfesional.this, panelcontrolEmpresario.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
    }

    private void Iniciar() {
        imgPerfil = findViewById(R.id.civPerfilEmp);
        txtNombre = findViewById(R.id.tvAliEmpresario);
        btnEncanta = findViewById(R.id.bMeEncanta);
        btnLike = findViewById(R.id.bLike);
        btnDislike = findViewById(R.id.bDisLike);
        btnFinalizar = findViewById(R.id.bFinal);
        Fondo = findViewById(R.id.drawer_layout);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mTokenProvider = new tokenProvider();
        mNotificacion = new notificacionProvider();
        mBusquedaProvider = new busquedaProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mHistorialProvider = new historialBusquedaProvider();
        mProcesador = new procesador();
        mAuth = FirebaseAuth.getInstance();
        mUserProvider = new userProvider();
        idUsuario = mAuth.getCurrentUser().getUid();

        cargarDatos();

    }

    private void cargarDatos() {
        idProfesional = getIntent().getStringExtra("idProfesion");
        palabraClave = getIntent().getStringExtra("palabraClave");
        idBusqueda = getIntent().getStringExtra("idBusqueda");
        costo = getIntent().getStringExtra("costo");

        mUserProvider.obtenerUsuario(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                imagenUsuario = snapshot.child("imagen").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void enviarNotificacion() {
        mTokenProvider.obtenerToken(idProfesional).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String token = snapshot.child("token").getValue().toString();
                    Map<String, String> map = new HashMap<>();
                    map.put("titulo", "CALIFICACIÓN RECIBIDA");
                    map.put("contenido", "La calificacion del servicio: " + palabraClave + "\nAcaba de llegar, toque para realizar la calificación al empresario");
                    map.put("imagen", imagenUsuario);
                    FCMBody fcmBody = new FCMBody(token, "high", "4500s", map);
                    mNotificacion.sendNotification(fcmBody).enqueue(new Callback<FCMResponse>() {
                        @Override
                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                            if (response.body() != null){
                                if (response.body().getSuccess() == 1){
                                    //Toast.makeText(viewCalificarProfesional.this, "Gracias por su colaboración", Toast.LENGTH_SHORT).show();
                                }else {
                                    //Toast.makeText(viewCalificarProfesional.this, "No se pudo enviar la notificación", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<FCMResponse> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}