package com.inforad.fixall.empresario;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.inforad.fixall.R;

public class sinOfertas extends AppCompatActivity {

    private TextView txtTitulo;
    private Button btnAtras;
    private String titulo, idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sin_ofertas);
        Iniciar();

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(sinOfertas.this, viewPerfilEmpresario.class);
                intent.putExtra("idUsuario", idUsuario);
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        txtTitulo = findViewById(R.id.tvTitulo);
        btnAtras = findViewById(R.id.bVolver);

        titulo = getIntent().getStringExtra("palabraClave");
        idUsuario = getIntent().getStringExtra("idUsuario");

        txtTitulo.setText("NO SE ENCONTRÓ " + titulo + "CERCA");
    }
}