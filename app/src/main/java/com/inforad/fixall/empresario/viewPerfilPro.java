package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.profesionAdaptador;
import com.inforad.fixall.adapters.trabajoAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.ProfesionProfesional;
import com.inforad.fixall.model.Trabajo;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

public class viewPerfilPro extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private CircleImageView imgPerfil;
    private TextView txtAlias, txtNombre, txtEdad, txtDescripcion;
    private RecyclerView listaProfesion, listaTrabajo, listaUniversidad;
    private Button btnVolver;
    private String idProfesional, idUsuario;
    private userProvider mUsuarioProvider;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private profesionAdaptador mProfesionAdaptador;
    private trabajoAdaptador mTrabajoAdaptador;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private ConstraintLayout fondo;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private procesador mProcesador;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_perfil_pro);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewPerfilPro.this);

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewPerfilPro.this, viewOfertasRecibidas.class);
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewPerfilPro.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });


    }

    private void Iniciar(){
        imgPerfil = findViewById(R.id.ivPerfil);
        txtAlias = findViewById(R.id.tvAlias);
        txtNombre = findViewById(R.id.tvNombre);
        txtEdad = findViewById(R.id.tvEdad);
        txtDescripcion = findViewById(R.id.tvDescripcion);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        listaUniversidad = findViewById(R.id.recyclerListaUniversidad);
        LinearLayoutManager linearLayoutManagerU = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        listaUniversidad.setLayoutManager(linearLayoutManagerU);
        listaProfesion = findViewById(R.id.recyclerListaProfesion);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listaProfesion.setLayoutManager(linearLayoutManager);
        listaTrabajo = findViewById(R.id.recyclerListaTrabajo);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this);
        listaTrabajo.setLayoutManager(linearLayoutManager1);
        btnVolver = findViewById(R.id.bVolverOfertas);

        mUsuarioProvider = new userProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProcesador = new procesador();
        obtenerDatos();
        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewPerfilPro.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    private void obtenerDatos() {

        idProfesional = getIntent().getStringExtra("idProfesional");
        mUsuarioProvider.obtenerUsuario(idProfesional).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtAlias.setText(snapshot.child("alias").getValue().toString());
                    txtNombre.setText(snapshot.child("nombre").getValue().toString() + " " + snapshot.child("apellido").getValue().toString());
                    txtEdad.setText(snapshot.child("edad").getValue().toString());
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(viewPerfilPro.this).load(img).into(imgPerfil);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryProfesion = mDatabase.child("Profesion_Profesional").child(idProfesional);
        FirebaseRecyclerOptions<ProfesionProfesional> options = new FirebaseRecyclerOptions.Builder<ProfesionProfesional>().setQuery(queryProfesion, ProfesionProfesional.class).build();
        mProfesionAdaptador = new profesionAdaptador(options, viewPerfilPro.this);
        listaProfesion.setAdapter(mProfesionAdaptador);
        mProfesionAdaptador.startListening();

        Query queryTrabajo = mDatabase.child("Trabajo").child(idProfesional);
        FirebaseRecyclerOptions<Trabajo> options1 = new FirebaseRecyclerOptions.Builder<Trabajo>().setQuery(queryTrabajo, Trabajo.class).build();
        mTrabajoAdaptador = new trabajoAdaptador(options1, viewPerfilPro.this);
        listaTrabajo.setAdapter(mTrabajoAdaptador);
        mTrabajoAdaptador.startListening();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicio:
                break;
            case R.id.nav_buscar:
                Intent viewBuscar = new Intent(viewPerfilPro.this, viewPerfilEmpresario.class);
                viewBuscar.putExtra("idUsuario", mAuth.getCurrentUser().getUid());
                viewBuscar.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(viewBuscar);
                break;
            case R.id.nav_ofertas:
                Intent viewOfertas = new Intent(viewPerfilPro.this, viewOfertasRecibidas.class);
                startActivity(viewOfertas);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewPerfilPro.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_historial:
                Intent viewHist = new Intent(viewPerfilPro.this, viewHistprial.class);
                startActivity(viewHist);
                break;
            case  R.id.nav_cambio_perfilEmp:

                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewPerfilPro.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewPerfilPro.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewPerfilPro.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewPerfilPro.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;

        }

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }
}