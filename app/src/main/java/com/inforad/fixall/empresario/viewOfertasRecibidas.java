package com.inforad.fixall.empresario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.MainActivity;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.buscarAdaptador;
import com.inforad.fixall.adapters.busquedaAdaptador;
import com.inforad.fixall.adapters.ofertaAceptadaAdaptador;
import com.inforad.fixall.adapters.ofertaRecibidaAdaptador;
import com.inforad.fixall.adapters.palabraUsuarioAdaptador;
import com.inforad.fixall.adapters.profesionalLlegoAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.model.Buscar;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.model.UsuarioPalabra;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.viewCitaPendiente;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.provider.ofertaProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;

import pl.droidsonroids.gif.GifImageView;

public class viewOfertasRecibidas extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerListaOfertas, recyclerListaBusquedas;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ofertaRecibidaAdaptador mOfertaAdaptador;
    private userProvider mUserProvider;
    private String palabraClave;
    private String idUsuario;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private procesador mProcesador;
    private ofertaProvider mOfertaProvider;
    private profesionalLlegoAdaptador mProfesionalAdaptador;
    private ofertaAceptadaAdaptador mAceptarAdaptador;
    private authProvider mAuthProvider;
    private CheckBox banco;
    private buscarAdaptador mBuscarAdapter;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_ofertas_recibidas);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewOfertasRecibidas.this);
        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewOfertasRecibidas.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });
    }

    private void Iniciar() {
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider = new userProvider();
        recyclerListaOfertas = findViewById(R.id.recyclerListaOfertas);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaOfertas.setLayoutManager(linearLayoutManager);
        recyclerListaBusquedas = findViewById(R.id.recyclerListaBusquedas);
        LinearLayoutManager linearLayoutManagerBusquedas = new LinearLayoutManager(this);
        recyclerListaBusquedas.setLayoutManager(linearLayoutManagerBusquedas);

        mProcesador = new procesador();
        mOfertaProvider = new ofertaProvider();
        mAuthProvider = new authProvider();

        obtenerDatos();
        cargarMenu();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(getApplicationContext(), panelcontrolEmpresario.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);
        //banco = findViewById(R.id.cbBanco);
        mProcesador.carcateristicaMenu(navigationView);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void obtenerDatos() {
        palabraClave = getIntent().getStringExtra("palabraClave");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Query query = mDatabase.child("Buscar").child(idUsuario).orderByChild("estado").equalTo("PENDIENTE");
        FirebaseRecyclerOptions<Buscar> options = new FirebaseRecyclerOptions.Builder<Buscar>().setQuery(query, Buscar.class).build();
        mBuscarAdapter = new buscarAdaptador(options, viewOfertasRecibidas.this);
        recyclerListaBusquedas.setAdapter(mBuscarAdapter);
        mBuscarAdapter.startListening();

        mOfertaProvider.obtenerOferta(idUsuario).orderByChild("estado").equalTo("PENDIENTE").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    Query query = mDatabase.child("Oferta").child(idUsuario);
                    FirebaseRecyclerOptions<Oferta> options = new FirebaseRecyclerOptions.Builder<Oferta>().setQuery(query, Oferta.class).build();
                    mOfertaAdaptador = new ofertaRecibidaAdaptador(options, viewOfertasRecibidas.this);
                    recyclerListaOfertas.setAdapter(mOfertaAdaptador);
                    mOfertaAdaptador.startListening();
                }else{
                    mOfertaProvider.obtenerOferta(idUsuario).orderByChild("estado").equalTo("ACEPTADO").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                Query query = mDatabase.child("Oferta").child(idUsuario).orderByChild("estado").equalTo("ACEPTADO");
                                FirebaseRecyclerOptions<Oferta> options = new FirebaseRecyclerOptions.Builder<Oferta>().setQuery(query, Oferta.class).build();
                                mOfertaAdaptador = new ofertaRecibidaAdaptador(options, viewOfertasRecibidas.this);
                                recyclerListaOfertas.setAdapter(mOfertaAdaptador);
                                mOfertaAdaptador.startListening();
                            }else {
                                Query query = mDatabase.child("Oferta").child(idUsuario).orderByChild("estado").equalTo("LLEGO");
                                FirebaseRecyclerOptions<Oferta> options = new FirebaseRecyclerOptions.Builder<Oferta>().setQuery(query, Oferta.class).build();
                                mProfesionalAdaptador = new profesionalLlegoAdaptador(options, viewOfertasRecibidas.this);
                                recyclerListaOfertas.setAdapter(mProfesionalAdaptador);
                                mProfesionalAdaptador.startListening();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicio:
                Intent viewInicio = new Intent(viewOfertasRecibidas.this, panelcontrolEmpresario.class);
                startActivity(viewInicio);
                break;
            case R.id.nav_buscar:
                Intent viewBuscar = new Intent(viewOfertasRecibidas.this, viewPerfilEmpresario.class);
                viewBuscar.putExtra("idUsuario", idUsuario);
                viewBuscar.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(viewBuscar);
                break;
            case R.id.nav_ofertas:
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewOfertasRecibidas.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_historial:
                Intent viewHist = new Intent(viewOfertasRecibidas.this, viewHistprial.class);
                startActivity(viewHist);
                break;
            case  R.id.nav_cambio_perfilEmp:
                Intent modoProfesional = new Intent(viewOfertasRecibidas.this, viewPerfilP.class);
                startActivity(modoProfesional);
                break;
            case R.id.nav_banco:
                if (banco.isChecked()) {
                    //Toast.makeText(this, "BANCO", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(this, "PERSONA", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewOfertasRecibidas.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewOfertasRecibidas.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewOfertasRecibidas.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_favoritos:
                Intent viewFavorito = new Intent(viewOfertasRecibidas.this, viewListaFavorito.class);
                startActivity(viewFavorito);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewOfertasRecibidas.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                mProcesador.accesoClub(viewOfertasRecibidas.this);
                Intent viewClub = new Intent(viewOfertasRecibidas.this, clubEmpresario.class);
                startActivity(viewClub);
                break;
            case R.id.nav_logoit:
                mAuthProvider.cerrarSesion();
                Intent viewCerrar = new Intent(viewOfertasRecibidas.this, MainActivity.class);
                startActivity(viewCerrar);
                finish();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }
}