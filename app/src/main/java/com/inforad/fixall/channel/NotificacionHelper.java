package com.inforad.fixall.channel;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Message;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.Person;
import androidx.core.graphics.drawable.IconCompat;

import com.inforad.fixall.R;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.Mensaje;
import com.inforad.fixall.model.Oferta;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Date;

public class NotificacionHelper extends ContextWrapper {

    private static final String CHANNEL_ID = "com.inforad.fixall";
    private static final String CHANNEL_NAME = "FIX ALL";

    private NotificationManager manager;

    public NotificacionHelper(Context base) {
        super(base);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            createChannels();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createChannels(){
        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        notificationChannel.setLightColor(Color.GRAY);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        getManager().createNotificationChannel(notificationChannel);
    }

    public NotificationManager getManager(){
        if (manager == null){
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }

    public NotificationCompat.Builder getNotification(String titulo, String contenido){
        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setContentText(titulo).setContentText(contenido).setAutoCancel(true).setColor(Color.BLUE).setSmallIcon(R.drawable.logo).setStyle(new NotificationCompat.BigTextStyle().bigText(contenido).setBigContentTitle(titulo));
    }

    public NotificationCompat.Builder getNotificationMensaje(Mensaje[] mensajes, String aliasEnvia, String aliasRecive, Bitmap bitmapEnvia, NotificationCompat.Action accion){

        Person person2 = new Person.Builder().setName(aliasEnvia).setIcon(IconCompat.createWithBitmap(bitmapEnvia)).build();
        NotificationCompat.MessagingStyle mensajeStyle = new NotificationCompat.MessagingStyle(person2);

        for (Mensaje m: mensajes){
            NotificationCompat.MessagingStyle.Message mensaje2 = new NotificationCompat.MessagingStyle.Message(m.getMensaje(), m.getTimestamp(), person2);
            mensajeStyle.addMessage(mensaje2);
        }

        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setSmallIcon(R.drawable.logo).setStyle(mensajeStyle).addAction(accion);

    }

    public NotificationCompat.Builder obtenerNotificacionBusqueda(Busqueda[] busquedas, String aliasEmpresario, Bitmap bitmapEmpresario){
        Person person = new Person.Builder().setName(aliasEmpresario).setIcon(IconCompat.createWithBitmap(bitmapEmpresario)).build();
        NotificationCompat.MessagingStyle busquedaStyle = new NotificationCompat.MessagingStyle(person);

        for (Busqueda b: busquedas){
            NotificationCompat.MessagingStyle.Message busqueda2 = new NotificationCompat.MessagingStyle.Message(b.getAliasProfesional(), Long.parseLong(b.getDescripcion()), person);
            busquedaStyle.addMessage(busqueda2);
        }

        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setSmallIcon(R.drawable.logo).setStyle(busquedaStyle);

    }

    public NotificationCompat.Builder getNotificationOferta(Oferta[] ofertas, String aliasProfesional, String contenido, Bitmap imgProfesional){

        Person person = new Person.Builder().setName(aliasProfesional).setIcon(IconCompat.createWithBitmap(imgProfesional)).build();
        NotificationCompat.MessagingStyle ofertaStyle = new NotificationCompat.MessagingStyle(person);
        NotificationCompat.MessagingStyle.Message ofer = new NotificationCompat.MessagingStyle.Message(contenido, new Date().getTime(), person);
        ofertaStyle.addMessage(ofer);

        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setSmallIcon(R.drawable.logo).setStyle(ofertaStyle);

    }

    public NotificationCompat.Builder getNotificationAceptarOferta(String aliasEmpresario, String contenido, Bitmap imgEmpresario){
        Person person = new Person.Builder().setName(aliasEmpresario).setIcon(IconCompat.createWithBitmap(imgEmpresario)).build();
        NotificationCompat.MessagingStyle ofertaStyle = new NotificationCompat.MessagingStyle(person);
        NotificationCompat.MessagingStyle.Message ofer = new NotificationCompat.MessagingStyle.Message(contenido, new Date().getTime(), person);
        ofertaStyle.addMessage(ofer);

        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setSmallIcon(R.drawable.logo).setStyle(ofertaStyle);
    }

    public NotificationCompat.Builder getNotificationIcon(String titulo, String contenido, Bitmap img){
        Person person = new Person.Builder().setName(titulo).setIcon(IconCompat.createWithBitmap(img)).build();
        NotificationCompat.MessagingStyle ofertaStyle = new NotificationCompat.MessagingStyle(person);
        NotificationCompat.MessagingStyle.Message ofer = new NotificationCompat.MessagingStyle.Message(contenido, new Date().getTime(), person);
        ofertaStyle.addMessage(ofer);

        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setSmallIcon(R.drawable.logo).setStyle(ofertaStyle);
    }

/*
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createChannels(){
        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        notificationChannel.setLightColor(Color.GRAY);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        getManager().createNotificationChannel(notificationChannel);
    }

    public NotificationManager getManager(){
        if (manager == null){
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getNotification(String titulo, String contenido, PendingIntent intent, Uri sounUri) throws IOException {
        return new Notification.Builder(getApplicationContext(), CHANNEL_ID).setContentTitle(titulo).setContentText(contenido).setAutoCancel(true).setSound(sounUri).setContentIntent(intent).setSmallIcon(R.drawable.logo).setStyle(new Notification.BigTextStyle().bigText(contenido).setBigContentTitle(titulo));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getNotificationAccion(String titulo, String contenido, Uri sounUri, Notification.Action aceptarAccion, String imagen) throws IOException {
        Bitmap img_foto = Picasso.with(getApplicationContext()).load(imagen).get();
        return new Notification.Builder(getApplicationContext(), CHANNEL_ID).setContentTitle(titulo).setContentText(contenido).setAutoCancel(true).setSound(sounUri).setSmallIcon(R.drawable.logo).addAction(aceptarAccion).setStyle(new Notification.BigTextStyle().bigText(contenido).setBigContentTitle(titulo));
    }

    public NotificationCompat.Builder getNotificationSimple(String titulo, String contenido){
        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setContentTitle(titulo).setContentText(contenido).setAutoCancel(true).setColor(Color.GRAY).setSmallIcon(R.drawable.logo).setStyle(new NotificationCompat.BigTextStyle().bigText(contenido).setBigContentTitle(titulo));
    }

    public NotificationCompat.Builder getNotificationOldApi(String titulo, String contenido, PendingIntent intent, Uri sounUri) throws IOException {
        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setContentTitle(titulo).setContentText(contenido).setAutoCancel(true).setSound(sounUri).setContentIntent(intent).setSmallIcon(R.drawable.logo).setStyle(new NotificationCompat.BigTextStyle().bigText(contenido).setBigContentTitle(titulo));
    }

    public NotificationCompat.Builder getNotificationOldApiAccion(String titulo, String contenido, Uri sounUri, NotificationCompat.Action aceptarAccion, String imagen) throws IOException {
        Bitmap img_foto = Picasso.with(getApplicationContext()).load(imagen).get();
        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setContentTitle(titulo).setContentText(contenido).setAutoCancel(true).setSound(sounUri).setSmallIcon(R.drawable.logo).addAction(aceptarAccion).setStyle(new NotificationCompat.BigTextStyle().bigText(contenido).setBigContentTitle(titulo));
    }
    */

}
