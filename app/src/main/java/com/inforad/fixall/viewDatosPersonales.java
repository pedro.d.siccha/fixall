package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hbb20.CountryCodePicker;
import com.google.firebase.database.*;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.model.Plan;
import com.inforad.fixall.model.Trabajo;
import com.inforad.fixall.model.User;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.viewAgregarProfesion;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.planProvider;
import com.inforad.fixall.provider.tokenProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.utilidades.CompresorBitmapImage;
import com.inforad.fixall.utilidades.DatePickerFragment;
import com.inforad.fixall.utilidades.FileUtilidades;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class viewDatosPersonales extends AppCompatActivity {

    private EditText Nombre, Apellido, Alias, Numero, DNI, txtFecNacimiento;
    private Button btnSiguiente;
    private CountryCodePicker codigoRegion;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private userProvider mUserProvider;
    private tokenProvider mToken;
    private notificacionProvider mNotificacionProvider;
    private CircleImageView btnSubirImagen;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private planProvider mPlanProvider;
    private RadioGroup radioOpciones;
    private RadioButton rbtnVaron, rbtnMujer;
    private String seleccion, dir = "";
    private ProgressDialog mProgressDialog;
    //private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_datos_personales);
        Iniciar();

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validarSeleccion();
                mProgressDialog.setMessage("Cargando Imagen...");
                mProgressDialog.setTitle("Espere...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                if (mImageFile != null){
                    String nomImagen = Alias.getText().toString().replace(" ","");
                    guardarImagen(nomImagen);
                }else {
                    if (seleccion == "Varón"){
                        String img = "https://firebasestorage.googleapis.com/v0/b/fixallapp-a57f9.appspot.com/o/imgUsuario%2Fdefault%2Fhombre.png?alt=media&token=a8bb773e-24b5-4659-a30e-f2be53e9ef50";
                        llamarVerifNumTelf(img);
                    }else {
                        String img = "https://firebasestorage.googleapis.com/v0/b/fixallapp-a57f9.appspot.com/o/imgUsuario%2Fdefault%2Fmujer.png?alt=media&token=f33f437a-035a-40ed-9328-3a44345b0904";
                        llamarVerifNumTelf(img);
                    }
                }
            }
        });

        btnSubirImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarGaleria();
            }
        });

        txtFecNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.txtFechaNacimiento:
                        showDatePickerDialog();
                        break;
                }
            }
        });

        DNI.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() == 8){
                    Toast.makeText(getApplicationContext(),"Por favor, ingrese un número de DNI correcto.",Toast.LENGTH_LONG).show();
                    DNI.setFocusable(true);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        Numero.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() == 9){
                    Toast.makeText(getApplicationContext(),"Por favor, ingrese un número de celular correcto.",Toast.LENGTH_LONG).show();
                    Numero.setFocusable(true);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void Iniciar() {
        Nombre = findViewById(R.id.txtNombre);
        Apellido = findViewById(R.id.txtApellido);
        Alias = findViewById(R.id.txtAlias);
        Numero = findViewById(R.id.txtTelefono);
        DNI = findViewById(R.id.txtDni);
        codigoRegion = findViewById(R.id.cbRegion);
        btnSubirImagen = findViewById(R.id.civPerfil);
        radioOpciones = findViewById(R.id.rgOpciones);
        rbtnVaron = findViewById(R.id.rbVaron);
        rbtnMujer = findViewById(R.id.rbMujer);
        txtFecNacimiento = findViewById(R.id.txtFechaNacimiento);
        btnSiguiente = findViewById(R.id.btnSiguienteDP);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserProvider = new userProvider();
        mToken = new tokenProvider();
        mNotificacionProvider = new notificacionProvider();
        mPlanProvider = new planProvider();
        mProgressDialog = new ProgressDialog(viewDatosPersonales.this);
        //mProcesador = new procesador();

    }

    private void showDatePickerDialog() {

        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int anio, int mes, int dia) {
                final String selectedDate = dia + "/" + (mes + 1) + "/" + anio;
                txtFecNacimiento.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void mostrarGaleria() {
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                btnSubirImagen.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
               // Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void guardarImagen(String alias){
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgUsuario").child(mAuth.getCurrentUser().getUid()).child(alias + ".jpg");
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String img = uri.toString();
                            llamarVerifNumTelf(img);
                        }
                    });
                }else {
                   // Toast.makeText(viewDatosPersonales.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public String fecActual(){
        String fecha = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        return fecha;
    }

    public String fecUnMes(){
        String fecActual = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Date dt = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.add(Calendar.MONTH, 1);
        String fecFinal = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        return fecFinal;
    }



    public void llamarVerifNumTelf(String urlImagen){

        String user_id = mAuth.getCurrentUser().getUid();
        String nombre = Nombre.getText().toString();
        String apellido = Apellido.getText().toString();
        String alias = Alias.getText().toString();
        String numero = Numero.getText().toString();
        String numTelfono ="+" + codigoRegion.getFullNumber() + numero;
        String imgUsuario = urlImagen;
        String dni = DNI.getText().toString();
        String fecActual = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String fecNacimiento = txtFecNacimiento.getText().toString();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date dt = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.add(Calendar.MONTH, 1);
        String fecFinal = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

        if (seleccion == null){
            Toast.makeText(this, "Por favor seleccione un género", Toast.LENGTH_SHORT).show();
        }

        User user = new User(user_id, nombre, apellido, alias, numTelfono, imgUsuario, seleccion, dni, "BASICO", "ACTIVO", fecActual, fecFinal, "PRIMERO", fecNacimiento, Integer.parseInt(edad(txtFecNacimiento)), 0);
        mUserProvider.crear(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                generarToken(user_id);

                Plan plan = new Plan(user_id, "BASICO", fecActual(), fecUnMes(), 0.00, 0, 0.00, 0, 3);
                mPlanProvider.registrarPlan(plan, user_id).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //mProgressDialog.dismiss();
                        Intent intent = new Intent(viewDatosPersonales.this, viewIngresarDireccion.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
    }

    private void validarSeleccion(){
        if (rbtnVaron.isChecked()){
            seleccion = "Varón";
        }

        if (rbtnMujer.isChecked()){
            seleccion = "Mujer";
        }
    }

    void generarToken(String idUsuario){
        mToken.crear(idUsuario);
    }

    private String edad(EditText txtFecha){
        String devEdad = "";
        String [] fecha_nacimiento = txtFecha.getText().toString().split("/");
        int dia = Integer.parseInt(fecha_nacimiento[0]);
        int mes = Integer.parseInt(fecha_nacimiento[1]);
        int anio = Integer.parseInt(fecha_nacimiento[2]);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            LocalDate today = LocalDate.now();
            LocalDate birthdate = LocalDate.of(anio, mes, dia);
            Period p = Period.between(birthdate, today);
            devEdad = "" + p.getYears();
        }else {
            devEdad = "26";
        }
        return devEdad;
    }

}