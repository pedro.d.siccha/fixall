package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.empresario.agregarTarjeta;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.detalleProfesional;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewPerfilEmpresario;
import com.inforad.fixall.model.User;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.provider.tajertaCreditoProvider;
import com.inforad.fixall.provider.userProvider;
import com.tooltip.Tooltip;

public class viewTipoUsuario extends AppCompatActivity {

    private ConstraintLayout /*empresario, profecional,*/ Fondo;
    private TextView btnEmpresario, btnProfesional;
    private String tipoUsuario, idUsuario, actividad;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private userProvider mUserProvider;
    private procesador mProcesador;
    private Button btnInfoEmpresario, btnInfoProfesional;
    private tajertaCreditoProvider mTarjetaProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_tipo_usuario);
        Iniciar();
        mostrarTuto();
        mProcesador.caracteristicasPlanConstraint(Fondo, viewTipoUsuario.this);

        btnInfoProfesional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewTipoUsuario.this, detalleProfesional.class);
                startActivity(intent);
            }
        });

        btnInfoEmpresario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewTipoUsuario.this, detalleEmpresario.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar() {

        btnEmpresario = findViewById(R.id.tvEmpresario);
        btnProfesional = findViewById(R.id.tvProfesional);
        btnInfoEmpresario = findViewById(R.id.bInfoEmpresario);
        btnInfoProfesional = findViewById(R.id.bInfoProfesional);
        Fondo = findViewById(R.id.clFondo);

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserProvider = new userProvider();
        mTarjetaProvider = new tajertaCreditoProvider();
        mProcesador = new procesador();

    }

    private void mostrarTuto() {
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    actividad = snapshot.child("actividad").getValue().toString();

                   // Toast.makeText(viewTipoUsuario.this, actividad, Toast.LENGTH_SHORT).show();

                    if (actividad.equals("PRIMERO")){

                        btnEmpresario.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(viewTipoUsuario.this, validarCuenta.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("tipoUsuario", "EMPRESARIO");
                                startActivity(intent);
                            }
                        });

                        btnProfesional.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(viewTipoUsuario.this, validarCuenta.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("tipoUsuario", "PROFESIONAL");
                                startActivity(intent);
                            }
                        });

                    }else {
                        btnEmpresario.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mTarjetaProvider.mostrarTarjeta(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if (snapshot.exists()){
                                            Intent intent = new Intent(viewTipoUsuario.this, panelcontrolEmpresario.class);
                                            intent.putExtra("tipoUsuario", "EMPRESARIO");
                                            startActivity(intent);
                                        }else {
                                            Intent intent = new Intent(viewTipoUsuario.this, agregarTarjeta.class);
                                            intent.putExtra("tipoUsuario", "EMPRESARIO");
                                            startActivity(intent);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {
                                        Intent intent = new Intent(viewTipoUsuario.this, agregarTarjeta.class);
                                        intent.putExtra("tipoUsuario", "EMPRESARIO");
                                        startActivity(intent);
                                    }
                                });

                            }
                        });

                        btnProfesional.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mTarjetaProvider.mostrarTarjeta(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if (snapshot.exists()){
                                            mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                    if (snapshot.child("actividad").getValue().toString().equals("FinConfig")){
                                                        Intent intent = new Intent(viewTipoUsuario.this, viewPerfilP.class);
                                                        startActivity(intent);
                                                    }else {
                                                        Intent intent = new Intent(viewTipoUsuario.this, panelcontrolProfesional.class);
                                                        intent.putExtra("tipoUsuario", "PROFESIONAL");
                                                        startActivity(intent);
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError error) {
                                                   // Toast.makeText(viewTipoUsuario.this, "Error: " + error, Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }else {
                                            Intent intent = new Intent(viewTipoUsuario.this, agregarTarjeta.class);
                                            intent.putExtra("tipoUsuario", "PROFESIONAL");
                                            startActivity(intent);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {
                                        Intent intent = new Intent(viewTipoUsuario.this, agregarTarjeta.class);
                                        intent.putExtra("tipoUsuario", "PROFESIONAL");
                                        startActivity(intent);
                                    }
                                });

                            }
                        });
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                //Toast.makeText(viewTipoUsuario.this, "Error Tutorial: " + error, Toast.LENGTH_SHORT).show();
            }
        });
    }

}