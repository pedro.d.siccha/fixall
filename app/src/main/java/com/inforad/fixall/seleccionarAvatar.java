package com.inforad.fixall;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.airbnb.lottie.LottieAnimationView;
import com.inforad.fixall.procesos.procesador;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

public class seleccionarAvatar extends AppCompatActivity {

    private GifImageView mAvatarHombre, mAvatarMujer;
    private ConstraintLayout Fondo;
    private procesador mProcesaro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_seleccionar_avatar);
        Iniciar();

        mProcesaro.caracteristicasPlanConstraint(Fondo, seleccionarAvatar.this);

        mAvatarHombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(seleccionarAvatar.this, avatarVaron.class);
                startActivity(intent);
            }
        });

        mAvatarMujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(seleccionarAvatar.this, avatarMujer.class);
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {

        mAvatarHombre = findViewById(R.id.givAvatarVaron);
        mAvatarMujer = findViewById(R.id.givAvatarMujer);
        Fondo = findViewById(R.id.clFondo);

        mProcesaro = new procesador();

    }
}