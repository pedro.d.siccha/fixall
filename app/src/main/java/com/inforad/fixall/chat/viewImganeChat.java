package com.inforad.fixall.chat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.inforad.fixall.R;
import com.squareup.picasso.Picasso;

public class viewImganeChat extends AppCompatActivity {

    private String url;
    private ImageView btnCerrar, imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_imgane_chat);
        Inicio();

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void Inicio() {
        url = getIntent().getStringExtra("urlImg");

        btnCerrar = findViewById(R.id.ivCerrar);
        imagen = findViewById(R.id.ivImagen);

        Picasso.with(this).load(url).into(imagen);

    }
}