package com.inforad.fixall.reciver;

import android.app.NotificationManager;
import android.app.RemoteInput;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.model.Mensaje;
import com.inforad.fixall.provider.mensajeProvider;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.tokenProvider;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.inforad.fixall.servicios.MyFirebaseMessagingClient.NOTIFICATION_REPLAY;

public class mensajeReciver extends BroadcastReceiver {

    String idEnvia, idRecive, idChat, aliasEnvia, imgEnvia;
    int idNotificacion;
    tokenProvider mTokenProvider;
    notificacionProvider mNotificacionProvider;

    @Override
    public void onReceive(Context context, Intent intent) {
        idEnvia = intent.getExtras().getString("idEnvia");
        idRecive = intent.getExtras().getString("idRecive");
        idChat = intent.getExtras().getString("idChat");
        aliasEnvia = intent.getExtras().getString("aliasEnvia");
        imgEnvia = intent.getExtras().getString("imgEnvia");
        idNotificacion = intent.getExtras().getInt("idNotificacion");
        mTokenProvider = new tokenProvider();
        mNotificacionProvider = new notificacionProvider();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(idNotificacion);

        String mensajeTxt = obtenerMensaje(intent).toString();

        enviarMensaje(mensajeTxt);

    }

    private void enviarMensaje(String mensaje) {

        if (!mensaje.isEmpty()){
            Mensaje msj = new Mensaje();
            msj.setIdChat(idChat);
            msj.setIdEnvia(idEnvia);
            msj.setIdRecibe(idRecive);
            msj.setMensaje(mensaje);
            msj.setVisto(false);
            msj.setTimestamp(new Date().getTime());
            mensajeProvider mMensajeProvider = new mensajeProvider();
            mMensajeProvider.crearMensaje(msj).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    //enviarNotificacion(msj);
                    //Toast.makeText(chatActivity.this, "Mensaje Enviado", Toast.LENGTH_SHORT).show();
                    enviarNotificacion(msj);
                }
            });
        }
    }

    private CharSequence obtenerMensaje(Intent intent){
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null){
            return remoteInput.getCharSequence(NOTIFICATION_REPLAY);
        }
        return null;
    }

    private void enviarNotificacion(final Mensaje mensaje){
        mTokenProvider.obtenerToken(idRecive).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String token = snapshot.child("token").getValue().toString();
                    ArrayList<Mensaje> mensajeArrayList = new ArrayList<>();
                    mensajeArrayList.add(mensaje);
                    Gson gson = new Gson();
                    String mensajes = gson.toJson(mensajeArrayList);
                    Map<String, String> map = new HashMap<>();
                    map.put("titulo", " NUEVO MENSAJE ");
                    map.put("contenido", mensaje.getMensaje());
                    map.put("idNotificacion", String.valueOf(idNotificacion));
                    map.put("mensajes", mensajes);
                    map.put("aliasEnvia", aliasEnvia.toUpperCase());
                    map.put("aliasRecive", aliasEnvia.toUpperCase());
                    map.put("imgEnvia", imgEnvia);
                    map.put("idEnvia", mensaje.getIdEnvia());
                    map.put("idRecive", mensaje.getIdRecibe());
                    map.put("idChat", mensaje.getIdChat());
                    FCMBody fcmBody = new FCMBody(token, "high", "4500s", map);
                    mNotificacionProvider.sendNotification(fcmBody).enqueue(new Callback<FCMResponse>() {
                        @Override
                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                            if (response.body() != null){
                                if (response.body().getSuccess() == 1){
                                    //Toast.makeText(chatActivity.this, "Oferta enviada", Toast.LENGTH_SHORT).show();
                                }else {
                                    // Toast.makeText(viewReportarProblemaE.this, "No se pudo enviar la notificación", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<FCMResponse> call, Throwable t) {

                        }
                    });



                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }



}
