package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewAceptarProfesionalE;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.provider.tajertaCreditoProvider;
import com.inforad.fixall.provider.userProvider;

import pl.droidsonroids.gif.GifImageView;

public class pagarDestacarte extends AppCompatActivity {

    private ConstraintLayout Fondo;
    private Button btnPagar, btnNuevo;
    private procesador mProcesador;
    private TextView txtNumero, txtFecha;
    private tajertaCreditoProvider mTarjetaProvider;
    private FirebaseAuth mAuth;
    private String idUsuario, direccion, idTarjeta;
    private DatabaseReference mDatabase;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagar_destacarte);
        Inicio();
        mProcesador.caracteristicasPlanConstraint(Fondo, pagarDestacarte.this);

        btnPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (direccion.equals("EMPRESARIO")){
                    Intent intent = new Intent(pagarDestacarte.this, panelcontrolEmpresario.class);
                    startActivity(intent);
                }else if (direccion.equals("PROFESIONAL")){
                    Intent intent = new Intent(pagarDestacarte.this, panelcontrolProfesional.class);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(pagarDestacarte.this, viewPerfilP.class);
                    startActivity(intent);
                }
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(pagarDestacarte.this, viewAgregarTarjeta.class);
                intent.putExtra("direccion", direccion);
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(pagarDestacarte.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Inicio() {
        Fondo = findViewById(R.id.clFondo);
        btnPagar = findViewById(R.id.bPagar);
        btnNuevo = findViewById(R.id.bNuevo);
        txtNumero = findViewById(R.id.tvTarjeta);
        txtFecha = findViewById(R.id.tvFechaTarjeta);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mProcesador = new procesador();
        mTarjetaProvider = new tajertaCreditoProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        this.idTarjeta = "";

        cargarDatos();
    }

    private void cargarDatos(){

        direccion = getIntent().getStringExtra("direccion");

        mDatabase.child("Tarjeta").child(idUsuario).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                idTarjeta = snapshot.getKey();
                mTarjetaProvider.obtenerTarjeta(idUsuario, idTarjeta).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            txtNumero.setText(snapshot.child("numTarjeta").getValue().toString());
                            txtFecha.setText(snapshot.child("fecVencimiento").getValue().toString());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}