package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.TarjetaCredito;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.provider.tajertaCreditoProvider;
import com.inforad.fixall.provider.userProvider;

public class viewDestacarte extends AppCompatActivity {

    private LinearLayout btnGold, btnBlack;
    private Button btnOmitir;
    private String direccion, idUsuario;
    private userProvider mUserProvider;
    private FirebaseAuth mAuth;
    private procesador mProcesador;
    private DrawerLayout Fondo;
    private tajertaCreditoProvider mTarjetaProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_destacarte);
        Iniciar();
        obtenerDatos();
        mProcesador.caracteristicasPlanDrawer(Fondo, viewDestacarte.this);

        btnGold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewDestacarte.this, viewDescripcionPlan.class);
                intent.putExtra("plan", "GOLD");
                intent.putExtra("direccion", direccion);
                startActivity(intent);
            }
        });

        btnBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewDestacarte.this, viewDescripcionPlan.class);
                intent.putExtra("plan", "BLACK");
                intent.putExtra("direccion", direccion);
                startActivity(intent);
            }
        });

        btnOmitir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUserProvider.actualizarActividad(idUsuario, "NORMAL").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if (direccion.equals("EMPRESARIO")){
                            Intent intent = new Intent(viewDestacarte.this, panelcontrolEmpresario.class);
                            startActivity(intent);
                        }else if (direccion.equals("PROFESIONAL")){
                            Intent intent = new Intent(viewDestacarte.this, panelcontrolProfesional.class);
                            startActivity(intent);
                        }else {
                            //Toast.makeText(viewDestacarte.this, "Hubo un error, por favor reinice el aplicativo o seleccione algun plan.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    private void Iniciar() {
        btnGold = findViewById(R.id.llGold);
        btnBlack = findViewById(R.id.llBlack);
        btnOmitir = findViewById(R.id.bOmitir);
        Fondo = findViewById(R.id.dlFondo);

        mUserProvider = new userProvider();
        mAuth = FirebaseAuth.getInstance();
        mProcesador = new procesador();
        mTarjetaProvider = new tajertaCreditoProvider();

        idUsuario = mAuth.getCurrentUser().getUid();
    }

    private void obtenerDatos(){
        direccion = getIntent().getStringExtra("tipoUsuario");
    }

}