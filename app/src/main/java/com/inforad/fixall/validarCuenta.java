package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.model.TarjetaCredito;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.tajertaCreditoProvider;

public class validarCuenta extends AppCompatActivity {

    private TextView tvNumero, tvFecha;
    private Button btnGuardar;
    private tajertaCreditoProvider mTarjetaProvider;
    private String idUsuario, tipoUsuario;
    private FirebaseAuth mAuth;
    private procesador mProcesador;
    private ConstraintLayout Fondo;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_validar_cuenta);
        Iniciar();

        mProcesador.caracteristicasPlanConstraint(Fondo, validarCuenta.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!tvNumero.getText().toString().isEmpty()){
                    if (!tvFecha.getText().toString().isEmpty()){
                        guardarTarjeta();
                    }else{
                        Toast.makeText(validarCuenta.this, "Por favor ingrese una fecha válida para verificar su cuenta", Toast.LENGTH_SHORT).show();
                        tvFecha.setFocusable(true);
                    }
                }else{
                    Toast.makeText(validarCuenta.this, "Por favor ingrese un número de cuenta válido para verificar su cuenta", Toast.LENGTH_SHORT).show();
                    tvNumero.setFocusable(true);
                }
            }
        });

    }

    private void guardarTarjeta() {
        String idTarjeta = mDatabase.push().getKey();
        TarjetaCredito tarjeta = new TarjetaCredito(idTarjeta, "", tvNumero.getText().toString(), tvFecha.getText().toString(), "ACTIVO");
        mTarjetaProvider.crearTarjeta(tarjeta, idUsuario, idTarjeta).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Intent intent = new Intent(validarCuenta.this, viewDestacarte.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("tipoUsuario", tipoUsuario);
                startActivity(intent);
                finish();
            }
        });
    }

    private void Iniciar() {
        tvNumero = findViewById(R.id.tvTarjeta);
        tvFecha = findViewById(R.id.tvFechaTarjeta);
        btnGuardar = findViewById(R.id.btnGuardar);
        Fondo = findViewById(R.id.clFondo);

        mTarjetaProvider = new tajertaCreditoProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();

        mProcesador = new procesador();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        cargarDatos();
    }

    private void cargarDatos() {
        tipoUsuario = getIntent().getStringExtra("tipoUsuario");
    }

}