package com.inforad.fixall;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.TarjetaCredito;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.provider.tajertaCreditoProvider;

public class viewAgregarTarjeta extends AppCompatActivity {

    private EditText txtNumeroTarjeta, txtFechaTarjeta;
    private Button btnCrear;
    private tajertaCreditoProvider mTarjetaProvider;
    private FirebaseAuth mAuth;
    private String direccion;
    private procesador mProcesador;
    private ConstraintLayout Fondo;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_agregar_tarjeta);
        Iniciar();

        mProcesador.caracteristicasPlanConstraint(Fondo, viewAgregarTarjeta.this);

        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String idTarjeta = mDatabase.push().getKey();
                TarjetaCredito tarjeta = new TarjetaCredito(idTarjeta, "BN", txtNumeroTarjeta.getText().toString(), txtFechaTarjeta.getText().toString(), "PRINCIPAL");
                mTarjetaProvider.crearTarjeta(tarjeta, mAuth.getCurrentUser().getUid(), idTarjeta).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if (direccion.equals("EMPRESARIO")){
                            Intent intent = new Intent(viewAgregarTarjeta.this, panelcontrolEmpresario.class);
                            startActivity(intent);
                        }else if (direccion.equals("PROFESIONAL")){
                            Intent intent = new Intent(viewAgregarTarjeta.this, panelcontrolProfesional.class);
                            startActivity(intent);
                        }else {
                            Intent intent = new Intent(viewAgregarTarjeta.this, viewPerfilP.class);
                            startActivity(intent);
                        }
                    }
                });
            }
        });
    }

    private void Iniciar() {
        txtNumeroTarjeta = findViewById(R.id.tvTarjeta);
        txtFechaTarjeta = findViewById(R.id.tvFechaTarjeta);
        btnCrear = findViewById(R.id.btnGuardar);
        Fondo = findViewById(R.id.clFondo);

        mTarjetaProvider = new tajertaCreditoProvider();
        mAuth = FirebaseAuth.getInstance();
        mProcesador = new procesador();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        cargarDatos();
    }

    private void cargarDatos(){
        direccion = getIntent().getStringExtra("direccion");
    }
}