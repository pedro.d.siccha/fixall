package com.inforad.fixall.pantallasDetalles;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.profesional.editarEstudios;
import com.inforad.fixall.profesional.editarIso;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.provider.isoProvider;
import com.inforad.fixall.provider.userProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class detalleIso extends AppCompatActivity {

    private CircleImageView imgIso;
    private TextView txtCodigo, txtNombre;
    private ImageView btnCerrar;
    private String idIso, idUsuario;
    private userProvider mUserProvider;
    private FirebaseAuth mAuth;
    private Button btnEditar, btnEliminar;
    private isoProvider mIsoProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detalle_iso);
        Inicio();

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            if (snapshot.child("actividad").getValue().toString().equals("FinConfig")){
                                Intent intent = new Intent(detalleIso.this, viewPerfilP.class);
                                startActivity(intent);
                            }else {
                                Intent intent = new Intent(detalleIso.this, panelcontrolProfesional.class);
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });

        imgIso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(detalleIso.this, imgIso.class);
                intent.putExtra("idIso", idIso);
                startActivity(intent);
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(detalleIso.this, editarIso.class);
                intent.putExtra("idIso", idIso);
                startActivity(intent);
                finish();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsoProvider.eliminarIso(idUsuario, idIso).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(detalleIso.this, "El ISO se eliminó correctamente", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });

    }

    private void Inicio() {
        txtCodigo = findViewById(R.id.tvCodigo);
        txtNombre = findViewById(R.id.tvNombre);
        imgIso = findViewById(R.id.civIso);
        btnCerrar = findViewById(R.id.ivCerrar);
        btnEditar = findViewById(R.id.bEditar);
        btnEliminar = findViewById(R.id.bEliminar);

        mUserProvider = new userProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mIsoProvider = new isoProvider();

        cargarDatos();
    }

    private void cargarDatos() {
        idIso = getIntent().getStringExtra("idIso");

        mIsoProvider.obtenerIso(idUsuario, idIso).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtCodigo.setText(snapshot.child("codigo").getValue().toString());
                    txtNombre.setText(snapshot.child("nombre").getValue().toString());
                    Picasso.with(detalleIso.this).load(snapshot.child("imagen").getValue().toString()).into(imgIso);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }
}