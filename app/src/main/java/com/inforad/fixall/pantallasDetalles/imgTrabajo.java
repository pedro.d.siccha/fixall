package com.inforad.fixall.pantallasDetalles;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.inforad.fixall.R;
import com.squareup.picasso.Picasso;

public class imgTrabajo extends AppCompatActivity {

    private ImageView btnAtras, imgCentral;
    private String cargo, empresa, tipoEmpleado, fecInicio, fecFin, img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_img_trabajo);
        Iniciar();

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                /*
                Intent intent = new Intent(imgTrabajo.this, detalleTrabajo.class);
                intent.putExtra("cargo", cargo);
                intent.putExtra("fecfin", fecFin);
                intent.putExtra("fecinicio", fecInicio);
                intent.putExtra("imagen", img);
                intent.putExtra("nomempresa", empresa);
                intent.putExtra("tipoempleo", tipoEmpleado);
                startActivity(intent);
                 */
            }
        });
    }

    private void Iniciar() {
        btnAtras = findViewById(R.id.ivCerrar);
        imgCentral = findViewById(R.id.ivCentral);

        cargarDatos();
    }

    private void cargarDatos() {
        cargo = getIntent().getStringExtra("cargo");
        empresa = getIntent().getStringExtra("nomempresa");
        tipoEmpleado = getIntent().getStringExtra("tipoempleo");
        fecInicio = getIntent().getStringExtra("fecinicio");
        fecFin = getIntent().getStringExtra("fecfin");
        img = getIntent().getStringExtra("imagen");

        Picasso.with(imgTrabajo.this).load(img).into(imgCentral);
    }
}