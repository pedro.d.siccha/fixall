package com.inforad.fixall.pantallasDetalles;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.profesional.editarEstudios;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.provider.estudioProvider;
import com.inforad.fixall.provider.userProvider;

public class detalleEstudios extends AppCompatActivity {

    private TextView txtCarrera, txtUniversidad, txtFecInicio, txtFecFin;
    private ImageView btnCerrar;
    private userProvider mUserProvider;
    private FirebaseAuth mAuth;
    private String idUsuario, idEstudio;
    private Button btnEditar, btnEliminar;
    private estudioProvider mEstudioProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detalle_estudios);
        Iniciar();

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            if (snapshot.child("actividad").getValue().toString().equals("FinConfig")){
                                Intent intent = new Intent(detalleEstudios.this, viewPerfilP.class);
                                startActivity(intent);
                                finish();
                            }else {
                                Intent intent = new Intent(detalleEstudios.this, panelcontrolProfesional.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(detalleEstudios.this, editarEstudios.class);
                intent.putExtra("idEstudio", idEstudio);
                startActivity(intent);
                finish();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEstudioProvider.eliminarEstudio(idUsuario, idEstudio).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(detalleEstudios.this, "El estudio fué eliminado correctamente", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });

    }

    private void Iniciar() {
        txtCarrera = findViewById(R.id.tvCarrera);
        txtUniversidad = findViewById(R.id.tvUniversidad);
        txtFecInicio = findViewById(R.id.tvFecInicio);
        txtFecFin = findViewById(R.id.tvFecFin);
        btnCerrar = findViewById(R.id.ivCerrar);
        btnEditar = findViewById(R.id.bEditar);
        btnEliminar = findViewById(R.id.bEliminar);

        mUserProvider = new userProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mEstudioProvider = new estudioProvider();

        cargarDatos();
    }

    private void cargarDatos() {
        idEstudio = getIntent().getStringExtra("id");

        mEstudioProvider.obtenerEstudio(idUsuario, idEstudio).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtCarrera.setText(snapshot.child("nombreColegio").getValue().toString());
                    txtUniversidad.setText(snapshot.child("grado").getValue().toString());
                    txtFecInicio.setText(snapshot.child("fecInicio").getValue().toString());
                    txtFecFin.setText(snapshot.child("fecFin").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}