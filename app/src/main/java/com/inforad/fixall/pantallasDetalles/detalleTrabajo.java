package com.inforad.fixall.pantallasDetalles;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.provider.trabajoProvider;
import com.inforad.fixall.provider.userProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class detalleTrabajo extends AppCompatActivity {

    private TextView txtCargo, txtEmpresa, txtTipoEmpleado, txtFecInicio, txtFecFin;
    private CircleImageView imgTrabajo;
    private ImageView btnCerrar;
    private userProvider mUserProvider;
    private FirebaseAuth mAuth;
    private String idUsuario, idTrabajo;
    private trabajoProvider mTrabajoProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detalle_trabajo);
        Iniciar();

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                /*
                mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            if (snapshot.child("actividad").getValue().toString().equals("FinConfig")){
                                Intent intent = new Intent(detalleTrabajo.this, viewPerfilP.class);
                                startActivity(intent);
                            }else {
                                Intent intent = new Intent(detalleTrabajo.this, panelcontrolProfesional.class);
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                 */
            }
        });

        imgTrabajo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(detalleTrabajo.this, imgTrabajo.class);
                intent.putExtra("idTrabajo", idTrabajo);
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        txtCargo = findViewById(R.id.tvCargo);
        txtEmpresa = findViewById(R.id.tvEmpresa);
        txtTipoEmpleado = findViewById(R.id.tvTipoEmpleado);
        txtFecInicio = findViewById(R.id.tvFecInicio);
        txtFecFin = findViewById(R.id.tvFecFin);
        imgTrabajo = findViewById(R.id.civTrabajo);
        btnCerrar = findViewById(R.id.ivCerrar);

        mUserProvider = new userProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mTrabajoProvider = new trabajoProvider();

        cargarDatos();
    }

    private void cargarDatos() {
        idTrabajo = getIntent().getStringExtra("idTrabajo");

        mTrabajoProvider.obtenerTrabajo(idUsuario, idTrabajo).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtCargo.setText(snapshot.child("cargo").getValue().toString());
                    txtEmpresa.setText(snapshot.child("nomEmpresa").getValue().toString());
                    txtTipoEmpleado.setText(snapshot.child("tipoEmpleo").getValue().toString());
                    txtFecInicio.setText(snapshot.child("fecInicio").getValue().toString());
                    txtFecFin.setText(snapshot.child("fecFin").getValue().toString());
                    Picasso.with(detalleTrabajo.this).load(snapshot.child("imagen").getValue().toString()).into(imgTrabajo);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}