package com.inforad.fixall.pantallasDetalles;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.provider.isoProvider;
import com.squareup.picasso.Picasso;

public class imgIso extends AppCompatActivity {

    private ImageView btnAtras, imgCentral;
    private String idIso, idUsuario;
    private isoProvider mIsoProvider;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_img_iso);
        Iniciar();

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void Iniciar() {
        btnAtras = findViewById(R.id.ivCerrar);
        imgCentral = findViewById(R.id.ivCentral);

        mIsoProvider = new isoProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();

        cargarDatos();
    }

    private void cargarDatos() {
        idIso = getIntent().getStringExtra("idIso");

        mIsoProvider.obtenerIso(idUsuario, idIso).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    Picasso.with(imgIso.this).load(snapshot.child("imagen").getValue().toString()).into(imgCentral);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }
}