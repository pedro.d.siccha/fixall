package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.fixall.empresario.viewDescuento;
import com.inforad.fixall.model.Flyer;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.viewAgregarIso;
import com.inforad.fixall.provider.flyerProvider;
import com.inforad.fixall.utilidades.CompresorBitmapImage;
import com.inforad.fixall.utilidades.FileUtilidades;

import java.io.File;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class subirFlyer extends AppCompatActivity {

    private EditText inputInformacion;
    private CircleImageView imgFlyer;
    private Button btnGuardar;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private procesador mProcesador;
    private ProgressDialog mProgressDialog;
    private ConstraintLayout fondo;
    private DatabaseReference mDatabase;
    private flyerProvider mFlayerProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_subir_flyer);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(fondo, subirFlyer.this);

        imgFlyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirGaleria();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressDialog.setMessage("Guardando Flyer...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                guardarFlyer();
            }
        });

    }

    private void guardarFlyer() {
        String idFlyer = mDatabase.push().getKey();
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgFlyer").child(mAuth.getCurrentUser().getUid()).child(idFlyer + ".jpg");
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String img = uri.toString();

                            Flyer flyer = new Flyer(idFlyer, idUsuario, inputInformacion.getText().toString(), img, "ACTIVO", new Date().getTime());
                            mFlayerProvider.crearFlyer(flyer, idFlyer).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    mProgressDialog.dismiss();
                                    Intent intent = new Intent(subirFlyer.this, viewDescuento.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    private void abrirGaleria() {
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                imgFlyer.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
            }
        }
    }

    private void Iniciar() {
        inputInformacion = findViewById(R.id.txtInformacion);
        imgFlyer = findViewById(R.id.civFlyer);
        btnGuardar = findViewById(R.id.bGuardar);
        fondo = findViewById(R.id.clFondo);

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProcesador = new procesador();
        mProgressDialog = new ProgressDialog(subirFlyer.this);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mFlayerProvider = new flyerProvider();
    }



}