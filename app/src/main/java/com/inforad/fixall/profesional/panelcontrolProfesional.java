package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.MainActivity;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.estudioAdaptador;
import com.inforad.fixall.adapters.isoAdaptador;
import com.inforad.fixall.adapters.palabraAdaptado;
import com.inforad.fixall.adapters.profesionAdaptador;
import com.inforad.fixall.adapters.trabajoAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewListaProfesionalesLlego;
import com.inforad.fixall.empresario.viewPerfilEmpresario;
import com.inforad.fixall.empresario.viewPerfilPro;
import com.inforad.fixall.empresario.viewResultadoBusquedaE;
import com.inforad.fixall.model.Descripcion;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.model.Iso;
import com.inforad.fixall.model.PalabraClave;
import com.inforad.fixall.model.ProfesionProfesional;
import com.inforad.fixall.model.Trabajo;
import com.inforad.fixall.model.UsuarioPalabra;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.provider.descripcionProvider;
import com.inforad.fixall.provider.profesionalProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.utilidades.CaracterisiticasPlan;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

public class panelcontrolProfesional extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private String idUsuario;
    private CircleImageView imgPerfil;
    private TextView txtNombre, txtApellido, txtAlias, txtEdad;
    private userProvider mUserProvider;
    private Button btnACarrera, btnALaboral, btnAPClave, btnAIso, btnGuardar;
    private estudioAdaptador mEstudioAdaptador;
    private trabajoAdaptador mTrabajoAdaptador;
    private palabraAdaptado mPalabraAdaptador;
    private isoAdaptador mIsoAdaptador;
    private RecyclerView recyclerListaEstudios, recyclerListaTrabajo, recyclerListaPalabra, recyclerListaIso;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnAtras, btnMenu;
    private procesador mProcesador;
    private authProvider mAuthProvide;
    private EditText inputDescripcion;
    private descripcionProvider mDescripcionProvider;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_panelcontrol_profesional);
        Inicio();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, panelcontrolProfesional.this);

        btnACarrera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(panelcontrolProfesional.this, viewAgregarEstudios.class);
                startActivity(intent);
            }
        });

        btnALaboral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(panelcontrolProfesional.this, viewAgregarProfesion.class);
                startActivity(intent);

            }
        });

        btnAPClave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(panelcontrolProfesional.this, viewAgregarPalabrasClave.class);
                startActivity(intent);
            }
        });

        btnAIso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(panelcontrolProfesional.this, viewAgregarIso.class);
                startActivity(intent);
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (inputDescripcion.equals("")){
                    Toast.makeText(panelcontrolProfesional.this, "Por favor complete la descripción de su perfil", Toast.LENGTH_SHORT).show();
                    inputDescripcion.setFocusable(true);
                }else{
                    agregarDescripcion();
                }
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(panelcontrolProfesional.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void agregarDescripcion() {
        String idDescripcion = mDatabase.push().getKey();
        Descripcion descripcion = new Descripcion(idDescripcion, idUsuario, inputDescripcion.getText().toString());
        mDescripcionProvider.crearDescripcion(descripcion, idUsuario, idDescripcion).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(panelcontrolProfesional.this, "Descripción guardada correctamente", Toast.LENGTH_SHORT).show();
                inputDescripcion.setEnabled(false);
                actualizarEstado();
            }
        });
    }

    private void actualizarEstado() {
        mUserProvider.actualizarActividad(idUsuario, "FinConfig").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Intent intent = new Intent(panelcontrolProfesional.this, destacarteProfesional.class);
                startActivity(intent);
            }
        });
    }

    private void Inicio() {

        imgPerfil = findViewById(R.id.civPerfil);
        txtNombre = findViewById(R.id.tvNombre);
        txtApellido = findViewById(R.id.tvApellido);
        txtAlias = findViewById(R.id.tvAlias);
        txtEdad = findViewById(R.id.tvEdad);
        btnACarrera = findViewById(R.id.bAgregar);
        btnALaboral = findViewById(R.id.bAgregarRefLaboral);
        btnAPClave = findViewById(R.id.bAgregarPalClave);
        btnAIso = findViewById(R.id.bAgregarIso);
        inputDescripcion = findViewById(R.id.etDescripcion);
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mAuthProvide = new authProvider();
        btnGuardar = findViewById(R.id.bGuardar);
        recyclerListaEstudios = findViewById(R.id.rvCarrera);
        LinearLayoutManager linearLayoutManagerEstudios = new LinearLayoutManager(this);
        recyclerListaEstudios.setLayoutManager(linearLayoutManagerEstudios);
        recyclerListaTrabajo = findViewById(R.id.rvRefLaboral);
        LinearLayoutManager linearLayoutManagerTrabajo = new LinearLayoutManager(this);
        recyclerListaTrabajo.setLayoutManager(linearLayoutManagerTrabajo);
        recyclerListaPalabra = findViewById(R.id.rvPalClave);
        LinearLayoutManager linearLayoutManagerPClave = new LinearLayoutManager(this);
        recyclerListaPalabra.setLayoutManager(linearLayoutManagerPClave);
        recyclerListaIso = findViewById(R.id.rvIso);
        LinearLayoutManager linearLayoutManagerIso = new LinearLayoutManager(this);
        recyclerListaIso.setLayoutManager(linearLayoutManagerIso);


        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider = new userProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mProcesador = new procesador();
        mDescripcionProvider = new descripcionProvider();
        cargarMenu();
        cargarDatos();
    }

    private void cargarMenu() {
        drawerLayout = findViewById(R.id.dlPanelPro);
        navigationView = findViewById(R.id.nvMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicioPro);
        mProcesador.carcateristicaMenuPro(navigationView);

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(panelcontrolProfesional.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                break;
            case R.id.nav_pendiente:
                Intent viewPendiente = new Intent(panelcontrolProfesional.this, viewCitaPendiente.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(panelcontrolProfesional.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                Intent viewHistorial = new Intent(panelcontrolProfesional.this, viewListaHistorialPro.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(panelcontrolProfesional.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(panelcontrolProfesional.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                mAuthProvide.cerrarSesion();
                Intent viewCerrar = new Intent(panelcontrolProfesional.this, MainActivity.class);
                startActivity(viewCerrar);
                finish();
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(panelcontrolProfesional.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(panelcontrolProfesional.this, panelcontrolEmpresario.class);
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(panelcontrolProfesional.this, viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(panelcontrolProfesional.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                mProcesador.accesoClub(panelcontrolProfesional.this);
                Intent club = new Intent(panelcontrolProfesional.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }

    private void cargarDatos() {

        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(panelcontrolProfesional.this).load(img).into(imgPerfil);
                    txtNombre.setText(snapshot.child("nombre").getValue().toString());
                    txtApellido.setText(snapshot.child("apellido").getValue().toString());
                    txtAlias.setText(snapshot.child("alias").getValue().toString());
                    txtEdad.setText("" + snapshot.child("edad").getValue().toString() + " años");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        Query queryEstudios = mDatabase.child("Estudio").child(idUsuario);
        FirebaseRecyclerOptions<EstudioProfesional> options = new FirebaseRecyclerOptions.Builder<EstudioProfesional>().setQuery(queryEstudios, EstudioProfesional.class).build();
        mEstudioAdaptador = new estudioAdaptador(options, panelcontrolProfesional.this);
        recyclerListaEstudios.setAdapter(mEstudioAdaptador);
        mEstudioAdaptador.startListening();

        Query queryIso = mDatabase.child("Iso").child(idUsuario);
        FirebaseRecyclerOptions<Iso> optionsIso = new FirebaseRecyclerOptions.Builder<Iso>().setQuery(queryIso, Iso.class).build();
        mIsoAdaptador = new isoAdaptador(optionsIso, panelcontrolProfesional.this);
        recyclerListaIso.setAdapter(mIsoAdaptador);
        mIsoAdaptador.startListening();

        Query queryTrabajo = mDatabase.child("Trabajo").child(idUsuario);
        FirebaseRecyclerOptions<Trabajo> options2 = new FirebaseRecyclerOptions.Builder<Trabajo>().setQuery(queryTrabajo, Trabajo.class).build();
        mTrabajoAdaptador = new trabajoAdaptador(options2, panelcontrolProfesional.this);
        recyclerListaTrabajo.setAdapter(mTrabajoAdaptador);
        mTrabajoAdaptador.startListening();

        Query queryPalabra = mDatabase.child("Usuario_PalabraClave").child(idUsuario);
        FirebaseRecyclerOptions<UsuarioPalabra> options3 = new FirebaseRecyclerOptions.Builder<UsuarioPalabra>().setQuery(queryPalabra, UsuarioPalabra.class).build();
        mPalabraAdaptador = new palabraAdaptado(options3, panelcontrolProfesional.this);
        recyclerListaPalabra.setAdapter(mPalabraAdaptador);
        mPalabraAdaptador.startListening();

    }
}