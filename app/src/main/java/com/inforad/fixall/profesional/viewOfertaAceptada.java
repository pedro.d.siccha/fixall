package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.busquedaProvider;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.ofertaProvider;
import com.inforad.fixall.provider.tokenProvider;
import com.inforad.fixall.provider.userProvider;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class viewOfertaAceptada extends AppCompatActivity {

    private String idOferta, idBusqueda, palabraClave, idProfesional, aliasProfesional, imgProfesional, dirProfesional, idEmpresario;
    private double costo;
    private CircleImageView imgPerfil;
    private TextView txtEmpresario, txtCosto, txtDistancia, txtFecha;
    private Button btnLlegue, btnVolver;
    private busquedaProvider mBusquedaProvider;
    private ofertaProvider mOfertaProvider;
    private notificacionProvider mNotificacion;
    private tokenProvider mTokenProvider;
    private FirebaseAuth mAuth;
    private userProvider mUserProvider;
    private DrawerLayout backGroundPrincipal;
    private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_oferta_aceptada);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(backGroundPrincipal, viewOfertaAceptada.this);

        btnLlegue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendario = Calendar.getInstance();
                int anio = calendario.get(Calendar.YEAR);
                int mes = calendario.get(Calendar.MONTH);
                int dia = calendario.get(Calendar.DAY_OF_MONTH);
                String fecActual = String.valueOf(dia) + "/" + String.valueOf(mes + 1) + "/" + String.valueOf(anio);
                mBusquedaProvider.actualizarBusqueda(palabraClave, idBusqueda, idProfesional, aliasProfesional, imgProfesional, dirProfesional, costo, "LLEGO", fecActual).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mOfertaProvider.actualizarOferta(idEmpresario, idProfesional, "LLEGO").addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(viewOfertaAceptada.this, "Notificación enviada", Toast.LENGTH_SHORT).show();
                                enviarNotificacion();
                                Intent intent = new Intent(viewOfertaAceptada.this, viewPerfilP.class);
                                startActivity(intent);
                            }
                        });
                    }
                });
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                Intent intent = new Intent(viewOfertaAceptada.this, viewPerfilP.class);
                startActivity(intent);
                 */
                finish();
            }
        });
    }

    private void Iniciar() {
        obtenerDatos();
        imgPerfil = findViewById(R.id.ivPerfil);
        txtEmpresario = findViewById(R.id.tvNomEmpresario);
        txtCosto = findViewById(R.id.tvCosto);
        txtDistancia = findViewById(R.id.tvDistancia);
        txtFecha = findViewById(R.id.tvFecha);
        btnLlegue = findViewById(R.id.bLlegue);
        btnVolver = findViewById(R.id.bVolver);
        backGroundPrincipal = findViewById(R.id.clBackground);

        mTokenProvider = new tokenProvider();
        mNotificacion = new notificacionProvider();
        mBusquedaProvider = new busquedaProvider();
        mOfertaProvider = new ofertaProvider();
        mAuth = FirebaseAuth.getInstance();
        mUserProvider = new userProvider();
        mProcesador = new procesador();
        mBusquedaProvider.obtenerBusqueda(palabraClave, idBusqueda).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    idProfesional = snapshot.child("idProfesional").getValue().toString();
                    idEmpresario = snapshot.child("idUsuario").getValue().toString();
                    aliasProfesional = snapshot.child("aliasProfesional").getValue().toString();
                    imgProfesional = snapshot.child("imgProfesional").getValue().toString();
                    dirProfesional = snapshot.child("idDirProfesional").getValue().toString();
                    costo = Double.parseDouble(snapshot.child("costo").getValue().toString());

                    String img = snapshot.child("imgUsuario").getValue().toString();
                    Picasso.with(viewOfertaAceptada.this).load(img).into(imgPerfil);
                    txtEmpresario.setText(snapshot.child("aliasUsuario").getValue().toString());
                    txtCosto.setText("Costo Aceptado: S/. " + snapshot.child("costo").getValue().toString());
                    txtFecha.setText("Fecha de Presentación: " + snapshot.child("horaCita").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void obtenerDatos() {

        idOferta = getIntent().getStringExtra("idOferta");
        idBusqueda = getIntent().getStringExtra("idBusqueda");
        palabraClave = getIntent().getStringExtra("palabraClave");

    }

    private void enviarNotificacion() {
        mTokenProvider.obtenerToken(idEmpresario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String token = snapshot.child("token").getValue().toString();
                    mUserProvider.obtenerUsuario(idProfesional).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                String alias = "", img = "";
                                alias = snapshot.child("alias").getValue().toString();
                                img = snapshot.child("imagen").getValue().toString();


                                Map<String, String> map = new HashMap<>();
                                map.put("titulo", "NUEVA OFERTA");
                                map.put("contenido", "Nombre: " + alias + "\nAcaba de llegar ");
                                map.put("imagen", img);
                                FCMBody fcmBody = new FCMBody(token, "high", "4500s", map);
                                mNotificacion.sendNotification(fcmBody).enqueue(new Callback<FCMResponse>() {
                                    @Override
                                    public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                                        if (response.body() != null){
                                            if (response.body().getSuccess() == 1){
                                                Toast.makeText(viewOfertaAceptada.this, "Oferta enviada", Toast.LENGTH_SHORT).show();
                                            }else {
                                                Toast.makeText(viewOfertaAceptada.this, "No se pudo enviar la notificación", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<FCMResponse> call, Throwable t) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });




                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        //mNotificacion.sendNotification()
    }

}