package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.Trabajo;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.trabajoProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.utilidades.CompresorBitmapImage;
import com.inforad.fixall.utilidades.DatePickerFragment;
import com.inforad.fixall.utilidades.FileUtilidades;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

import java.io.File;

public class viewAgregarProfesion extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private Button btnGuardar;
    private ImageView imgEmpleo;
    private EditText txtCargo, txtEmpresa, txtTipoEmpleo, txtFecInicio, txtFecFin;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private FirebaseAuth mAuth;
    private trabajoProvider mTrabajo;
    private userProvider mUserProvider;
    private DrawerLayout backGroundPrincipal;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnAtras;
    private procesador mProcesador;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_agregar_profesion);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewAgregarProfesion.this);

        imgEmpleo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirGaleria();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressDialog.setMessage("Guardando Experiencia Laboral ...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                if (!txtCargo.getText().toString().isEmpty()){
                    if (!txtTipoEmpleo.getText().toString().isEmpty()){
                        if (!txtEmpresa.getText().toString().isEmpty()){
                            if (!txtFecInicio.getText().toString().isEmpty()){
                                if (!txtFecFin.getText().toString().isEmpty()){
                                    guardarImagen(txtCargo.getText().toString(), txtTipoEmpleo.getText().toString(), txtEmpresa.getText().toString(), "", txtFecInicio.getText().toString(), txtFecFin.getText().toString(), "");
                                }else {
                                    mProgressDialog.dismiss();
                                    Toast.makeText(viewAgregarProfesion.this,   "Por favor ingrese una fecha de fin valida", Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                mProgressDialog.dismiss();
                                Toast.makeText(viewAgregarProfesion.this, "Por favor ingrese una fecha de inicio valido", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            mProgressDialog.dismiss();
                            Toast.makeText(viewAgregarProfesion.this, "Por favor ingrese la empresa", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        mProgressDialog.dismiss();
                        Toast.makeText(viewAgregarProfesion.this, "Por favor ingrese el tipo de empleo", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    mProgressDialog.dismiss();
                    Toast.makeText(viewAgregarProfesion.this, "Por favor ingrese su caro", Toast.LENGTH_SHORT).show();
                }

            }
        });

        txtFecInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.txtFecInicio:
                        showDatePickerDialog();
                        break;
                }
            }
        });

        txtFecFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.txtFecFin:
                        showDatePickerDialog2();
                        break;
                }
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(getApplicationContext(), panelcontrolProfesional.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void showDatePickerDialog2() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int anio, int mes, int dia) {
                final String selectedDate = dia + "/" + (mes + 1) + "/" + anio;
                txtFecFin.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int anio, int mes, int dia) {
                final String selectedDate = dia + "/" + (mes + 1) + "/" + anio;
                txtFecInicio.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void Iniciar(){
        btnGuardar = findViewById(R.id.bGuardarProfesion);
        txtCargo = findViewById(R.id.etCargo);
        txtEmpresa = findViewById(R.id.etNombreEmpresa);
        txtTipoEmpleo = findViewById(R.id.etTipoEmpleo);
        txtFecInicio = findViewById(R.id.txtFecInicio);
        txtFecFin = findViewById(R.id.txtFecFin);
        imgEmpleo = findViewById(R.id.ivImgTrabajo);
        backGroundPrincipal = findViewById(R.id.drawer_layout);

        mAuth = FirebaseAuth.getInstance();
        mTrabajo = new trabajoProvider();
        mUserProvider = new userProvider();
        mProcesador = new procesador();
        mProgressDialog = new ProgressDialog(viewAgregarProfesion.this);

        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicioPro);

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewAgregarProfesion.this, panelcontrolProfesional.class);
                startActivity(intent);
            }
        });
    }

    ;

    private void abrirGaleria(){
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                imgEmpleo.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
               // Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void guardarImagen(String cargo, String tipoempleo, String nomempresa, String direccion, String fecinicio, String fecfin, String descripcion){
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgEmpleoUsuario").child(mAuth.getCurrentUser().getUid()).child(mImageFile.getName() + ".jpg");
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String img = uri.toString();
                            String idUsuario = mAuth.getCurrentUser().getUid();

                            Trabajo trabajo = new Trabajo("", cargo, tipoempleo, nomempresa, direccion, fecinicio, fecfin, descripcion, img);
                            mTrabajo.crearTrabajo(trabajo, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(viewAgregarProfesion.this, "Trabajo guardado correctamente", Toast.LENGTH_SHORT).show();
                                    mProgressDialog.dismiss();
                                    Intent intent = new Intent(viewAgregarProfesion.this, panelcontrolProfesional.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    });
                }else {
                   // Toast.makeText(viewAgregarProfesion.this, "Hubo un error al subir la imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                break;
            case R.id.nav_pendiente:
                Intent viewPendiente = new Intent(viewAgregarProfesion.this, viewCitaPendiente.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(viewAgregarProfesion.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                Intent viewHistorial = new Intent(viewAgregarProfesion.this, viewListaHistorialPro.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewAgregarProfesion.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewAgregarProfesion.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewAgregarProfesion.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(viewAgregarProfesion.this, panelcontrolEmpresario.class);
                intent.putExtra("idUsuario", mAuth.getCurrentUser().getUid());
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewAgregarProfesion.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewAgregarProfesion.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                Intent club = new Intent(viewAgregarProfesion.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}