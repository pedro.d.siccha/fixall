package com.inforad.fixall.profesional;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.inforad.fixall.R;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.viewLogin;
import com.inforad.fixall.viewSubscripcion;

public class newConfigPerfil extends AppCompatActivity {

    Button botonGuardar;
    String idUsuario, nombre, apellido, alias, telefono, descripcion, profesion, plan;
    TextView txtNombre, txtApellido, txtAlias, txtTelefono, txtProfesion, txtPlan;
    EditText txtDescripcion;
    private procesador mProcesador;
    private DrawerLayout Fondo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_new_config_perfil);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(Fondo, newConfigPerfil.this);

    }

    private void Iniciar() {
        txtNombre = findViewById(R.id.tvNombre);
        txtApellido = findViewById(R.id.tvApellido);
        txtAlias = findViewById(R.id.tvAlias);
        txtTelefono = findViewById(R.id.tvTelefono);
        txtProfesion = findViewById(R.id.tvProfesion);
        txtPlan = findViewById(R.id.tvPlan);
        txtDescripcion = findViewById(R.id.etDescripcion);
        Fondo = findViewById(R.id.drawer_layout);

        obtenerDatos();

        txtNombre.setText(nombre);
        txtApellido.setText(apellido);
        txtAlias.setText(alias);
        txtTelefono.setText(telefono);
        txtProfesion.setText(profesion);
        txtPlan.setText(plan);
        txtDescripcion.setText(descripcion);
        mProcesador = new procesador();
    }

    private void obtenerDatos(){
        idUsuario = getIntent().getStringExtra("idUsuario");
        plan = getIntent().getStringExtra("plan");
    }
}