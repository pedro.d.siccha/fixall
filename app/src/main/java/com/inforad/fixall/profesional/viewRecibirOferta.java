package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.busquedaProvider;
import com.inforad.fixall.provider.invitacionProvider;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.ofertaProvider;
import com.inforad.fixall.provider.tokenProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class viewRecibirOferta extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {
    private ImageView imgDescripcion;
    private TextView txtPalabra, txtDescripcion, txtDistancia;
    private EditText inputOferta;
    private Button btnAceptar;
    private String idBusqueda, palabraClave, idUsuario, idEmpresario, alias, imgProfesional, descripcion;
    private double costo;
    private busquedaProvider mBusqueda;
    private FirebaseAuth mAuth;
    private userProvider mUserProvider;
    private ofertaProvider mOfertaProvider;
    private notificacionProvider mNotificacion;
    private tokenProvider mTokenProvider;
    private invitacionProvider mInvitacionProvider;
    private DatabaseReference mDatabase;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private procesador mProcesador;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_recibir_oferta);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewRecibirOferta.this);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                costo = Double.parseDouble(inputOferta.getText().toString());
                Oferta oferta = new Oferta(idEmpresario, palabraClave, idUsuario, alias, imgProfesional, idBusqueda, descripcion, "PENDIENTE", txtDistancia.getText().toString(), costo, new Date().getTime());
                mOfertaProvider.crearOferta(oferta, idEmpresario, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        enviarNotificacion(oferta);
                        Intent intent = new Intent(viewRecibirOferta.this, viewPerfilP.class);
                        startActivity(intent);
                    }
                });
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewRecibirOferta.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void obtenerIdInvitacion(){

        mDatabase.child("Invitacion_Profesional").child(mAuth.getCurrentUser().getUid()).orderByChild("estado").equalTo("PENDIENTE").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String idInvitacion = snapshot.getChildren().iterator().next().getKey();
                    mInvitacionProvider.actualizarInvitacionProfesional(idUsuario, idInvitacion, "FIN").addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            mDatabase.child("Invitacion_Empresario").child(idEmpresario).orderByChild("estado").equalTo("PENDIENTE").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if (snapshot.exists()){
                                        String idInvitacionEmpresario = snapshot.getChildren().iterator().next().getKey();
                                        mInvitacionProvider.actualizarInvitacionEmpresario(idEmpresario, idInvitacionEmpresario, "FIN").addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {

                                            }
                                        });
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void enviarNotificacion(Oferta oferta) {
        mTokenProvider.obtenerToken(idEmpresario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String token = snapshot.child("token").getValue().toString();
                    mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                ArrayList<Oferta> ofertaArrayList = new ArrayList<>();
                                ofertaArrayList.add(oferta);
                                Gson gson = new Gson();
                                String ofertas = gson.toJson(ofertaArrayList);
                                Map<String, String> map = new HashMap<>();
                                map.put("titulo", "NUEVA OFERTA");
                                map.put("contenido", "Nombre: " + snapshot.child("alias").getValue().toString() + "\nCosto: S/." + oferta.getCosto() + "\nDistancia: " + oferta.getDistancia());
                                map.put("ofertas", ofertas);
                                map.put("imgProfesional", snapshot.child("imagen").getValue().toString());
                                FCMBody fcmBody = new FCMBody(token, "high", "4500s", map);
                                mNotificacion.sendNotification(fcmBody).enqueue(new Callback<FCMResponse>() {
                                    @Override
                                    public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                                        if (response.body() != null){
                                            if (response.body().getSuccess() == 1){
                                                Toast.makeText(viewRecibirOferta.this, "Oferta enviada", Toast.LENGTH_SHORT).show();
                                            }else {
                                                Toast.makeText(viewRecibirOferta.this, "No se pudo enviar la notificación", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<FCMResponse> call, Throwable t) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void Iniciar() {
        imgDescripcion = findViewById(R.id.ivImgDescripcion);
        txtDescripcion = findViewById(R.id.tvDescripcion);
        inputOferta = findViewById(R.id.etOferta);
        btnAceptar = findViewById(R.id.bEnviarOferta);
        txtPalabra = findViewById(R.id.tvPalabra);
        txtDistancia = findViewById(R.id.tvDistancia);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mTokenProvider = new tokenProvider();
        mNotificacion = new notificacionProvider();
        mOfertaProvider = new ofertaProvider();
        mAuth = FirebaseAuth.getInstance();
        mUserProvider = new userProvider();
        idUsuario = mAuth.getCurrentUser().getUid();
        mBusqueda = new busquedaProvider();
        mInvitacionProvider = new invitacionProvider();
        mProcesador = new procesador();
        obtenerDatos();
        mostrarDatos();
        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewRecibirOferta.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    private void obtenerDatos() {
        idBusqueda = getIntent().getStringExtra("idBusqueda");
        palabraClave = getIntent().getStringExtra("palabraClave");

        txtPalabra.setText(palabraClave);

    }

    private void mostrarDatos() {
        mBusqueda.obtenerBusqueda(palabraClave, idBusqueda).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){

                    String imagDescripcion = snapshot.child("imagenDescripcion").getValue().toString();
                    descripcion = snapshot.child("descripcion").getValue().toString();
                    idEmpresario = snapshot.child("idUsuario").getValue().toString();
                    txtDescripcion.setText(descripcion);
                    txtDistancia.setText(snapshot.child("rangoBusqueda").getValue().toString() + " m.");
                    Picasso.with(viewRecibirOferta.this).load(imagDescripcion).into(imgDescripcion);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                break;
            case R.id.nav_pendiente:
                Intent viewPendiente = new Intent(viewRecibirOferta.this, viewCitaPendiente.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(viewRecibirOferta.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                Intent viewHistorial = new Intent(viewRecibirOferta.this, viewListaHistorialPro.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewRecibirOferta.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewRecibirOferta.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewRecibirOferta.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(viewRecibirOferta.this, panelcontrolEmpresario.class);
                intent.putExtra("idUsuario", idUsuario);
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewRecibirOferta.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewRecibirOferta.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                Intent club = new Intent(viewRecibirOferta.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}