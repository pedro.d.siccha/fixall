package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.fixall.R;
import com.inforad.fixall.model.Iso;
import com.inforad.fixall.provider.isoProvider;
import com.inforad.fixall.utilidades.CompresorBitmapImage;
import com.inforad.fixall.utilidades.FileUtilidades;
import com.squareup.picasso.Picasso;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class editarIso extends AppCompatActivity {

    private EditText inputCodigo, inputNombre;
    private Button btnGuardar;
    private ImageView btnCerrar;
    private CircleImageView imgIso;
    private isoProvider mIsoProvider;
    private FirebaseAuth mAuth;
    private String idUsuario, idIso;
    private ProgressDialog mProgressDialog;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private int flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_editar_iso);
        Inicio();

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressDialog.setMessage("Guardando Iso...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                guardarImagen();
            }
        });

        imgIso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                abrirGaleria();
            }
        });

    }

    private void abrirGaleria(){
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                imgIso.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
                flag = 1;
            }catch (Exception e){
                flag = 0;
            }
        }
    }

    private void guardarImagen(){

        if (flag == 1){
            if (!inputNombre.getText().toString().isEmpty()){
                if (!inputCodigo.getText().toString().isEmpty()){
                    byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
                    StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgIso").child(mAuth.getCurrentUser().getUid()).child(mImageFile.getName() + ".jpg");
                    UploadTask uploadTask = storage.putBytes(imageByte);
                    uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            if (task.isSuccessful()){
                                storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        String img = uri.toString();
                                        guardar(img);
                                    }
                                });
                            }else {
                            }
                        }
                    });
                }else {
                    mProgressDialog.dismiss();
                    Toast.makeText(this, "Por favor ingrese el codigo de su ISO", Toast.LENGTH_SHORT).show();
                }
            }else {
                mProgressDialog.dismiss();
                Toast.makeText(this, "Por favor ingrese el nombre de su ISO", Toast.LENGTH_SHORT).show();
            }
        }else {
            mIsoProvider.obtenerIso(idUsuario, idIso).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()){
                        guardar(snapshot.child("imagen").getValue().toString());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    private void guardar(String img) {
        String codigo = inputCodigo.getText().toString();
        String nombre = inputNombre.getText().toString();
        String nomImagen = img;
        if (!codigo.isEmpty()){
            if (!nombre.isEmpty()){
                Iso iso = new Iso(idIso, nombre, codigo, nomImagen);
                mIsoProvider.actualizarIso(iso, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(editarIso.this, "Iso actualizado correctamente", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }else {
                Toast.makeText(this, "Por favor ingrese un nombre", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, "Por favor ingrese un codigo de iso", Toast.LENGTH_SHORT).show();
        }
    }

    private void Inicio() {
        inputCodigo = findViewById(R.id.etCodigo);
        inputNombre = findViewById(R.id.etNombre);
        btnGuardar = findViewById(R.id.bGuardar);
        btnCerrar = findViewById(R.id.ivCerrar);
        imgIso = findViewById(R.id.civIso);

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mIsoProvider = new isoProvider();
        mProgressDialog = new ProgressDialog(editarIso.this);

        cargarDatos();
    }

    private void cargarDatos() {
        idIso = getIntent().getStringExtra("idIso");

        mIsoProvider.obtenerIso(idUsuario, idIso).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    inputCodigo.setText(snapshot.child("codigo").getValue().toString());
                    inputNombre.setText(snapshot.child("nombre").getValue().toString());
                    Picasso.with(editarIso.this).load(snapshot.child("imagen").getValue().toString()).into(imgIso);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}