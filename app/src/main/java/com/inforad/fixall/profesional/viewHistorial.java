package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.ofertaAceptadaAdaptador;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.userProvider;

import java.util.Collection;

public class viewHistorial extends AppCompatActivity {

    private RecyclerView recyclerListaCitaAceptado;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ofertaAceptadaAdaptador mOfertaAdaptador;
    private String idUsuario;
    private userProvider mUserProvider;
    private DrawerLayout backGroundPrincipal;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_historial);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(backGroundPrincipal, viewHistorial.this);

    }

    private void Iniciar() {

        backGroundPrincipal = findViewById(R.id.drawer_layout);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider = new userProvider();
        mProcesador = new procesador();

        recyclerListaCitaAceptado = findViewById(R.id.recyclerListaCitaAceptado);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaCitaAceptado.setLayoutManager(linearLayoutManager);

    }

    @Override
    protected void onStart() {
        super.onStart();// MoYFo4YKT0eDz2EmeiDuOBL6wWX2  Buh5tzBoHpWtimeW1pYdPlXQIFv2
        String consul = idUsuario + "/estado";
        mDatabase.child("Oferta").orderByChild(consul).equalTo("ACEPTADO").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String idEmpresario = snapshot.getChildren().iterator().next().getKey();

                    Query query = mDatabase.child("Oferta").child(idEmpresario).orderByChild("estado").equalTo("ACEPTADO");
                    FirebaseRecyclerOptions<Oferta> options = new FirebaseRecyclerOptions.Builder<Oferta>().setQuery(query, Oferta.class).build();
                    mOfertaAdaptador = new ofertaAceptadaAdaptador(options, viewHistorial.this);
                    recyclerListaCitaAceptado.setAdapter(mOfertaAdaptador);
                    mOfertaAdaptador.startListening();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}