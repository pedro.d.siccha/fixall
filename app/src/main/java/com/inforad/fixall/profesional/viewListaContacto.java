package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.contactosAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.model.Chat;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.provider.chatProvider;
import com.inforad.fixall.viewTipoUsuario;

import pl.droidsonroids.gif.GifImageView;

public class viewListaContacto extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private RecyclerView recyclerListaContactos;
    private String idUsuario, idChat;
    private contactosAdaptador mContactoAdaptador;
    private DrawerLayout drawerLayout;
    private ImageView btnAtras;
    private procesador mProcesador;
    private authProvider mAuthProvider;
    private GifImageView btnAsistente;
    private chatProvider mChatProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_lista_contacto);
        Inicio();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewListaContacto.this);
        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewListaContacto.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });
    }

    private void Inicio() {
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mProcesador = new procesador();
        mAuthProvider = new authProvider();
        mChatProvider = new chatProvider();

        recyclerListaContactos = findViewById(R.id.rvListaContactos);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaContactos.setLayoutManager(linearLayoutManager);

        cargarMenu();
    }

    private void cargarMenu() {
        drawerLayout = findViewById(R.id.drawer_layout);
        btnAtras = findViewById(R.id.ivAtras);

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewListaContacto.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();

        Query queryContacto = mDatabase.child("Chats").child(idUsuario).child("Users");
        FirebaseRecyclerOptions<Chat> options = new FirebaseRecyclerOptions.Builder<Chat>().setQuery(queryContacto, Chat.class).build();
        mContactoAdaptador = new contactosAdaptador(options, viewListaContacto.this);
        recyclerListaContactos.setAdapter(mContactoAdaptador);
        mContactoAdaptador.startListening();

    }
}