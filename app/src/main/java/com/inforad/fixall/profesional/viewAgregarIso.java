package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewAceptarProfesionalE;
import com.inforad.fixall.model.Iso;
import com.inforad.fixall.model.Trabajo;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.isoProvider;
import com.inforad.fixall.utilidades.CompresorBitmapImage;
import com.inforad.fixall.utilidades.FileUtilidades;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

public class viewAgregarIso extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private CircleImageView imgIso;
    private EditText inputNombre, inputCodigo;
    private Button btnGuardar;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private isoProvider mIsoProvider;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnAtras;
    private procesador mProcesador;
    private ProgressDialog mProgressDialog;
    private GifImageView btnAsistente;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_agregar_iso);
        Inicio();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewAgregarIso.this);

        imgIso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirGaleria();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressDialog.setMessage("Guardando Iso...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                guardarImagen();
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewAgregarIso.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(getApplicationContext(), panelcontrolProfesional.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void abrirGaleria(){
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                imgIso.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                //Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void Inicio() {
        imgIso = findViewById(R.id.civIso);
        inputNombre = findViewById(R.id.etNombreIso);
        inputCodigo = findViewById(R.id.etCodigoIso);
        btnGuardar = findViewById(R.id.bGuardar);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mIsoProvider = new isoProvider();
        mProcesador = new procesador();
        mProgressDialog = new ProgressDialog(viewAgregarIso.this);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        cargarMenu();

    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicioPro);

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewAgregarIso.this, panelcontrolProfesional.class);
                startActivity(intent);
            }
        });
    }

    private void guardarImagen(){
        if (!mImageFile.getPath().isEmpty()){
            if (!inputNombre.getText().toString().isEmpty()){
                if (!inputCodigo.getText().toString().isEmpty()){
                    byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
                    StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgIso").child(mAuth.getCurrentUser().getUid()).child(mImageFile.getName() + ".jpg");
                    UploadTask uploadTask = storage.putBytes(imageByte);
                    uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            if (task.isSuccessful()){
                                storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        String img = uri.toString();
                                        String idUsuario = mAuth.getCurrentUser().getUid();
                                        String idIso = mDatabase.push().getKey();
                                        Iso iso = new Iso(idIso, inputNombre.getText().toString(), inputCodigo.getText().toString(), img);
                                        mIsoProvider.crearIso(iso, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                mProgressDialog.dismiss();
                                                Intent intent = new Intent(viewAgregarIso.this, panelcontrolProfesional.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        });
                                    }
                                });
                            }else {
                            }
                        }
                    });
                }else {
                    mProgressDialog.dismiss();
                    Toast.makeText(this, "Por favor ingrese el codigo de su ISO", Toast.LENGTH_SHORT).show();
                }
            }else {
                mProgressDialog.dismiss();
                Toast.makeText(this, "Por favor ingrese el nombre de su ISO", Toast.LENGTH_SHORT).show();
            }
        }else {
            mProgressDialog.dismiss();
            Toast.makeText(this, "Por favor adjunte la imagen de su ISO", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                break;
            case R.id.nav_pendiente:
                Intent viewPendiente = new Intent(viewAgregarIso.this, viewCitaPendiente.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(viewAgregarIso.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                Intent viewHistorial = new Intent(viewAgregarIso.this, viewListaHistorialPro.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewAgregarIso.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewAgregarIso.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewAgregarIso.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(viewAgregarIso.this, panelcontrolEmpresario.class);
                intent.putExtra("idUsuario", idUsuario);
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewAgregarIso.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewAgregarIso.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                Intent club = new Intent(viewAgregarIso.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }
}