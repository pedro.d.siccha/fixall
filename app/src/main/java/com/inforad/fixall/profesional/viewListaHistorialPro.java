package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.fixall.MainActivity;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.historialAdaptado;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewHistprial;
import com.inforad.fixall.model.HistorialBusqueda;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

import pl.droidsonroids.gif.GifImageView;

public class viewListaHistorialPro extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private FirebaseAuth mAuth;
    private procesador mProcesador;
    private historialAdaptado mHistorialAdaptador;
    private DatabaseReference mDatabase;
    private RecyclerView recyclerListaHistorial;
    private String idUsuario;
    private authProvider mAuthProvide;
    private CheckBox banco;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_lista_historial_pro);
        Inicio();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewListaHistorialPro.this);
        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewListaHistorialPro.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });
    }

    private void Inicio() {
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        recyclerListaHistorial = findViewById(R.id.rvListaHistorial);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaHistorial.setLayoutManager(linearLayoutManager);
        mProcesador = new procesador();
        mAuthProvide = new authProvider();

        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicioPro);
        mProcesador.carcateristicaMenuPro(navigationView);
        banco = findViewById(R.id.cbBanco);
        mProcesador.carcateristicaMenuPro(navigationView);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewListaHistorialPro.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                Intent viewPendiente = new Intent(viewListaHistorialPro.this, viewPerfilP.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_pendiente:
                Intent viewHistorial = new Intent(viewListaHistorialPro.this, viewCitaPendiente.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(viewListaHistorialPro.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewListaHistorialPro.this, panelcontrolProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewListaHistorialPro.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                mAuthProvide.cerrarSesion();
                Intent viewCerrar = new Intent(viewListaHistorialPro.this, MainActivity.class);
                startActivity(viewCerrar);
                finish();
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewListaHistorialPro.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(viewListaHistorialPro.this, panelcontrolEmpresario.class);
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewListaHistorialPro.this, viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewListaHistorialPro.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                mProcesador.accesoClub(viewListaHistorialPro.this);
                Intent club = new Intent(viewListaHistorialPro.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query query = mDatabase.child("HistorialBusqueda").orderByChild("idProfesional").equalTo(idUsuario);
        FirebaseRecyclerOptions<HistorialBusqueda> options = new FirebaseRecyclerOptions.Builder<HistorialBusqueda>().setQuery(query, HistorialBusqueda.class).build();
        mHistorialAdaptador = new historialAdaptado(options, viewListaHistorialPro.this);
        recyclerListaHistorial.setAdapter(mHistorialAdaptador);
        mHistorialAdaptador.startListening();
    }
}