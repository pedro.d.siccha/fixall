package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.PalabraClave;
import com.inforad.fixall.model.UsuarioPalabra;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.palabraClaveProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.provider.usuariopalabraclaveProvider;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

public class viewAgregarPalabrasClave extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private EditText txtPalabra;
    private Button btnGuardar;
    private palabraClaveProvider mPalabraClave;
    private FirebaseAuth mAuth;
    private userProvider mUsuarioProvider;
    private usuariopalabraclaveProvider mUsuarioPalabraProvider;
    private DrawerLayout backGroundPrincipal;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnAtras;
    private procesador mProcesador;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_agregar_palabras_clave);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewAgregarPalabrasClave.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressDialog.setMessage("Guardando Palabra Clave...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                if (!txtPalabra.getText().toString().isEmpty()){
                    guardar(txtPalabra.getText().toString().toUpperCase());
                }else {
                    mProgressDialog.dismiss();
                    Toast.makeText(viewAgregarPalabrasClave.this, "Por favor ingrese una palabra clave valida", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(getApplicationContext(), panelcontrolProfesional.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar(){
        txtPalabra = findViewById(R.id.etPalabraClave);
        btnGuardar = findViewById(R.id.bGuardarProfesion);

        mUsuarioPalabraProvider = new usuariopalabraclaveProvider();
        mPalabraClave = new palabraClaveProvider();
        mAuth = FirebaseAuth.getInstance();
        mUsuarioProvider = new userProvider();
        mProcesador = new procesador();
        mProgressDialog = new ProgressDialog(viewAgregarPalabrasClave.this);

        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicioPro);

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewAgregarPalabrasClave.this, panelcontrolProfesional.class);
                startActivity(intent);
            }
        });
    }

    private void guardar(String palabra){
        String idUsuario = mAuth.getCurrentUser().getUid();


        mUsuarioProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String imgPro = snapshot.child("imagen").getValue().toString();
                    String nomPro = snapshot.child("nombre").getValue().toString();
                    String apePro = snapshot.child("apellido").getValue().toString();
                    String aliasPro = snapshot.child("alias").getValue().toString();

                    PalabraClave palabraClave = new PalabraClave(idUsuario, imgPro, nomPro, apePro, aliasPro, "", palabra, "ACTIVO");
                    mPalabraClave.crearPalabraClave(palabraClave, idUsuario, palabra).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            UsuarioPalabra usuario = new UsuarioPalabra(palabra, "ACTIVO");
                            mUsuarioPalabraProvider.crearUsuarioPalabraClave(usuario, idUsuario, palabra).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(viewAgregarPalabrasClave.this, "Palabra Clave registrada correctamente", Toast.LENGTH_SHORT).show();
                                    mProgressDialog.dismiss();
                                    Intent intent = new Intent(viewAgregarPalabrasClave.this, panelcontrolProfesional.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                break;
            case R.id.nav_pendiente:
                Intent viewPendiente = new Intent(viewAgregarPalabrasClave.this, viewCitaPendiente.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(viewAgregarPalabrasClave.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                Intent viewHistorial = new Intent(viewAgregarPalabrasClave.this, viewListaHistorialPro.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewAgregarPalabrasClave.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewAgregarPalabrasClave.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewAgregarPalabrasClave.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(viewAgregarPalabrasClave.this, panelcontrolEmpresario.class);
                intent.putExtra("idUsuario", mAuth.getCurrentUser().getUid());
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewAgregarPalabrasClave.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewAgregarPalabrasClave.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                Intent club = new Intent(viewAgregarPalabrasClave.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }
}