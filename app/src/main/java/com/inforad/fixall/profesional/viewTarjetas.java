package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.userProvider;

public class viewTarjetas extends AppCompatActivity {

    private Button botonPagar;
    private userProvider mUserProvider;
    private DrawerLayout backGroundPrincipal;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_tarjetas);
        Inicio();
        mProcesador.caracteristicasPlanDrawer(backGroundPrincipal, viewTarjetas.this);

        botonPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewTarjetas.this, viewFinSubscripcion.class);
                startActivity(intent);
            }
        });

    }

    private void Inicio() {
        botonPagar = findViewById(R.id.btnRealizarPago);
        backGroundPrincipal = findViewById(R.id.drawer_layout);

        mAuth = FirebaseAuth.getInstance();
        mUserProvider = new userProvider();
        mProcesador = new procesador();

        idUsuario = mAuth.getCurrentUser().getUid();
    }
}