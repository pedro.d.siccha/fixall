package com.inforad.fixall.profesional;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewDescripcionPlan;
import com.inforad.fixall.viewDestacarte;

public class destacarteProfesional extends AppCompatActivity {

    private LinearLayout btnGold, btnBlack;
    private Button btnOmitir;
    private String direccion, idUsuario;
    private userProvider mUserProvider;
    private FirebaseAuth mAuth;
    private ConstraintLayout Fondo;
    protected procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_destacarte_profesional);
        Iniciar();
        mProcesador.caracteristicasPlanConstraint(Fondo, destacarteProfesional.this);

        btnGold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(destacarteProfesional.this, viewDescripcionPlan.class);
                intent.putExtra("plan", "GOLD");
                intent.putExtra("direccion", "DescartarProfesional");
                startActivity(intent);
            }
        });

        btnBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(destacarteProfesional.this, viewDescripcionPlan.class);
                intent.putExtra("plan", "BLACK");
                intent.putExtra("direccion", "DescartarProfesional");
                startActivity(intent);
            }
        });

        btnOmitir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(destacarteProfesional.this, viewPerfilP.class);
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        btnGold = findViewById(R.id.llGold);
        btnBlack = findViewById(R.id.llBlack);
        btnOmitir = findViewById(R.id.bOmitir);
        Fondo = findViewById(R.id.clFondo);

        mUserProvider = new userProvider();
        mAuth = FirebaseAuth.getInstance();
        mProcesador = new procesador();

        idUsuario = mAuth.getCurrentUser().getUid();
    }
}