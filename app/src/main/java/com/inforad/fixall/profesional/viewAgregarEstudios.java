package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewAceptarProfesionalE;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.estudioProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.utilidades.DatePickerFragment;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewDatosPersonales;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

import pl.droidsonroids.gif.GifImageView;

public class viewAgregarEstudios extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private EditText txtColegio, txtGrado, txtFecInicio, txtFecFin;
    private Button btnGuardar;
    private estudioProvider mEstudio;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private DrawerLayout backGroundPrincipal;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private userProvider mUserProvider;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnAtras;
    private procesador mProcesador;
    private ProgressDialog mProgressDialog;
    private GifImageView btnAsistente;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_agregar_estudios);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewAgregarEstudios.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressDialog.setMessage("Guardando Estudio...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                if (!txtColegio.getText().toString().isEmpty()){
                    if (!txtGrado.getText().toString().isEmpty()){
                        if (!txtFecInicio.getText().toString().isEmpty()){
                            if (!txtFecFin.getText().toString().isEmpty()){
                                guardarEstudio(txtColegio.getText().toString(), txtGrado.getText().toString(), txtFecInicio.getText().toString(), txtFecFin.getText().toString(), "", idUsuario);
                            }else {
                                Toast.makeText(viewAgregarEstudios.this, "Por favor ingrese una fecha final correcta", Toast.LENGTH_SHORT).show();
                                mProgressDialog.dismiss();
                            }
                        }else {
                            Toast.makeText(viewAgregarEstudios.this, "Por favor ingrese una fecha de inicio correcta", Toast.LENGTH_SHORT).show();
                            mProgressDialog.dismiss();
                        }
                    }else {
                        Toast.makeText(viewAgregarEstudios.this, "Por favor ingrese su institucion educativa", Toast.LENGTH_SHORT).show();
                        mProgressDialog.dismiss();
                    }
                }else{
                    Toast.makeText(viewAgregarEstudios.this, "Por favor ingrese su carrera profesional", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }

            }
        });

        txtFecInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.etFecInicio:
                        showDatePickerDialog();
                        break;
                }
            }
        });

        txtFecFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.etFecFin:
                        showDatePickerDialog2();
                        break;
                }
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewAgregarEstudios.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(getApplicationContext(), panelcontrolProfesional.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void showDatePickerDialog2() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int anio, int mes, int dia) {
                final String selectedDate = dia + "/" + (mes + 1) + "/" + anio;
                txtFecFin.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int anio, int mes, int dia) {
                final String selectedDate = dia + "/" + (mes + 1) + "/" + anio;
                txtFecInicio.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void Iniciar(){
        txtColegio = findViewById(R.id.etInstitucion);
        txtGrado = findViewById(R.id.etGrado);
        txtFecInicio = findViewById(R.id.etFecInicio);
        txtFecFin = findViewById(R.id.etFecFin);
        btnGuardar = findViewById(R.id.bGuardar);
        backGroundPrincipal = findViewById(R.id.drawer_layout);
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mEstudio = new estudioProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider = new userProvider();
        mProcesador = new procesador();
        mProgressDialog = new ProgressDialog(viewAgregarEstudios.this);

        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicioPro);

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewAgregarEstudios.this, panelcontrolProfesional.class);
                startActivity(intent);
            }
        });
    }

    private void guardarEstudio(String colegio, String grado, String fecinicio, String fecfin, String descripcion, String idUsuario){
        String idEstudio = mDatabase.push().getKey();
        EstudioProfesional estudio = new EstudioProfesional(idEstudio, colegio, grado, fecinicio, fecfin, descripcion);
        mEstudio.crearEstudio(estudio, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mProgressDialog.dismiss();
                Toast.makeText(viewAgregarEstudios.this, "Estudios Agregados Correctamente", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(viewAgregarEstudios.this, panelcontrolProfesional.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                break;
            case R.id.nav_pendiente:
                Intent viewPendiente = new Intent(viewAgregarEstudios.this, viewCitaPendiente.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(viewAgregarEstudios.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                Intent viewHistorial = new Intent(viewAgregarEstudios.this, viewListaHistorialPro.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewAgregarEstudios.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewAgregarEstudios.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewAgregarEstudios.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(viewAgregarEstudios.this, panelcontrolEmpresario.class);
                intent.putExtra("idUsuario", idUsuario);
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewAgregarEstudios.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewAgregarEstudios.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                Intent club = new Intent(viewAgregarEstudios.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}