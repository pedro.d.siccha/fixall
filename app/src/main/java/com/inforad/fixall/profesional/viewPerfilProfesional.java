package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hbb20.CountryCodePicker;
import com.inforad.fixall.MainActivity;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.clubEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewHistprial;
import com.inforad.fixall.empresario.viewListaFavorito;
import com.inforad.fixall.empresario.viewOfertasRecibidas;
import com.inforad.fixall.empresario.viewPerfilEmpresario;
import com.inforad.fixall.model.Plan;
import com.inforad.fixall.model.User;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.utilidades.CompresorBitmapImage;
import com.inforad.fixall.utilidades.FileUtilidades;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewDatosPersonales;
import com.inforad.fixall.viewIngresarDireccion;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class viewPerfilProfesional extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private EditText txtNombre, txtApellido, txtAlias, txtDni, txtNumTelefono;
    private Button btnGuardar;
    private CountryCodePicker codigoRegion;
    private CircleImageView btnSubirImagen1;
    private FloatingActionButton btnSubirImagen2;
    private userProvider mUserProvider;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private procesador mProcesador;
    private authProvider mAuthProvider;
    private CheckBox banco;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_perfil_profesional);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewPerfilProfesional.this);

        btnSubirImagen1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarGaleria();
            }
        });

        btnSubirImagen2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarGaleria();
            }
        });
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressDialog.setMessage("Cargando Imagen...");
                mProgressDialog.setTitle("Espere...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                guardarImagen(txtAlias.getText().toString());
            }
        });

    }

    private void mostrarGaleria() {
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                btnSubirImagen1.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                // Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void guardarImagen(String alias){
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgUsuario").child(mAuth.getCurrentUser().getUid()).child(alias + ".jpg");
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String img = uri.toString();
                            editarUsuario(img);
                        }
                    });
                }else {
                    // Toast.makeText(viewDatosPersonales.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void editarUsuario(String img) {
        String user_id = mAuth.getCurrentUser().getUid();
        String nombre = txtNombre.getText().toString();
        String apellido = txtApellido.getText().toString();
        String alias = txtAlias.getText().toString();
        String numero = txtNumTelefono.getText().toString();
        String numTelfono ="+" + codigoRegion.getFullNumber() + numero;
        String imgUsuario = img;
        String dni = txtDni.getText().toString();

        mUserProvider.actualizarUsuario(user_id, img, nombre, apellido, alias, dni, numTelfono).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mProgressDialog.dismiss();
                finish();
            }
        });
    }


    private void Iniciar() {
        txtNombre = findViewById(R.id.txtNombre);
        txtApellido = findViewById(R.id.txtApellido);
        txtAlias = findViewById(R.id.txtAlias);
        txtDni = findViewById(R.id.txtDni);
        txtNumTelefono = findViewById(R.id.txtTelefono);
        btnGuardar = findViewById(R.id.btnSiguienteDP);
        codigoRegion = findViewById(R.id.cbRegion);
        btnSubirImagen1 = findViewById(R.id.civPerfil);
        btnSubirImagen2 = findViewById(R.id.fabPerfil);
        mProgressDialog = new ProgressDialog(viewPerfilProfesional.this);

        mUserProvider = new userProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProcesador = new procesador();
        mAuthProvider = new authProvider();

        cargarDatos();
        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);
        //banco = findViewById(R.id.cbBanco);
        mProcesador.carcateristicaMenuPro(navigationView);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewPerfilProfesional.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    private void cargarDatos(){
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    txtNombre.setText(snapshot.child("nombre").getValue().toString());
                    txtApellido.setText(snapshot.child("apellido").getValue().toString());
                    txtAlias.setText(snapshot.child("alias").getValue().toString());
                    txtDni.setText(snapshot.child("dni").getValue().toString());
                    txtNumTelefono.setText(snapshot.child("telefono").getValue().toString());
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(viewPerfilProfesional.this).load(img).into(btnSubirImagen1);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicio:
                Intent viewInicio = new Intent(viewPerfilProfesional.this, panelcontrolEmpresario.class);
                startActivity(viewInicio);
                break;
            case R.id.nav_buscar:
                Intent viewBuscar = new Intent(viewPerfilProfesional.this, viewPerfilEmpresario.class);
                viewBuscar.putExtra("idUsuario", idUsuario);
                viewBuscar.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(viewBuscar);
                break;
            case R.id.nav_ofertas:
                Intent viewPerfil = new Intent(viewPerfilProfesional.this, viewOfertasRecibidas.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_perfil:
                break;
            case R.id.nav_historial:
                Intent viewHist = new Intent(viewPerfilProfesional.this, viewHistprial.class);
                startActivity(viewHist);
                break;
            case  R.id.nav_cambio_perfilEmp:
                Intent modoProfesional = new Intent(viewPerfilProfesional.this, viewPerfilP.class);
                startActivity(modoProfesional);
                break;
            case R.id.nav_banco:
                if (banco.isChecked()) {
                    //Toast.makeText(this, "BANCO", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(this, "PERSONA", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewPerfilProfesional.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewPerfilProfesional.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewPerfilProfesional.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_favoritos:
                Intent viewFavorito = new Intent(viewPerfilProfesional.this, viewListaFavorito.class);
                startActivity(viewFavorito);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewPerfilProfesional.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                mProcesador.accesoClub(viewPerfilProfesional.this);
                Intent viewClub = new Intent(viewPerfilProfesional.this, clubEmpresario.class);
                startActivity(viewClub);
                break;
            case R.id.nav_logoit:
                mAuthProvider.cerrarSesion();
                Intent viewCerrar = new Intent(viewPerfilProfesional.this, MainActivity.class);
                startActivity(viewCerrar);
                finish();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }
}