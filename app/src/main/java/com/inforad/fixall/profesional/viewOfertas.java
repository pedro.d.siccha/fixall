package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.ofertaAceptadaAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

import pl.droidsonroids.gif.GifImageView;

public class viewOfertas extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerListaOdertaAceptada;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ofertaAceptadaAdaptador mOfertaProvider;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private procesador mProcesador;
    private GifImageView btnAsistente;
    private String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_ofertas);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewOfertas.this);
        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewOfertas.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        btnAsistente = findViewById(R.id.givAvatarVaron);
        recyclerListaOdertaAceptada = findViewById(R.id.recyclerListaOfertaAceptada);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaOdertaAceptada.setLayoutManager(linearLayoutManager);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mProcesador = new procesador();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();

        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewOfertas.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        String idUsuario = mAuth.getCurrentUser().getUid();
        Query query = mDatabase.child("Oferta").orderByKey().equalTo(idUsuario).orderByChild("estado").equalTo("ACEPTADO");
        FirebaseRecyclerOptions<Oferta> options = new FirebaseRecyclerOptions.Builder<Oferta>().setQuery(query, Oferta.class).build();
        mOfertaProvider = new ofertaAceptadaAdaptador(options, viewOfertas.this);
        recyclerListaOdertaAceptada.setAdapter(mOfertaProvider);
        mOfertaProvider.startListening();

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                break;
            case R.id.nav_pendiente:
                Intent viewPendiente = new Intent(viewOfertas.this, viewCitaPendiente.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(viewOfertas.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                Intent viewHistorial = new Intent(viewOfertas.this, viewListaHistorialPro.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewOfertas.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewOfertas.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewOfertas.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(viewOfertas.this, panelcontrolEmpresario.class);
                intent.putExtra("idUsuario", mAuth.getCurrentUser().getUid());
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewOfertas.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewOfertas.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                Intent club = new Intent(viewOfertas.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}