package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.fixall.MainActivity;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.descripcionAdaptador;
import com.inforad.fixall.adapters.estudioAdaptador;
import com.inforad.fixall.adapters.historialAdaptado;
import com.inforad.fixall.adapters.isoAdaptador;
import com.inforad.fixall.adapters.palabraAdaptado;
import com.inforad.fixall.adapters.profesionAdaptador;
import com.inforad.fixall.adapters.trabajoAdaptador;
import com.inforad.fixall.agregarDireccion;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.Descripcion;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.model.HistorialBusqueda;
import com.inforad.fixall.model.Iso;
import com.inforad.fixall.model.Profesion;
import com.inforad.fixall.model.ProfesionProfesional;
import com.inforad.fixall.model.Trabajo;
import com.inforad.fixall.model.UsuarioPalabra;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.provider.perfilProvider;
import com.inforad.fixall.provider.profesionalProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.utilidades.CompresorBitmapImage;
import com.inforad.fixall.utilidades.FileUtilidades;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class viewPerfilP extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private CircleImageView imgPerfil;
    private TextView txtNombre, txtApellido, txtAlias, txtEdad;
    private RecyclerView rvListaEstudios, rvListaTrabajo, rvListaPClave, rvListaIso, rvComentario, rvDescripcion;
    private estudioAdaptador mEstudioAdaptador;
    private trabajoAdaptador mTrabajoAdaptador;
    private palabraAdaptado mPalabraAdapter;
    private isoAdaptador mIsoAdapter;
    private descripcionAdaptador mDescripcionAdapter;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private DatabaseReference mDatabase;
    private userProvider mUserProvider;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnAtras, btnMenu;
    private historialAdaptado mHistorialBusqueda;
    private procesador mProcesador;
    private authProvider mAuthProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_perfil_p);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewPerfilP.this);

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK){
            Intent intent = new Intent(getApplicationContext(), viewTipoUsuario.class);
            startActivity(intent);
        }
        return super.onKeyUp(keyCode, event);
    }

    private void Iniciar() {
        imgPerfil = findViewById(R.id.imgFotoPerfil);
        txtNombre = findViewById(R.id.tvNomPerfil);
        txtApellido = findViewById(R.id.tvApePerfil);
        txtAlias = findViewById(R.id.tvAliasPerfil);
        txtEdad = findViewById(R.id.tvEdadPerfil);
        rvListaEstudios = findViewById(R.id.recyclerListaEstudios);
        LinearLayoutManager linearLayoutManagerEstudios = new LinearLayoutManager(this);
        rvListaEstudios.setLayoutManager(linearLayoutManagerEstudios);
        rvListaTrabajo = findViewById(R.id.recyclerListaTrabajo);
        LinearLayoutManager linearLayoutManagerTrabajo = new LinearLayoutManager(this);
        rvListaTrabajo.setLayoutManager(linearLayoutManagerTrabajo);
        rvListaPClave = findViewById(R.id.recyclerListaPalabraClave);
        LinearLayoutManager linearLayoutManagerPClave = new LinearLayoutManager(this);
        rvListaPClave.setLayoutManager(linearLayoutManagerPClave);
        rvListaIso = findViewById(R.id.recyclerListaIso);
        LinearLayoutManager linearLayoutManagerIso = new LinearLayoutManager(this);
        rvListaIso.setLayoutManager(linearLayoutManagerIso);
        rvComentario = findViewById(R.id.recyclerListaComentario);
        LinearLayoutManager linearLayoutManagerComentario = new LinearLayoutManager(this);
        rvComentario.setLayoutManager(linearLayoutManagerComentario);
        rvDescripcion = findViewById(R.id.recyclerListaDescripcion);
        LinearLayoutManager linearLayoutManagerDescripcion = new LinearLayoutManager(this);
        rvDescripcion.setLayoutManager(linearLayoutManagerDescripcion);

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserProvider = new userProvider();
        mProcesador = new procesador();
        mAuthProvider = new authProvider();

        cargarDatos();
        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        btnAtras = findViewById(R.id.ivAtras);
        btnMenu = findViewById(R.id.ivMenu);
        drawerLayout = findViewById(R.id.clBackground);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicioPro);
        mProcesador.carcateristicaMenuPro(navigationView);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewPerfilP.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                break;
            case R.id.nav_pendiente:
                Intent viewPendiente = new Intent(viewPerfilP.this, viewCitaPendiente.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(viewPerfilP.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                Intent viewHistorial = new Intent(viewPerfilP.this, viewListaHistorialPro.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewPerfilP.this, panelcontrolProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewPerfilP.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                mAuthProvider.cerrarSesion();
                Intent viewCerrar = new Intent(viewPerfilP.this, MainActivity.class);
                startActivity(viewCerrar);
                finish();
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewPerfilP.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(viewPerfilP.this, panelcontrolEmpresario.class);
                intent.putExtra("idUsuario", idUsuario);
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewPerfilP.this, viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewPerfilP.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                mProcesador.accesoClub(viewPerfilP.this);
                Intent club = new Intent(viewPerfilP.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }

    private void cargarDatos() {
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(viewPerfilP.this).load(img).into(imgPerfil);
                    txtNombre.setText(snapshot.child("nombre").getValue().toString());
                    txtApellido.setText(snapshot.child("apellido").getValue().toString());
                    txtEdad.setText(" " + snapshot.child("edad").getValue().toString() + " años");
                    txtAlias.setText(snapshot.child("alias").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryEstudios = mDatabase.child("Estudio").child(idUsuario);
        FirebaseRecyclerOptions<EstudioProfesional> options = new FirebaseRecyclerOptions.Builder<EstudioProfesional>().setQuery(queryEstudios, EstudioProfesional.class).build();
        mEstudioAdaptador = new estudioAdaptador(options, viewPerfilP.this);
        rvListaEstudios.setAdapter(mEstudioAdaptador);
        mEstudioAdaptador.startListening();

        Query queryIso = mDatabase.child("Iso").child(idUsuario);
        FirebaseRecyclerOptions<Iso> optionsIso = new FirebaseRecyclerOptions.Builder<Iso>().setQuery(queryIso, Iso.class).build();
        mIsoAdapter = new isoAdaptador(optionsIso, viewPerfilP.this);
        rvListaIso.setAdapter(mIsoAdapter);
        mIsoAdapter.startListening();

        Query queryTrabajo = mDatabase.child("Trabajo").child(idUsuario);
        FirebaseRecyclerOptions<Trabajo> options2 = new FirebaseRecyclerOptions.Builder<Trabajo>().setQuery(queryTrabajo, Trabajo.class).build();
        mTrabajoAdaptador = new trabajoAdaptador(options2, viewPerfilP.this);
        rvListaTrabajo.setAdapter(mTrabajoAdaptador);
        mTrabajoAdaptador.startListening();

        Query queryPalabra = mDatabase.child("Usuario_PalabraClave").child(idUsuario);
        FirebaseRecyclerOptions<UsuarioPalabra> options3 = new FirebaseRecyclerOptions.Builder<UsuarioPalabra>().setQuery(queryPalabra, UsuarioPalabra.class).build();
        mPalabraAdapter = new palabraAdaptado(options3, viewPerfilP.this);
        rvListaPClave.setAdapter(mPalabraAdapter);
        mPalabraAdapter.startListening();

        Query query = mDatabase.child("HistorialBusqueda").orderByChild("idProfesional").equalTo(idUsuario);
        FirebaseRecyclerOptions<HistorialBusqueda> optionsComentario = new FirebaseRecyclerOptions.Builder<HistorialBusqueda>().setQuery(query, HistorialBusqueda.class).build();
        mHistorialBusqueda = new historialAdaptado(optionsComentario, viewPerfilP.this);
        rvComentario.setAdapter(mHistorialBusqueda);
        mHistorialBusqueda.startListening();

        Query queryDescripcion = mDatabase.child("Descripcion").child(idUsuario);
        FirebaseRecyclerOptions<Descripcion> optionsDescripcion = new FirebaseRecyclerOptions.Builder<Descripcion>().setQuery(queryDescripcion, Descripcion.class).build();
        mDescripcionAdapter = new descripcionAdaptador(optionsDescripcion, viewPerfilP.this);
        rvDescripcion.setAdapter(mDescripcionAdapter);
        mDescripcionAdapter.startListening();
    }
}