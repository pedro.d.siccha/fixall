package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.provider.estudioProvider;

public class editarEstudios extends AppCompatActivity {
    private EditText inputTitulo, inputUniversidad, inputFecInicio, inputFecFin;
    private Button btnGuardar;
    private String idUsuario, idEstudio;
    private FirebaseAuth mAuth;
    private estudioProvider mEstudioProvider;
    private ImageView btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_editar_estudios);
        Iniciar();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardar();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void guardar() {
        String titulo = inputTitulo.getText().toString();
        String universidad = inputUniversidad.getText().toString();
        String fecInicio = inputFecInicio.getText().toString();
        String fecFin = inputFecFin.getText().toString();
        if (!titulo.isEmpty()){
            if (!universidad.isEmpty()){
                if (!fecInicio.isEmpty()){
                    if (!fecFin.isEmpty()){
                        EstudioProfesional estudio = new EstudioProfesional(idEstudio, titulo, universidad, fecInicio, fecFin, "");
                        mEstudioProvider.actualizarEstudio(estudio, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(editarEstudios.this, "Estudio actualizado correctamente", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });
                    }else {
                        Toast.makeText(this, "Por favor ingrese una fecha final válida", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(this, "Por favor ingrese una fecha válida", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(this, "Por favor ingrese una istitución educativa", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, "Por favor ingrese una profesion", Toast.LENGTH_SHORT).show();
        }
    }

    private void Iniciar() {
        inputTitulo = findViewById(R.id.etTitulo);
        inputUniversidad = findViewById(R.id.etUniversidad);
        inputFecInicio = findViewById(R.id.etFecInicio);
        inputFecFin = findViewById(R.id.etFecFin);
        btnGuardar = findViewById(R.id.bGuardar);
        btnCerrar = findViewById(R.id.ivCerrar);

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mEstudioProvider = new estudioProvider();

        cargarDatos();
    }

    private void cargarDatos() {
        idEstudio = getIntent().getStringExtra("idEstudio");

        mEstudioProvider.obtenerEstudio(idUsuario, idEstudio).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    inputTitulo.setText(snapshot.child("nombreColegio").getValue().toString());
                    inputUniversidad.setText(snapshot.child("grado").getValue().toString());
                    inputFecInicio.setText(snapshot.child("fecInicio").getValue().toString());
                    inputFecFin.setText(snapshot.child("fecFin").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}