package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.model.ProfesionProfesional;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.estudioProvider;
import com.inforad.fixall.provider.profesionProfesionalProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.utilidades.DatePickerFragment;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

public class viewAgregarTrabajo extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private EditText txtUniversidad, txtGrado, txtFecInicio, txtFecFin;
    private Button btnGuardar;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private profesionProfesionalProvider mProfesion;
    private userProvider mUserProvider;
    private DrawerLayout backGroundPrincipal;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private procesador mProcesador;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_agregar_trabajo);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(backGroundPrincipal, viewAgregarTrabajo.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressDialog.setMessage("Guardando Trabajo ...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                guardarEstudio(txtUniversidad.getText().toString(), txtGrado.getText().toString(), txtFecInicio.getText().toString(), txtFecFin.getText().toString(), "", idUsuario);
            }
        });

        txtFecInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.etFecInicio:
                        showDatePickerDialog();
                        break;
                }
            }
        });
        
        txtFecFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.etFecInicio:
                        showDatePickerDialog2();
                        break;
                }
            }
        });
    }

    private void showDatePickerDialog2() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int anio, int mes, int dia) {
                final String selectedDate = dia + "/" + (mes + 1) + "/" + anio;
                txtFecFin.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int anio, int mes, int dia) {
                final String selectedDate = dia + "/" + (mes + 1) + "/" + anio;
                txtFecInicio.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void Iniciar(){
        txtUniversidad = findViewById(R.id.etInstitucion);
        txtGrado = findViewById(R.id.etGrado);
        txtFecInicio = findViewById(R.id.etFecInicio);
        txtFecFin = findViewById(R.id.etFecFin);
        btnGuardar = findViewById(R.id.bGuardar);
        backGroundPrincipal = findViewById(R.id.drawer_layout);

        mUserProvider = new userProvider();
        mProfesion = new profesionProfesionalProvider();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mProfesion = new profesionProfesionalProvider();
        mProgressDialog = new ProgressDialog(viewAgregarTrabajo.this);

        cargarMenu();
    }

    private void cargarMenu() {
        navigationView = findViewById(R.id.nvMenu);
        drawerLayout = findViewById(R.id.drawer_layout);
        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicio);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewAgregarTrabajo.this, viewTipoUsuario.class);
                startActivity(intent);
            }
        });
    }

    private void guardarEstudio(String colegio, String grado, String fecinicio, String fecfin, String descripcion, String idUsuario){
        ProfesionProfesional profesion = new ProfesionProfesional("", colegio, grado, fecinicio, fecfin, descripcion);
        mProfesion.crearProfesionProfesional(profesion, idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(viewAgregarTrabajo.this, "Profesión Agregada Correctamente", Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
                Intent intent = new Intent(viewAgregarTrabajo.this, viewPerfilP.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                break;
            case R.id.nav_pendiente:
                Intent viewPendiente = new Intent(viewAgregarTrabajo.this, viewCitaPendiente.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(viewAgregarTrabajo.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                Intent viewHistorial = new Intent(viewAgregarTrabajo.this, viewListaHistorialPro.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewAgregarTrabajo.this, viewPerfilProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewAgregarTrabajo.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewAgregarTrabajo.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(viewAgregarTrabajo.this, panelcontrolEmpresario.class);
                intent.putExtra("idUsuario", idUsuario);
                intent.putExtra("tipoUsuario", "EMPRESARIO");
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewAgregarTrabajo.this, com.inforad.fixall.viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewAgregarTrabajo.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                Intent club = new Intent(viewAgregarTrabajo.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}