package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.MainActivity;
import com.inforad.fixall.R;
import com.inforad.fixall.adapters.busquedaAdaptador;
import com.inforad.fixall.adapters.palabraAdaptado;
import com.inforad.fixall.adapters.palabraUsuarioAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.Invitacion;
import com.inforad.fixall.model.PalabraClave;
import com.inforad.fixall.model.UsuarioPalabra;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.provider.usuariopalabraclaveProvider;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

import pl.droidsonroids.gif.GifImageView;

public class viewCitaPendiente extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerListaCitaPendiente;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private palabraUsuarioAdaptador mPalabraAdapter;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView btnMenu, btnAtras;
    private procesador mProcesador;
    private authProvider mAuthProvide;
    private GifImageView btnAsistente;
    private String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_cita_pendiente);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewCitaPendiente.this);

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewCitaPendiente.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar(){
        btnAsistente = findViewById(R.id.givAvatarVaron);
        recyclerListaCitaPendiente = findViewById(R.id.recyclerListaCitaPendiente);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerListaCitaPendiente.setLayoutManager(linearLayoutManager);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mAuthProvide = new authProvider();
        mProcesador = new procesador();

        cargarMenu();
    }

    private void cargarMenu() {
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nvMenu);

        btnMenu = findViewById(R.id.ivMenu);
        btnAtras = findViewById(R.id.ivAtras);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_inicioPro);
        mProcesador.carcateristicaMenuPro(navigationView);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.END)){
                    drawerLayout.closeDrawer(GravityCompat.END);
                }else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewCitaPendiente.this, viewPerfilP.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        String idUsuario = mAuth.getCurrentUser().getUid();

        Query query = mDatabase.child("Usuario_PalabraClave").child(idUsuario);
        FirebaseRecyclerOptions<UsuarioPalabra> options = new FirebaseRecyclerOptions.Builder<UsuarioPalabra>().setQuery(query, UsuarioPalabra.class).build();
        mPalabraAdapter = new palabraUsuarioAdaptador(options, viewCitaPendiente.this);
        recyclerListaCitaPendiente.setAdapter(mPalabraAdapter);
        mPalabraAdapter.startListening();

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_inicioPro:
                Intent viewPendiente = new Intent(viewCitaPendiente.this, viewPerfilP.class);
                startActivity(viewPendiente);
                break;
            case R.id.nav_pendiente:
                break;
            case R.id.nav_aceptado:
                Intent viewAceptado = new Intent(viewCitaPendiente.this, viewHistorial.class);
                startActivity(viewAceptado);
                break;
            case R.id.nav_historial:
                Intent viewHistorial = new Intent(viewCitaPendiente.this, viewListaHistorialPro.class);
                startActivity(viewHistorial);
                break;
            case R.id.nav_perfil:
                Intent viewPerfil = new Intent(viewCitaPendiente.this, panelcontrolProfesional.class);
                startActivity(viewPerfil);
                break;
            case R.id.nav_plan:
                Intent viewPlan = new Intent(viewCitaPendiente.this, viewSubscripcion.class);
                startActivity(viewPlan);
                break;
            case R.id.nav_logoit:
                mAuthProvide.cerrarSesion();
                Intent viewCerrar = new Intent(viewCitaPendiente.this, MainActivity.class);
                startActivity(viewCerrar);
                finish();
                break;
            case R.id.nav_contactos:
                Intent viewContacto = new Intent(viewCitaPendiente.this, viewListaContacto.class);
                startActivity(viewContacto);
                break;
            case R.id.nav_cambio_perfil:
                Intent intent = new Intent(viewCitaPendiente.this, panelcontrolEmpresario.class);
                startActivity(intent);
                break;
            case R.id.nav_billetera:
                Intent viewBilletera = new Intent(viewCitaPendiente.this, viewBilletera.class);
                startActivity(viewBilletera);
                break;
            case R.id.nav_direccion:
                Intent viewDireccion = new Intent(viewCitaPendiente.this, viewConfigDirecciones.class);
                startActivity(viewDireccion);
                break;
            case R.id.nav_club:
                mProcesador.accesoClub(viewCitaPendiente.this);
                Intent club = new Intent(viewCitaPendiente.this, clubProfesional.class);
                startActivity(club);
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }
}