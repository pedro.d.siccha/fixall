package com.inforad.fixall.profesional;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.WindowManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.userProvider;

public class viewFinServicio extends AppCompatActivity {

    private userProvider mUserProvider;
    private DrawerLayout backGroundPrincipal;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private procesador mProcesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_fin_servicio);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(backGroundPrincipal, viewFinServicio.this);
    }

    private void Iniciar() {
        backGroundPrincipal = findViewById(R.id.dlPanelPro);

        mAuth = FirebaseAuth.getInstance();
        mUserProvider = new userProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mProcesador = new procesador();
    }
    
}