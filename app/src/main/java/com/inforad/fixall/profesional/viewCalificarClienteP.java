package com.inforad.fixall.profesional;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.procesos.procesador;

import pl.droidsonroids.gif.GifImageView;

public class viewCalificarClienteP extends AppCompatActivity {

    private Button botonPerfil;
    private procesador mProcesador;
    private DrawerLayout Fondo;
    private GifImageView btnAsistente;
    private FirebaseAuth mAuth;
    private String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_calificar_cliente_p);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(Fondo, viewCalificarClienteP.this);

        botonPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewCalificarClienteP.this, newConfigPerfil.class);
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewCalificarClienteP.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });


    }

    private void Iniciar() {
        botonPerfil = findViewById(R.id.btnPerfil);
        Fondo = findViewById(R.id.drawer_layout);
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mProcesador = new procesador();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
    }
}