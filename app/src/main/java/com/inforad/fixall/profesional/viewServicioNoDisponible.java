package com.inforad.fixall.profesional;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.procesos.procesador;

import pl.droidsonroids.gif.GifImageView;

public class viewServicioNoDisponible extends AppCompatActivity {

    private Button botonPerfil;
    private procesador mProcesador;
    private ConstraintLayout Fondo;
    private GifImageView btnAsistente;
    private FirebaseAuth mAuth;
    private String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_servicio_no_disponible);
        Inicio();
        mProcesador.caracteristicasPlanConstraint(Fondo, viewServicioNoDisponible.this);

        botonPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewServicioNoDisponible.this, newConfigPerfil.class);
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewServicioNoDisponible.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Inicio() {
        Fondo = findViewById(R.id.clFondo);
        botonPerfil = findViewById(R.id.btnPerfil);
        btnAsistente = findViewById(R.id.givAvatarVaron);

        mProcesador = new procesador();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
    }
}