package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.adapters.direccionAdaptador;
import com.inforad.fixall.adapters.tarjetaAdaptador;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.clubEmpresario;
import com.inforad.fixall.empresario.detalleEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewHistprial;
import com.inforad.fixall.empresario.viewListaFavorito;
import com.inforad.fixall.empresario.viewOfertasRecibidas;
import com.inforad.fixall.empresario.viewPerfilEmpresario;
import com.inforad.fixall.model.Direccion;
import com.inforad.fixall.model.TarjetaCredito;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.profesional.clubProfesional;
import com.inforad.fixall.profesional.panelcontrolProfesional;
import com.inforad.fixall.profesional.viewCitaPendiente;
import com.inforad.fixall.profesional.viewHistorial;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewListaHistorialPro;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.authProvider;
import com.inforad.fixall.provider.direccionProvider;

import pl.droidsonroids.gif.GifImageView;

public class viewConfigDirecciones extends AppCompatActivity {

    private direccionProvider mDireccion;
    private FirebaseAuth mAuth;
    private TextView txtDireccion;
    private LinearLayout btnAgregar;
    private String idUsuario;
    private DrawerLayout drawerLayout;
    private ImageView btnAtras;
    private DatabaseReference mDatabase;
    private direccionAdaptador mDireccionAdaptador;
    private RecyclerView recyclerListaDireccion;
    private procesador mProcesador;
    private authProvider mAuthProvider;
    private GifImageView btnAsistente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_config_direcciones);
        Iniciar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewConfigDirecciones.this);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(viewConfigDirecciones.this, agregarDireccion.class);
                startActivity(intent);
            }
        });

        btnAsistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(viewConfigDirecciones.this, chatActivity.class);
                intent.putExtra("idEnvia", idUsuario);
                intent.putExtra("idRecibir", "soporteTecnico");
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        txtDireccion = findViewById(R.id.tvDireccion);
        btnAgregar = findViewById(R.id.llAgregarDireccion);
        btnAsistente = findViewById(R.id.givAvatarVaron);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        recyclerListaDireccion = findViewById(R.id.rvDireccion);
        LinearLayoutManager linearLayoutManagerEstudios = new LinearLayoutManager(this);
        recyclerListaDireccion.setLayoutManager(linearLayoutManagerEstudios);

        mDireccion = new direccionProvider();
        mAuth = FirebaseAuth.getInstance();
        mProcesador = new procesador();
        mAuthProvider = new authProvider();

        idUsuario = mAuth.getCurrentUser().getUid();

        //CargarDatos();
        cargarMenu();
    }

    private void cargarMenu() {
        drawerLayout = findViewById(R.id.drawer_layout);
        btnAtras = findViewById(R.id.ivAtras);

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryDireccion = mDatabase.child("direccion_local").child(idUsuario);
        FirebaseRecyclerOptions<Direccion> options = new FirebaseRecyclerOptions.Builder<Direccion>().setQuery(queryDireccion, Direccion.class).build();
        mDireccionAdaptador = new direccionAdaptador(options, viewConfigDirecciones.this);
        recyclerListaDireccion.setAdapter(mDireccionAdaptador);
        mDireccionAdaptador.startListening();
    }
}