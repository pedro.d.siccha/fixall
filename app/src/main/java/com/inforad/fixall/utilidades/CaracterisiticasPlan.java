package com.inforad.fixall.utilidades;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.provider.userProvider;

public class CaracterisiticasPlan {

    private userProvider mUserProvider;
    private FirebaseAuth mAuth;
    private String idUsuario;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private DrawerLayout backGroundPrincipal;

    public CaracterisiticasPlan(View mView, Context pantalla) {

        mUserProvider = new userProvider();
        mAuth = FirebaseAuth.getInstance();
        backGroundPrincipal = mView.findViewById(R.id.dlPanelPro);
        idUsuario = mAuth.getCurrentUser().getUid();

        fondo(pantalla);

    }



    private void fondo(Context context) {
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    switch (snapshot.child("plan").getValue().toString()){
                        case "BASICO":
                            cargarFondoBasico(context);
                            break;
                        case "GOLD":
                            cargarFondoGold(context);
                            break;
                        case "BLACK":
                            cargarFondoBlack(context);
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void cargarFondoBasico(Context mContext) {
        bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.fondo);
        background = new BitmapDrawable(mContext.getResources(), bitmap);
        backGroundPrincipal.setBackground(background);
    }

    private void cargarFondoGold(Context mContext) {
        bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.fondogold);
        background = new BitmapDrawable(mContext.getResources(), bitmap);
        backGroundPrincipal.setBackground(background);
    }

    private void cargarFondoBlack(Context mContext) {
        bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.fondoblack);
        background = new BitmapDrawable(mContext.getResources(), bitmap);
        backGroundPrincipal.setBackground(background);
    }

}
