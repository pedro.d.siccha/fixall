package com.inforad.fixall.model;

public class Mensaje {
    private String id;
    private String idEnvia;
    private String idRecibe;
    private String idChat;
    private String mensaje;
    private String imagen;
    private long timestamp;
    private boolean visto;

    public Mensaje() {
    }

    public Mensaje(String id, String idEnvia, String idRecibe, String idChat, String mensaje, String imagen, long timestamp, boolean visto) {
        this.id = id;
        this.idEnvia = idEnvia;
        this.idRecibe = idRecibe;
        this.idChat = idChat;
        this.mensaje = mensaje;
        this.imagen = imagen;
        this.timestamp = timestamp;
        this.visto = visto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEnvia() {
        return idEnvia;
    }

    public void setIdEnvia(String idEnvia) {
        this.idEnvia = idEnvia;
    }

    public String getIdRecibe() {
        return idRecibe;
    }

    public void setIdRecibe(String idRecibe) {
        this.idRecibe = idRecibe;
    }

    public String getIdChat() {
        return idChat;
    }

    public void setIdChat(String idChat) {
        this.idChat = idChat;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isVisto() {
        return visto;
    }

    public void setVisto(boolean visto) {
        this.visto = visto;
    }
}
