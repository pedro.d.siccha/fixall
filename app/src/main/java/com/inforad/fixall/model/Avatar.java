package com.inforad.fixall.model;

public class Avatar {
    String id;
    String idUsuario;
    String urlAvatar;
    String estado;

    public Avatar() {
    }

    public Avatar(String id, String idUsuario, String urlAvatar, String estado) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.urlAvatar = urlAvatar;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
