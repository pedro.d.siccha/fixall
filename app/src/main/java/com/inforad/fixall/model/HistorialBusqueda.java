package com.inforad.fixall.model;

public class HistorialBusqueda {
    String idHistorial;
    String palabraClave;
    String descripcion;
    String imagenDescripcion;
    String rangoBusqueda;
    String estado;
    String idUsuario; //Esta en observacion
    String aliasUsuario;
    String imgUsuario;
    String idDirUsuario;
    String idProfesional;
    String aliasProfesional;
    String imgProfesional;
    String idDirProfesional;
    String valoracion;
    String horaCita;
    double costo;
    String calificacionEmpresario;
    String calificacionProfesional;
    long timestamp;

    public HistorialBusqueda() {
    }

    public HistorialBusqueda(String idHistorial, String palabraClave, String descripcion, String imagenDescripcion, String rangoBusqueda, String estado, String idUsuario, String aliasUsuario, String imgUsuario, String idDirUsuario, String idProfesional, String aliasProfesional, String imgProfesional, String idDirProfesional, String valoracion, String horaCita, double costo, String calificacionEmpresario, String calificacionProfesional, long timestamp) {
        this.idHistorial = idHistorial;
        this.palabraClave = palabraClave;
        this.descripcion = descripcion;
        this.imagenDescripcion = imagenDescripcion;
        this.rangoBusqueda = rangoBusqueda;
        this.estado = estado;
        this.idUsuario = idUsuario;
        this.aliasUsuario = aliasUsuario;
        this.imgUsuario = imgUsuario;
        this.idDirUsuario = idDirUsuario;
        this.idProfesional = idProfesional;
        this.aliasProfesional = aliasProfesional;
        this.imgProfesional = imgProfesional;
        this.idDirProfesional = idDirProfesional;
        this.valoracion = valoracion;
        this.horaCita = horaCita;
        this.costo = costo;
        this.calificacionEmpresario = calificacionEmpresario;
        this.calificacionProfesional = calificacionProfesional;
        this.timestamp = timestamp;
    }

    public String getIdHistorial() {
        return idHistorial;
    }

    public void setIdHistorial(String idHistorial) {
        this.idHistorial = idHistorial;
    }

    public String getPalabraClave() {
        return palabraClave;
    }

    public void setPalabraClave(String palabraClave) {
        this.palabraClave = palabraClave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagenDescripcion() {
        return imagenDescripcion;
    }

    public void setImagenDescripcion(String imagenDescripcion) {
        this.imagenDescripcion = imagenDescripcion;
    }

    public String getRangoBusqueda() {
        return rangoBusqueda;
    }

    public void setRangoBusqueda(String rangoBusqueda) {
        this.rangoBusqueda = rangoBusqueda;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getAliasUsuario() {
        return aliasUsuario;
    }

    public void setAliasUsuario(String aliasUsuario) {
        this.aliasUsuario = aliasUsuario;
    }

    public String getImgUsuario() {
        return imgUsuario;
    }

    public void setImgUsuario(String imgUsuario) {
        this.imgUsuario = imgUsuario;
    }

    public String getIdDirUsuario() {
        return idDirUsuario;
    }

    public void setIdDirUsuario(String idDirUsuario) {
        this.idDirUsuario = idDirUsuario;
    }

    public String getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(String idProfesional) {
        this.idProfesional = idProfesional;
    }

    public String getAliasProfesional() {
        return aliasProfesional;
    }

    public void setAliasProfesional(String aliasProfesional) {
        this.aliasProfesional = aliasProfesional;
    }

    public String getImgProfesional() {
        return imgProfesional;
    }

    public void setImgProfesional(String imgProfesional) {
        this.imgProfesional = imgProfesional;
    }

    public String getIdDirProfesional() {
        return idDirProfesional;
    }

    public void setIdDirProfesional(String idDirProfesional) {
        this.idDirProfesional = idDirProfesional;
    }

    public String getValoracion() {
        return valoracion;
    }

    public void setValoracion(String valoracion) {
        this.valoracion = valoracion;
    }

    public String getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public String getCalificacionEmpresario() {
        return calificacionEmpresario;
    }

    public void setCalificacionEmpresario(String calificacionEmpresario) {
        this.calificacionEmpresario = calificacionEmpresario;
    }

    public String getCalificacionProfesional() {
        return calificacionProfesional;
    }

    public void setCalificacionProfesional(String calificacionProfesional) {
        this.calificacionProfesional = calificacionProfesional;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
