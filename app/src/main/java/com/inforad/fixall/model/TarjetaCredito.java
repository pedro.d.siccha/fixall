package com.inforad.fixall.model;

public class TarjetaCredito {
    String id;
    String banco;
    String numTarjeta;
    String fecVencimiento;
    String estado;

    public TarjetaCredito() {
    }

    public TarjetaCredito(String id, String banco, String numTarjeta, String fecVencimiento, String estado) {
        this.id = id;
        this.banco = banco;
        this.numTarjeta = numTarjeta;
        this.fecVencimiento = fecVencimiento;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getNumTarjeta() {
        return numTarjeta;
    }

    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public String getFecVencimiento() {
        return fecVencimiento;
    }

    public void setFecVencimiento(String fecVencimiento) {
        this.fecVencimiento = fecVencimiento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
