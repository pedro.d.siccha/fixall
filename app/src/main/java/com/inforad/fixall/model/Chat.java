package com.inforad.fixall.model;

import java.util.ArrayList;

public class Chat {

    private String id;
    private String idUsuario1;
    private String idUsuario2;
    private int idNotificacion;
    private boolean escribiendo;
    private long timestamp;

    public Chat() {
    }

    public Chat(String id, String idUsuario1, String idUsuario2, int idNotificacion, boolean escribiendo, long timestamp) {
        this.id = id;
        this.idUsuario1 = idUsuario1;
        this.idUsuario2 = idUsuario2;
        this.idNotificacion = idNotificacion;
        this.escribiendo = escribiendo;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUsuario1() {
        return idUsuario1;
    }

    public void setIdUsuario1(String idUsuario1) {
        this.idUsuario1 = idUsuario1;
    }

    public String getIdUsuario2() {
        return idUsuario2;
    }

    public void setIdUsuario2(String idUsuario2) {
        this.idUsuario2 = idUsuario2;
    }

    public int getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(int idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public boolean isEscribiendo() {
        return escribiendo;
    }

    public void setEscribiendo(boolean escribiendo) {
        this.escribiendo = escribiendo;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
