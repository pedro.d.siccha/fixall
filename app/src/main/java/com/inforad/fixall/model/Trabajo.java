package com.inforad.fixall.model;

public class Trabajo {

    String id;
    String cargo;
    String tipoEmpleo;
    String nomEmpresa;
    String direccion;
    String fecInicio;
    String fecFin;
    String descripcion;
    String imagen;

    public Trabajo() {
    }

    public Trabajo(String id, String cargo, String tipoEmpleo, String nomEmpresa, String direccion, String fecInicio, String fecFin, String descripcion, String imagen) {
        this.id = id;
        this.cargo = cargo;
        this.tipoEmpleo = tipoEmpleo;
        this.nomEmpresa = nomEmpresa;
        this.direccion = direccion;
        this.fecInicio = fecInicio;
        this.fecFin = fecFin;
        this.descripcion = descripcion;
        this.imagen = imagen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getTipoEmpleo() {
        return tipoEmpleo;
    }

    public void setTipoEmpleo(String tipoEmpleo) {
        this.tipoEmpleo = tipoEmpleo;
    }

    public String getNomEmpresa() {
        return nomEmpresa;
    }

    public void setNomEmpresa(String nomEmpresa) {
        this.nomEmpresa = nomEmpresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(String fecInicio) {
        this.fecInicio = fecInicio;
    }

    public String getFecFin() {
        return fecFin;
    }

    public void setFecFin(String fecFin) {
        this.fecFin = fecFin;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
