package com.inforad.fixall.model;

public class Invitacion {
    String idUsuario;
    String imgUsuario;
    String aliasUsuario;
    String idBusqueda;
    String descripcion;
    String imgDescripcion;
    String palabraClave;
    String idProfesional;
    String aliasProfesional;
    String imgProfesional;
    String estado;

    public Invitacion() {
    }

    public Invitacion(String idUsuario, String imgUsuario, String aliasUsuario, String idBusqueda, String descripcion, String imgDescripcion, String palabraClave, String idProfesional, String aliasProfesional, String imgProfesional, String estado) {
        this.idUsuario = idUsuario;
        this.imgUsuario = imgUsuario;
        this.aliasUsuario = aliasUsuario;
        this.idBusqueda = idBusqueda;
        this.descripcion = descripcion;
        this.imgDescripcion = imgDescripcion;
        this.palabraClave = palabraClave;
        this.idProfesional = idProfesional;
        this.aliasProfesional = aliasProfesional;
        this.imgProfesional = imgProfesional;
        this.estado = estado;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getImgUsuario() {
        return imgUsuario;
    }

    public void setImgUsuario(String imgUsuario) {
        this.imgUsuario = imgUsuario;
    }

    public String getAliasUsuario() {
        return aliasUsuario;
    }

    public void setAliasUsuario(String aliasUsuario) {
        this.aliasUsuario = aliasUsuario;
    }

    public String getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(String idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImgDescripcion() {
        return imgDescripcion;
    }

    public void setImgDescripcion(String imgDescripcion) {
        this.imgDescripcion = imgDescripcion;
    }

    public String getPalabraClave() {
        return palabraClave;
    }

    public void setPalabraClave(String palabraClave) {
        this.palabraClave = palabraClave;
    }

    public String getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(String idProfesional) {
        this.idProfesional = idProfesional;
    }

    public String getAliasProfesional() {
        return aliasProfesional;
    }

    public void setAliasProfesional(String aliasProfesional) {
        this.aliasProfesional = aliasProfesional;
    }

    public String getImgProfesional() {
        return imgProfesional;
    }

    public void setImgProfesional(String imgProfesional) {
        this.imgProfesional = imgProfesional;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
