package com.inforad.fixall.model;

public class PalabraClave_Busqueda {
    String idUsuario;
    String palabraClave;
    String estado;

    public PalabraClave_Busqueda() {
    }

    public PalabraClave_Busqueda(String idUsuario, String palabraClave, String estado) {
        this.idUsuario = idUsuario;
        this.palabraClave = palabraClave;
        this.estado = estado;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getPalabraClave() {
        return palabraClave;
    }

    public void setPalabraClave(String palabraClave) {
        this.palabraClave = palabraClave;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
