package com.inforad.fixall.model;

public class ProfesionProfesional {
    String id;
    String nombreUniversidad;
    String grado;
    String FecInicio;
    String FecFin;
    String descripcion;

    public ProfesionProfesional() {
    }

    public ProfesionProfesional(String id, String nombreUniversidad, String grado, String fecInicio, String fecFin, String descripcion) {
        this.id = id;
        this.nombreUniversidad = nombreUniversidad;
        this.grado = grado;
        this.FecInicio = fecInicio;
        this.FecFin = fecFin;
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreUniversidad() {
        return nombreUniversidad;
    }

    public void setNombreUniversidad(String nombreUniversidad) {
        this.nombreUniversidad = nombreUniversidad;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    public String getFecInicio() {
        return FecInicio;
    }

    public void setFecInicio(String fecInicio) {
        FecInicio = fecInicio;
    }

    public String getFecFin() {
        return FecFin;
    }

    public void setFecFin(String fecFin) {
        FecFin = fecFin;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
