package com.inforad.fixall.model;

public class Descripcion {
    String id;
    String idUsuario;
    String descripcion;

    public Descripcion() {
    }

    public Descripcion(String id, String idUsuario, String descripcion) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
