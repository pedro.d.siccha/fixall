package com.inforad.fixall.model;

public class PalabraClave {
    String idProfesional;
    String imgProfesional;
    String nomProfesional;
    String apeProfesional;
    String aliasProfesional;
    String idDireccion;
    String palabra;
    String estado;

    public PalabraClave() {
    }

    public PalabraClave(String idProfesional, String imgProfesional, String nomProfesional, String apeProfesional, String aliasProfesional, String idDireccion, String palabra, String estado) {
        this.idProfesional = idProfesional;
        this.imgProfesional = imgProfesional;
        this.nomProfesional = nomProfesional;
        this.apeProfesional = apeProfesional;
        this.aliasProfesional = aliasProfesional;
        this.idDireccion = idDireccion;
        this.palabra = palabra;
        this.estado = estado;
    }

    public String getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(String idProfesional) {
        this.idProfesional = idProfesional;
    }

    public String getImgProfesional() {
        return imgProfesional;
    }

    public void setImgProfesional(String imgProfesional) {
        this.imgProfesional = imgProfesional;
    }

    public String getNomProfesional() {
        return nomProfesional;
    }

    public void setNomProfesional(String nomProfesional) {
        this.nomProfesional = nomProfesional;
    }

    public String getApeProfesional() {
        return apeProfesional;
    }

    public void setApeProfesional(String apeProfesional) {
        this.apeProfesional = apeProfesional;
    }

    public String getAliasProfesional() {
        return aliasProfesional;
    }

    public void setAliasProfesional(String aliasProfesional) {
        this.aliasProfesional = aliasProfesional;
    }

    public String getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(String idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
