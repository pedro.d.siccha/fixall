package com.inforad.fixall.model;

public class Flyer {
    String id;
    String idUsuario;
    String informacion;
    String img;
    String estado;
    long timestamp;

    public Flyer() {
    }

    public Flyer(String id, String idUsuario, String informacion, String img, String estado, long timestamp) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.informacion = informacion;
        this.img = img;
        this.estado = estado;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getInformacion() {
        return informacion;
    }

    public void setInformacion(String informacion) {
        this.informacion = informacion;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
