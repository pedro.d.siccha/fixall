package com.inforad.fixall.model;

public class UsuarioPalabra {
    String id;
    String estado;

    public UsuarioPalabra() {
    }

    public UsuarioPalabra(String id, String estado) {
        this.id = id;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
