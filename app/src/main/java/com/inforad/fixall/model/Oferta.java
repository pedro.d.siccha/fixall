package com.inforad.fixall.model;

public class Oferta {
    String id;
    String palabraClave;
    String idProfesional;
    String alias;
    String imgProfesional;
    String idBusqueda;
    String descripcion;
    String estado;
    String distancia;
    double costo;
    long timestamp;

    public Oferta() {
    }

    public Oferta(String id, String palabraClave, String idProfesional, String alias, String imgProfesional, String idBusqueda, String descripcion, String estado, String distancia, double costo, long timestamp) {
        this.id = id;
        this.palabraClave = palabraClave;
        this.idProfesional = idProfesional;
        this.alias = alias;
        this.imgProfesional = imgProfesional;
        this.idBusqueda = idBusqueda;
        this.descripcion = descripcion;
        this.estado = estado;
        this.distancia = distancia;
        this.costo = costo;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPalabraClave() {
        return palabraClave;
    }

    public void setPalabraClave(String palabraClave) {
        this.palabraClave = palabraClave;
    }

    public String getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(String idProfesional) {
        this.idProfesional = idProfesional;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getImgProfesional() {
        return imgProfesional;
    }

    public void setImgProfesional(String imgProfesional) {
        this.imgProfesional = imgProfesional;
    }

    public String getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(String idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
