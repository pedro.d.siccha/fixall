package com.inforad.fixall.model;

public class Buscar {

    String id;
    String palabraClave;
    String descripcion;
    String imagenDescripcion;
    String rangoBusqueda;
    String estado;
    String idUsuario; //Esta en observacion
    String aliasUsuario;
    String imgUsuario;
    String idDirUsuario;
    String idProfesional;
    String aliasProfesional;
    String imgProfesional;
    String idDirProfesional;
    String valoracion;
    String horaCita;
    String fechaCita;
    String idHistorialBusqueda;
    double costo;
    double latOrigen;
    double longOrigen;
    long timestamp;

    public Buscar() {
    }

    public Buscar(String id, String palabraClave, String descripcion, String imagenDescripcion, String rangoBusqueda, String estado, String idUsuario, String aliasUsuario, String imgUsuario, String idDirUsuario, String idProfesional, String aliasProfesional, String imgProfesional, String idDirProfesional, String valoracion, String horaCita, String fechaCita, String idHistorialBusqueda, double costo, double latOrigen, double longOrigen, long timestamp) {
        this.id = id;
        this.palabraClave = palabraClave;
        this.descripcion = descripcion;
        this.imagenDescripcion = imagenDescripcion;
        this.rangoBusqueda = rangoBusqueda;
        this.estado = estado;
        this.idUsuario = idUsuario;
        this.aliasUsuario = aliasUsuario;
        this.imgUsuario = imgUsuario;
        this.idDirUsuario = idDirUsuario;
        this.idProfesional = idProfesional;
        this.aliasProfesional = aliasProfesional;
        this.imgProfesional = imgProfesional;
        this.idDirProfesional = idDirProfesional;
        this.valoracion = valoracion;
        this.horaCita = horaCita;
        this.fechaCita = fechaCita;
        this.idHistorialBusqueda = idHistorialBusqueda;
        this.costo = costo;
        this.latOrigen = latOrigen;
        this.longOrigen = longOrigen;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPalabraClave() {
        return palabraClave;
    }

    public void setPalabraClave(String palabraClave) {
        this.palabraClave = palabraClave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagenDescripcion() {
        return imagenDescripcion;
    }

    public void setImagenDescripcion(String imagenDescripcion) {
        this.imagenDescripcion = imagenDescripcion;
    }

    public String getRangoBusqueda() {
        return rangoBusqueda;
    }

    public void setRangoBusqueda(String rangoBusqueda) {
        this.rangoBusqueda = rangoBusqueda;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getAliasUsuario() {
        return aliasUsuario;
    }

    public void setAliasUsuario(String aliasUsuario) {
        this.aliasUsuario = aliasUsuario;
    }

    public String getImgUsuario() {
        return imgUsuario;
    }

    public void setImgUsuario(String imgUsuario) {
        this.imgUsuario = imgUsuario;
    }

    public String getIdDirUsuario() {
        return idDirUsuario;
    }

    public void setIdDirUsuario(String idDirUsuario) {
        this.idDirUsuario = idDirUsuario;
    }

    public String getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(String idProfesional) {
        this.idProfesional = idProfesional;
    }

    public String getAliasProfesional() {
        return aliasProfesional;
    }

    public void setAliasProfesional(String aliasProfesional) {
        this.aliasProfesional = aliasProfesional;
    }

    public String getImgProfesional() {
        return imgProfesional;
    }

    public void setImgProfesional(String imgProfesional) {
        this.imgProfesional = imgProfesional;
    }

    public String getIdDirProfesional() {
        return idDirProfesional;
    }

    public void setIdDirProfesional(String idDirProfesional) {
        this.idDirProfesional = idDirProfesional;
    }

    public String getValoracion() {
        return valoracion;
    }

    public void setValoracion(String valoracion) {
        this.valoracion = valoracion;
    }

    public String getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getIdHistorialBusqueda() {
        return idHistorialBusqueda;
    }

    public void setIdHistorialBusqueda(String idHistorialBusqueda) {
        this.idHistorialBusqueda = idHistorialBusqueda;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public double getLatOrigen() {
        return latOrigen;
    }

    public void setLatOrigen(double latOrigen) {
        this.latOrigen = latOrigen;
    }

    public double getLongOrigen() {
        return longOrigen;
    }

    public void setLongOrigen(double longOrigen) {
        this.longOrigen = longOrigen;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
