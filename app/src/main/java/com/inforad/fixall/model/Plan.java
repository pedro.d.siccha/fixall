package com.inforad.fixall.model;

public class Plan {
    String idUsuario;
    String plan;
    String fecInicio;
    String fecFin;
    double pago;
    int cantBono;
    double bono;
    int club;
    int nivel;

    public Plan() {
    }

    public Plan(String idUsuario, String plan, String fecInicio, String fecFin, double pago, int cantBono, double bono, int club, int nivel) {
        this.idUsuario = idUsuario;
        this.plan = plan;
        this.fecInicio = fecInicio;
        this.fecFin = fecFin;
        this.pago = pago;
        this.cantBono = cantBono;
        this.bono = bono;
        this.club = club;
        this.nivel = nivel;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(String fecInicio) {
        this.fecInicio = fecInicio;
    }

    public String getFecFin() {
        return fecFin;
    }

    public void setFecFin(String fecFin) {
        this.fecFin = fecFin;
    }

    public double getPago() {
        return pago;
    }

    public void setPago(double pago) {
        this.pago = pago;
    }

    public int getCantBono() {
        return cantBono;
    }

    public void setCantBono(int cantBono) {
        this.cantBono = cantBono;
    }

    public double getBono() {
        return bono;
    }

    public void setBono(double bono) {
        this.bono = bono;
    }

    public int getClub() {
        return club;
    }

    public void setClub(int club) {
        this.club = club;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
}
