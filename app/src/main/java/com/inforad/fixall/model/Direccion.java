package com.inforad.fixall.model;

public class Direccion {

    String id;
    String ciudad;
    String region;
    String pais;
    String nombre;
    String estado;
    String nivel;
    double dirLat;
    double dirLong;

    public Direccion() {
    }

    public Direccion(String id, String ciudad, String region, String pais, String nombre, String estado, String nivel, double dirLat, double dirLong) {
        this.id = id;
        this.ciudad = ciudad;
        this.region = region;
        this.pais = pais;
        this.nombre = nombre;
        this.estado = estado;
        this.nivel = nivel;
        this.dirLat = dirLat;
        this.dirLong = dirLong;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public double getDirLat() {
        return dirLat;
    }

    public void setDirLat(double dirLat) {
        this.dirLat = dirLat;
    }

    public double getDirLong() {
        return dirLong;
    }

    public void setDirLong(double dirLong) {
        this.dirLong = dirLong;
    }

    @Override
    public String toString() {
        return ciudad + " - " + nombre;
    }
}
