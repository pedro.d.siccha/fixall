package com.inforad.fixall.model;

public class Contacto {
    String idPricipal;
    String nombre;
    String foto;
    String telefono;
    String idContacto;

    public Contacto() {
    }

    public Contacto(String idPricipal, String nombre, String foto, String telefono, String idContacto) {
        this.idPricipal = idPricipal;
        this.nombre = nombre;
        this.foto = foto;
        this.telefono = telefono;
        this.idContacto = idContacto;
    }

    public String getIdPricipal() {
        return idPricipal;
    }

    public void setIdPricipal(String idPricipal) {
        this.idPricipal = idPricipal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(String idContacto) {
        this.idContacto = idContacto;
    }
}
