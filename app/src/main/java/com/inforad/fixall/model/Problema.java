package com.inforad.fixall.model;

public class Problema {
    String id;
    String idBusqueda;
    String idProfesional;
    String idEmpresario;
    String problema;
    String descProblema;

    public Problema() {
    }

    public Problema(String id, String idBusqueda, String idProfesional, String idEmpresario, String problema, String descProblema) {
        this.id = id;
        this.idBusqueda = idBusqueda;
        this.idProfesional = idProfesional;
        this.idEmpresario = idEmpresario;
        this.problema = problema;
        this.descProblema = descProblema;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(String idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    public String getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(String idProfesional) {
        this.idProfesional = idProfesional;
    }

    public String getIdEmpresario() {
        return idEmpresario;
    }

    public void setIdEmpresario(String idEmpresario) {
        this.idEmpresario = idEmpresario;
    }

    public String getProblema() {
        return problema;
    }

    public void setProblema(String problema) {
        this.problema = problema;
    }

    public String getDescProblema() {
        return descProblema;
    }

    public void setDescProblema(String descProblema) {
        this.descProblema = descProblema;
    }
}
