package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.model.Direccion;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.GeofireProvider;
import com.inforad.fixall.provider.direccionProvider;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.tokenProvider;
import com.inforad.fixall.provider.userProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class agregarDireccion extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFuseLocation;
    private final static int LOCATION_REQUEST_CODE = 1;
    private final static int SETTINGS_REQUEST_CODE = 2;
    private LatLng mCurrentLatLng;
    private GeofireProvider mGeofireProvide;
    private procesador mProcesador;
    private Button btnSiguiente;
    private Marker mMarker;
    private FirebaseAuth mAuth;
    private AutocompleteSupportFragment mAutocomplete;
    private PlacesClient mPlaces;
    private String mPosicion, ciudad, pais, direccion, idUsuario, codigo, departamento;
    private LatLng mPosicionLatLng;
    private GoogleMap.OnCameraIdleListener mCameraListener;
    private Spinner cbUbicacion;
    private DatabaseReference mDatabase;
    private ProgressDialog mProgressDialog;
    private direccionProvider mDireccion;
    private tokenProvider mToken;
    private notificacionProvider mNotificacionProvider;
    private userProvider mUserProvider;

    LocationCallback mLocationCallbac = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                if (getApplicationContext() != null) {

                    mCurrentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.moveCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(15f).build()
                    ));

                    updateLocation();

                }
            }
        }
    };

    private void updateLocation() {
        if (mProcesador.existeSesion() && mCurrentLatLng != null) {
            mGeofireProvide.guadarDireccion(mProcesador.obtenerId(), mCurrentLatLng);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_agregar_direccion);

        mDireccion = new direccionProvider();
        cbUbicacion = findViewById(R.id.spUnicacion);
        String[] ubicacion = {"CASA", "TRABAJO", "OFICINA"};
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(ubicacion));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.style_spinner, arrayList);
        cbUbicacion.setAdapter(arrayAdapter);
        mProcesador = new procesador();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mToken = new tokenProvider();
        mNotificacionProvider = new notificacionProvider();
        mUserProvider = new userProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        btnSiguiente = findViewById(R.id.btnSiguienteMapa);

        mGeofireProvide = new GeofireProvider();
        mProcesador = new procesador();
        mProgressDialog = new ProgressDialog(this);
        mFuseLocation = LocationServices.getFusedLocationProviderClient(this);

        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapaEmpresario);
        mMapFragment.getMapAsync(this);
        if (!Places.isInitialized()){
            Places.initialize(getApplicationContext(), getResources().getString(R.string.google_maps_key));
        }

        mPlaces = Places.createClient(this);
        mAutocomplete = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.buscadorDireccion);
        mAutocomplete.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME));
        mAutocomplete.setHint("Buscar Dirección");
        mAutocomplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                mPosicion = place.getName();
                mPosicionLatLng = place.getLatLng();
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(
                        new CameraPosition.Builder().target(new LatLng(mPosicionLatLng.latitude, mPosicionLatLng.longitude)).zoom(15f).build()
                ));

            }

            @Override
            public void onError(@NonNull Status status) {

            }
        });

        mCameraListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                try {
                    Geocoder geocoder = new Geocoder(agregarDireccion.this);
                    mPosicionLatLng = mMap.getCameraPosition().target;
                    List<Address> addressList = geocoder.getFromLocation(mPosicionLatLng.latitude, mPosicionLatLng.longitude, 1);
                    departamento = addressList.get(0).getAdminArea();
                    ciudad = addressList.get(0).getSubAdminArea();
                    pais = addressList.get(0).getCountryName();
                    direccion = addressList.get(0).getThoroughfare() + " N° " + addressList.get(0).getSubThoroughfare();
                    mPosicion = direccion + " " + pais + " " + ciudad;

                    mAutocomplete.setText(direccion);
                }catch (Exception e){
                    //Toast.makeText(viewDireccion.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        };

        mAuth = FirebaseAuth.getInstance();

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mProgressDialog.setMessage("Agregando nueva dirección...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();

                mDireccion.obtenerNomDireccion(idUsuario, cbUbicacion.getSelectedItem().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            Toast.makeText(agregarDireccion.this, "Dirección de " + cbUbicacion.getSelectedItem().toString() + " ya registrada", Toast.LENGTH_SHORT).show();
                            mProgressDialog.dismiss();
                            finish();
                        }else {
                            String idDireccion = mDatabase.push().getKey();
                            Direccion dir = new Direccion(idDireccion, direccion, ciudad, pais, cbUbicacion.getSelectedItem().toString(), "ACTIVO", "1", mPosicionLatLng.latitude, mPosicionLatLng.longitude);
                            mDireccion.guardarDireccion(dir, idUsuario, idDireccion).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    mProgressDialog.dismiss();
                                    Intent intent = new Intent(agregarDireccion.this, viewConfigDirecciones.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnCameraIdleListener(mCameraListener);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(5);
        startLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == LOCATION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    if (gpsActicado()){
                        mFuseLocation.requestLocationUpdates(mLocationRequest, mLocationCallbac, Looper.myLooper());
                    }else {
                        verAlertDiagloNoGps();
                    }
                } else {
                    checkLocationPermissions();
                }
            } else {
                checkLocationPermissions();
            }
        }

    }

    private void checkLocationPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this).setTitle("Por favor, proporcine los permisos para continuar").setMessage("Esta aplicación requiere de los permisos de ubicación para pdoer utilizarece").setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(agregarDireccion.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
                    }
                }).create().show();
            } else {
                ActivityCompat.requestPermissions(agregarDireccion.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SETTINGS_REQUEST_CODE && gpsActicado()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mFuseLocation.requestLocationUpdates(mLocationRequest, mLocationCallbac, Looper.myLooper());
        }else {

        }
    }

    private void verAlertDiagloNoGps(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Por favor activa tu ubicacion para continuar").setPositiveButton("Configuraciones", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), SETTINGS_REQUEST_CODE);
            }
        }).create().show();
    }

    private boolean gpsActicado(){
        boolean activo = false;
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            activo = true;
        }
        return activo;
    }

    private void startLocation(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                if (gpsActicado()){
                    mFuseLocation.requestLocationUpdates(mLocationRequest, mLocationCallbac, Looper.myLooper());
                }else {
                    verAlertDiagloNoGps();
                }
            }else {
                checkLocationPermissions();
            }
        }else {
            if (gpsActicado()){
                mFuseLocation.requestLocationUpdates(mLocationRequest, mLocationCallbac, Looper.myLooper());
            }else {
                verAlertDiagloNoGps();
            }
        }
    }

}