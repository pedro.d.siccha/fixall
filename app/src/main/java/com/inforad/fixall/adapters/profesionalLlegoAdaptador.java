package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.viewLLegoProfesionalE;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.provider.userProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class profesionalLlegoAdaptador extends FirebaseRecyclerAdapter<Oferta, profesionalLlegoAdaptador.ViewHolder> {
    private Context mContext;

    public profesionalLlegoAdaptador(@NonNull FirebaseRecyclerOptions<Oferta> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Oferta ofertaModel) {

        holder.mUserProvider.obtenerUsuario(ofertaModel.getIdProfesional()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String imagen = snapshot.child("imagen").getValue().toString();
                    holder.txtNombrePro.setText(snapshot.child("alias").getValue().toString());
                    holder.txtTelefono.setText(snapshot.child("telefono").getValue().toString());
                    Picasso.with(mContext).load(imagen).into(holder.imgPerfilPro);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, viewLLegoProfesionalE.class);
                intent.putExtra("idProfesional", ofertaModel.getIdProfesional());
                intent.putExtra("idBusqueda", ofertaModel.getIdBusqueda());
                intent.putExtra("palabraClave", ofertaModel.getPalabraClave());
                intent.putExtra("costo", ofertaModel.getCosto());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listaprofllego, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfilPro;
        private TextView txtNombrePro, txtTelefono;
        private userProvider mUserProvider;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfilPro = view.findViewById(R.id.ivImgPerfil);
            txtNombrePro = view.findViewById(R.id.tvNombrePro);
            txtTelefono = view.findViewById(R.id.tvTelefonoPro);

            mUserProvider = new userProvider();
        }

    }
}
