package com.inforad.fixall.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.model.User;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class usuarioAdapater extends FirebaseRecyclerAdapter<User, usuarioAdapater.ViewHolder> {
    private Context mContext;

    public usuarioAdapater(@NonNull FirebaseRecyclerOptions<User> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull User userModel) {
        String img = userModel.getImagen();
        Picasso.with(mContext).load(img).into(holder.imgPerfil);
        holder.txtAlias.setText(userModel.getAlias());
        holder.txtIso.setText(userModel.getDni());
        holder.txtDistancia.setText("Distancia: 2m");
        holder.txtOcupacion.setText("Edad: " + userModel.getEdad() + " años");
        holder.btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        holder.btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_usuario, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private TextView txtAlias, txtOcupacion, txtDistancia, txtIso;
        private Button btnPerfil, btnChat;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.civPerfil);
            txtAlias = view.findViewById(R.id.tvAlias);
            txtOcupacion = view.findViewById(R.id.tvOcupacion);
            txtDistancia = view.findViewById(R.id.tvDistancia);
            txtIso = view.findViewById(R.id.tvIso);
            btnPerfil = view.findViewById(R.id.bPerfil);
            btnChat = view.findViewById(R.id.bChat);
        }
    }

}
