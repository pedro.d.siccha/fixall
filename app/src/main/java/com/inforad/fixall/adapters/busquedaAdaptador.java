package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.sinOfertas;
import com.inforad.fixall.empresario.viewConfirmacionE;
import com.inforad.fixall.empresario.viewOfertasRecibidas;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.Invitacion;
import com.inforad.fixall.profesional.viewRecibirOferta;
import com.squareup.picasso.Picasso;

public class busquedaAdaptador extends FirebaseRecyclerAdapter<Busqueda, busquedaAdaptador.ViewHolder> {
    private Context mContext;

    public busquedaAdaptador(@NonNull FirebaseRecyclerOptions<Busqueda> options, Context context) {
        super(options);
        mContext = context;


    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Busqueda InvitacionModel) {

        String estado = InvitacionModel.getEstado();
        String img = InvitacionModel.getImgUsuario();
        Picasso.with(mContext).load(img).into(holder.imgPerfil);
        holder.txtCliente.setText(InvitacionModel.getAliasUsuario());
        holder.txtEstado.setText(InvitacionModel.getDescripcion());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (estado.equals("PENDIENTE")){

                    Intent intent = new Intent(mContext, viewRecibirOferta.class);
                    intent.putExtra("idBusqueda", InvitacionModel.getId());
                    intent.putExtra("palabraClave", InvitacionModel.getPalabraClave());
                    mContext.startActivity(intent);
                }else {
                    Intent intent = new Intent(mContext, sinOfertas.class);
                    mContext.startActivity(intent);
                }



            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_citapendiente, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgPerfil;
        private TextView txtCliente, txtEstado;
        private FirebaseAuth mAuth;
        private View mView;


        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.ivPerfilCliente);
            txtCliente = view.findViewById(R.id.tvNomCliente);
            txtEstado = view.findViewById(R.id.tvEstado);
            mAuth = FirebaseAuth.getInstance();
        }
    }
}
