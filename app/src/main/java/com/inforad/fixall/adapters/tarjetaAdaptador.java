package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.model.TarjetaCredito;
import com.inforad.fixall.provider.userProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class tarjetaAdaptador extends FirebaseRecyclerAdapter<TarjetaCredito, tarjetaAdaptador.ViewHolder> {
    private Context mContext;

    public tarjetaAdaptador(@NonNull FirebaseRecyclerOptions<TarjetaCredito> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull TarjetaCredito tarjetaModel) {

        holder.txtBanco.setText(tarjetaModel.getNumTarjeta());
        holder.txtFecha.setText(tarjetaModel.getFecVencimiento());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listtarjeta, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtBanco, txtFecha;
        private FirebaseAuth mAuth;
        private String idUsuario;
        private userProvider mUserProvider;
        private View mView;


        public ViewHolder(View view){
            super(view);
            mView = view;

            txtBanco = view.findViewById(R.id.tvBanco);
            txtFecha = view.findViewById(R.id.tvFecha);

            mUserProvider = new userProvider();
            mAuth = FirebaseAuth.getInstance();
            idUsuario = mAuth.getCurrentUser().getUid();
        }
    }
}
