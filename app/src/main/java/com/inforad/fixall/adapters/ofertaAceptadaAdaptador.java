package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.profesional.viewOfertaAceptada;
import com.inforad.fixall.provider.busquedaProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ofertaAceptadaAdaptador extends FirebaseRecyclerAdapter<Oferta, ofertaAceptadaAdaptador.ViewHolder> {
    private Context mContext;

    public ofertaAceptadaAdaptador(@NonNull FirebaseRecyclerOptions<Oferta> options, Context context) {
        super(options);
        mContext = context;

    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Oferta ofertaModel) {

        holder.mBusquedaProvider.obtenerBusqueda(ofertaModel.getPalabraClave(), ofertaModel.getIdBusqueda()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imgUsuario").getValue().toString();
                    Picasso.with(mContext).load(img).into(holder.imgPerfil);
                    holder.txtCliente.setText(snapshot.child("aliasUsuario").getValue().toString());
                    holder.txtDescripcion.setText(snapshot.child("descripcion").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, viewOfertaAceptada.class);
                intent.putExtra("idOferta", ofertaModel.getId());
                intent.putExtra("idBusqueda", ofertaModel.getIdBusqueda());
                intent.putExtra("palabraClave", ofertaModel.getPalabraClave());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listaofertaaceptada, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private TextView txtCliente, txtDescripcion;
        private View mView;
        private DatabaseReference mDatabase;
        private busquedaProvider mBusquedaProvider;

        public ViewHolder(View view){
            super(view);
            mView = view;

            imgPerfil = view.findViewById(R.id.ivPerfilEmp);
            txtCliente = view.findViewById(R.id.tvNomEmpresario);
            txtDescripcion = view.findViewById(R.id.tvDescripcion);

            mDatabase = FirebaseDatabase.getInstance().getReference();
            mBusquedaProvider = new busquedaProvider();

        }
    }
}
