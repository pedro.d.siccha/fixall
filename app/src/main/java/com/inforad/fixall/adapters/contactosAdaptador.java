package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.chatActivity;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.model.Chat;
import com.inforad.fixall.model.Contacto;
import com.inforad.fixall.provider.chatProvider;
import com.inforad.fixall.provider.mensajeProvider;
import com.inforad.fixall.provider.userProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class contactosAdaptador extends FirebaseRecyclerAdapter<Chat, contactosAdaptador.ViewHolder> {
    private Context mContext;

    public contactosAdaptador(@NonNull FirebaseRecyclerOptions<Chat> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Chat chatModel) {

        obtenerUsuario(chatModel.getIdUsuario2(), holder);
        //obtenerUltimoMsj(chatModel.getId(), holder);
        cantChatNoLeido(chatModel.getId(), chatModel.getIdUsuario1(), holder);
        holder.vistaContadorMensajes.setVisibility(View.GONE);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, chatActivity.class);
                intent.putExtra("idEnvia", chatModel.getIdUsuario1());
                intent.putExtra("idRecibir", chatModel.getIdUsuario2());
                mContext.startActivity(intent);
            }
        });

    }

    private void cantChatNoLeido(String id, String idUsuario1, ViewHolder holder) {
        holder.mMsjProvider.obtenerMensajeEnvio(id, idUsuario1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void obtenerUltimoMsj(String id, final ViewHolder holder) {
        holder.mMsjProvider.obtenerUltimoMensaje(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    holder.txtMsjNoLeido.setText(snapshot.child("mensaje").getValue().toString());
                }else {
                    holder.txtMsjNoLeido.setText("");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void obtenerUsuario(String idUsuario, final ViewHolder holder) {
        holder.mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String alias = snapshot.child("alias").getValue().toString();
                    String imagen = snapshot.child("imagen").getValue().toString();
                    holder.txtNombre.setText(alias);
                    Picasso.with(mContext).load(imagen).into(holder.imgPerfil);
                    String plan = snapshot.child("plan").getValue().toString();

                    if (plan.equals("BASICO")){
                        holder.txtNombre.setTextColor(Color.WHITE);
                        holder.txtUltimoMensaje.setTextColor(Color.WHITE);
                    }else if (plan.equals("GOLD")){
                        holder.txtNombre.setTextColor(Color.BLACK);
                        holder.txtUltimoMensaje.setTextColor(Color.BLACK);
                    }else if (plan.equals("BLACK")){
                        holder.txtNombre.setTextColor(Color.WHITE);
                        holder.txtUltimoMensaje.setTextColor(Color.WHITE);
                    }else{
                        holder.txtNombre.setTextColor(Color.WHITE);
                        holder.txtUltimoMensaje.setTextColor(Color.WHITE);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_usuario, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private TextView txtNombre, txtUltimoMensaje, txtMsjNoLeido;
        private FirebaseAuth mAuth;
        private String idUsuario;
        private userProvider mUserProvider;
        private chatProvider mChatProvider;
        private mensajeProvider mMsjProvider;
        private FrameLayout vistaContadorMensajes;
        private View mView;


        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.civImgPerfil);
            txtNombre = view.findViewById(R.id.tvAlia);
            txtUltimoMensaje = view.findViewById(R.id.tvUltimoMensaje);
            txtMsjNoLeido = view.findViewById(R.id.tvMensajeNoLeido);
            vistaContadorMensajes = view.findViewById(R.id.flNoLeido);

            mUserProvider = new userProvider();
            mAuth = FirebaseAuth.getInstance();
            idUsuario = mAuth.getCurrentUser().getUid();
            mChatProvider = new chatProvider();
            mMsjProvider = new mensajeProvider();
        }
    }

}
