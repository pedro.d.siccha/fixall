package com.inforad.fixall.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.model.HistorialBusqueda;
import com.inforad.fixall.model.Iso;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class historialAdaptado extends FirebaseRecyclerAdapter<HistorialBusqueda, historialAdaptado.ViewHolder> {
    private Context mContext;

    public historialAdaptado(@NonNull FirebaseRecyclerOptions<HistorialBusqueda> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull HistorialBusqueda historialModel) {
        String img = historialModel.getImgUsuario();
        Picasso.with(mContext).load(img).into(holder.imgEmp);
        holder.txtComentario.setText(historialModel.getDescripcion());
        holder.txtAlias.setText(historialModel.getAliasProfesional());

        if (historialModel.getValoracion().equals("LIKE")){
            holder.like.setVisibility(View.VISIBLE);
            holder.dislike.setVisibility(View.GONE);
        }else {
            holder.like.setVisibility(View.GONE);
            holder.dislike.setVisibility(View.VISIBLE);
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comentario, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgEmp;
        private TextView txtComentario, txtAlias;
        private ImageView like, dislike;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            imgEmp = view.findViewById(R.id.civFoto);
            txtComentario = view.findViewById(R.id.tvComentario);
            like = view.findViewById(R.id.ivLike);
            dislike = view.findViewById(R.id.ivDislike);
            txtAlias = view.findViewById(R.id.tvProfesional);

            like.setVisibility(View.GONE);
            dislike.setVisibility(View.GONE);
        }
    }
}
