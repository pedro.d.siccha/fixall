package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.model.EstudioProfesional;
import com.inforad.fixall.pantallasDetalles.detalleEstudios;

public class estudioAdaptador extends FirebaseRecyclerAdapter<EstudioProfesional, estudioAdaptador.ViewHolder> {

    private Context mContext;

    public estudioAdaptador(@NonNull FirebaseRecyclerOptions<EstudioProfesional> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull EstudioProfesional estudioModel) {
        holder.txtEstudios.setText(estudioModel.getNombreColegio());
        holder.txtInstitucios.setText(estudioModel.getGrado());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, detalleEstudios.class);
                intent.putExtra("id", estudioModel.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_carrera, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtEstudios, txtInstitucios;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            txtEstudios = view.findViewById(R.id.tvProfesion);
            txtInstitucios = view.findViewById(R.id.tvCentroEstudios);
        }
    }

}
