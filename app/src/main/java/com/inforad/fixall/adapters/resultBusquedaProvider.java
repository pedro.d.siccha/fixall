package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.viewPerfilPro;
import com.inforad.fixall.model.PalabraClave;

public class resultBusquedaProvider extends FirebaseRecyclerAdapter<PalabraClave, resultBusquedaProvider.ViewHolder> {
    private Context mContext;

    public resultBusquedaProvider(@NonNull FirebaseRecyclerOptions<PalabraClave> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull PalabraClave palabraClaveModel) {
        String idProfesional = palabraClaveModel.getIdProfesional();


        holder.txtAlias.setText(palabraClaveModel.getAliasProfesional());

        holder.btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Id de busqueda", Toast.LENGTH_SHORT).show();
            }
        });

        holder.btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, viewPerfilPro.class);
                intent.putExtra("idProfesional", idProfesional);
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_restbusquedapc, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgPerfil;
        private TextView txtAlias;
        private Button btnAceptar, btnPerfil;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.ivPerfilPro);
            txtAlias = view.findViewById(R.id.tvAliasPro);
            btnAceptar = view.findViewById(R.id.bAceptar);
            btnPerfil = view.findViewById(R.id.bPerfil);
        }
    }
}
