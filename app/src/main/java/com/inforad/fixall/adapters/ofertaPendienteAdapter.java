package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.fixall.R;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.UsuarioPalabra;
import com.inforad.fixall.profesional.viewRecibirOferta;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ofertaPendienteAdapter extends FirebaseRecyclerAdapter<Busqueda, ofertaPendienteAdapter.ViewHolder> {
    private Context mContext;

    public ofertaPendienteAdapter(@NonNull FirebaseRecyclerOptions<Busqueda> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Busqueda busquedaModel) {
        String img = busquedaModel.getImagenDescripcion();
        Picasso.with(mContext).load(img).into(holder.imgPerfil);
        holder.txtPalabra.setText(busquedaModel.getPalabraClave());
        holder.txtDistancia.setText(busquedaModel.getRangoBusqueda());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, viewRecibirOferta.class);
                intent.putExtra("idBusqueda", busquedaModel.getId());
                intent.putExtra("palabraClave", busquedaModel.getPalabraClave());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_oferta_pendiente, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private TextView txtPalabra, txtDistancia;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            txtPalabra = view.findViewById(R.id.tvPalabra);
            txtDistancia = view.findViewById(R.id.tvDistancia);
            imgPerfil = view.findViewById(R.id.civPerfil);

        }
    }

}
