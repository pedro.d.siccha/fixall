package com.inforad.fixall.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.model.PalabraClave;
import com.inforad.fixall.model.UsuarioPalabra;

public class palabraAdaptado extends FirebaseRecyclerAdapter<UsuarioPalabra, palabraAdaptado.ViewHolder> {
    private Context mContext;

    public palabraAdaptado(@NonNull FirebaseRecyclerOptions<UsuarioPalabra> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull UsuarioPalabra palabraModel) {
        holder.txtPalabra.setText(palabraModel.getId());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_palabraclave, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtPalabra;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            txtPalabra = view.findViewById(R.id.tvPalabraClave);

        }

    }
}
