package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.viewAceptarProfesionalE;
import com.inforad.fixall.empresario.viewPerfilPro;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.provider.userProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ofertaRecibidaAdaptador extends FirebaseRecyclerAdapter<Oferta, ofertaRecibidaAdaptador.ViewHolder> {
    private Context mContext;

    public ofertaRecibidaAdaptador(@NonNull FirebaseRecyclerOptions<Oferta> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Oferta ofertaModel) {
        holder.mUserProvider.obtenerUsuario(ofertaModel.getIdProfesional()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("imagen").getValue().toString();
                    Picasso.with(mContext).load(img).into(holder.imgPerfilPro);
                    holder.txtProfesional.setText(snapshot.child("alias").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        holder.txtCosto.setText("S/. " + String.valueOf(ofertaModel.getCosto()));
        holder.txtDistancia.setText("Distamcia: " + ofertaModel.getDistancia());
        holder.btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, viewPerfilPro.class);
                intent.putExtra("idProfesional", ofertaModel.getIdProfesional());
                mContext.startActivity(intent);
            }
        });
        holder.btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, viewAceptarProfesionalE.class);
                intent.putExtra("idBusqueda", ofertaModel.getIdBusqueda());
                intent.putExtra("idProfesional", ofertaModel.getIdProfesional());
                intent.putExtra("palabraClave", ofertaModel.getPalabraClave());
                intent.putExtra("costo", String.valueOf(ofertaModel.getCosto()));
                intent.putExtra("distancia", ofertaModel.getDistancia());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ofertarecibida, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfilPro;
        private TextView txtProfesional, txtCosto, txtDistancia;
        private Button btnAceptar, btnPerfil;
        private userProvider mUserProvider;
        private FloatingActionButton btnEstrellita;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;

            imgPerfilPro = view.findViewById(R.id.civImagen);
            txtProfesional = view.findViewById(R.id.tvProfesional);
            txtCosto = view.findViewById(R.id.tvCostoOferta);
            btnAceptar = view.findViewById(R.id.bAceptar);
            btnPerfil = view.findViewById(R.id.bPerfil);
            txtDistancia = view.findViewById(R.id.tvDistancia);
            btnEstrellita = view.findViewById(R.id.fabEstrella);

            mUserProvider = new userProvider();

        }

    }

}
