package com.inforad.fixall.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.model.ProfesionProfesional;

public class profesionAdaptador extends FirebaseRecyclerAdapter<ProfesionProfesional, profesionAdaptador.ViewHolder> {
    private Context mContext;

    public profesionAdaptador(@NonNull FirebaseRecyclerOptions<ProfesionProfesional> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull ProfesionProfesional profesionModel) {
        //holder.txtUniversidad.setText(profesionModel.getNombreUniversidad());
        holder.txtEspecialidad.setText(profesionModel.getGrado());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profesion, parent,false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgProfesion;
        private TextView txtEspecialidad, txtUniversidad;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            //imgProfesion = view.findViewById(R.id.ivImgUniversidad);
            txtEspecialidad = view.findViewById(R.id.tvEspecialidad);
            //txtUniversidad = view.findViewById(R.id.tvUniversidad);
        }
    }

}
