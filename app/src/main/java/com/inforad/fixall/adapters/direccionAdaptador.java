package com.inforad.fixall.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.model.Direccion;
import com.inforad.fixall.model.EstudioProfesional;

public class direccionAdaptador extends FirebaseRecyclerAdapter<Direccion, direccionAdaptador.ViewHolder> {

    private Context mContext;

    public direccionAdaptador(@NonNull FirebaseRecyclerOptions<Direccion> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Direccion direccionModel) {
        holder.txtDireccion.setText(direccionModel.getCiudad());
        holder.txtTipo.setText(direccionModel.getNombre());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_direccion, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtDireccion, txtTipo;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            txtDireccion = view.findViewById(R.id.tvDireccion);
            txtTipo = view.findViewById(R.id.tvTipo);
        }
    }

}
