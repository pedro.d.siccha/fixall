package com.inforad.fixall.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.model.Chat;
import com.inforad.fixall.model.Mensaje;
import com.inforad.fixall.provider.chatProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.utilidades.RelativeTime;

import de.hdodenhof.circleimageview.CircleImageView;

public class chatAdaptador extends FirebaseRecyclerAdapter<Mensaje, chatAdaptador.ViewHolder> {
    private Context mContext;

    public chatAdaptador(@NonNull FirebaseRecyclerOptions<Mensaje> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Mensaje mensajeModel) {

        final String mensajeId = mensajeModel.getId();
        holder.txtChat.setText(mensajeModel.getMensaje());
        String relativeTime = RelativeTime.getTimeAgo(mensajeModel.getTimestamp(), mContext);
        holder.txtDate.setText(relativeTime);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ofertarecibida, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        //private AppCompatImageView foto;
        private TextView txtChat, txtDate;
        private ImageView imgVisto;
        private FirebaseAuth mAut;
        private String idUsuario;
        private View mView;
        private chatProvider mChatProvider;

        public ViewHolder(View view){
            super(view);
            mView = view;
            /*
            txtDate = view.findViewById(R.id.tvDateMensaje);
            txtChat = view.findViewById(R.id.tvMensaje);
            imgVisto = view.findViewById(R.id.ivVisto);
            mAut = FirebaseAuth.getInstance();
            idUsuario = mAut.getCurrentUser().getUid();
             */

        }
    }
}
