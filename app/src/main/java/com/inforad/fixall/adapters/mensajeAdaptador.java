package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.chat.viewImganeChat;
import com.inforad.fixall.model.Mensaje;
import com.inforad.fixall.provider.chatProvider;
import com.inforad.fixall.utilidades.RelativeTime;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class mensajeAdaptador extends FirebaseRecyclerAdapter<Mensaje, mensajeAdaptador.ViewHolder> {
    private Context mContext;

    public mensajeAdaptador(@NonNull FirebaseRecyclerOptions<Mensaje> options, Context mContext) {
        super(options);
        this.mContext = mContext;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Mensaje mensajeModel) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String idUsuario = mAuth.getCurrentUser().getUid();
        holder.txtMensaje.setText(mensajeModel.getMensaje());
        String time = RelativeTime.getTimeAgo(mensajeModel.getTimestamp(), mContext);
        holder.txtFecha.setText(time);
        if (mensajeModel.getImagen() != null){
            holder.imgImagen.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(mensajeModel.getImagen()).into(holder.imgImagen);
            holder.imgImagen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, viewImganeChat.class);
                    intent.putExtra("urlImg", mensajeModel.getImagen());
                    mContext.startActivity(intent);
                }
            });
        }else {
            holder.imgImagen.setVisibility(View.GONE);
        }

        if (mensajeModel.getIdEnvia().equals(idUsuario)){
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.setMargins(150,0,0,0);
            holder.LLmensaje.setLayoutParams(params);
            holder.LLmensaje.setPadding(30, 20, 25, 20);
            holder.LLmensaje.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_linear_layout));
            holder.imgVisto.setVisibility(View.VISIBLE);
        }else{
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.setMargins(0,0,150,0);
            holder.LLmensaje.setLayoutParams(params);
            holder.LLmensaje.setPadding(30, 20, -40, 20);
            holder.LLmensaje.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_linear_layout_grey));
            holder.imgVisto.setVisibility(View.INVISIBLE);
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mensaje, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgVisto, imgImagen;
        private TextView txtMensaje, txtFecha;
        private LinearLayout LLmensaje;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            imgVisto = view.findViewById(R.id.ivVisto);
            imgImagen = view.findViewById(R.id.ivImagen);
            txtMensaje = view.findViewById(R.id.tvMensaje);
            txtFecha = view.findViewById(R.id.tvFecha);
            LLmensaje = view.findViewById(R.id.llMensaje);
        }
    }

}
