package com.inforad.fixall.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.R;
import com.inforad.fixall.model.Buscar;
import com.inforad.fixall.provider.buscarProvider;
import com.inforad.fixall.utilidades.RelativeTime;
import com.squareup.picasso.Picasso;

public class buscarAdaptador extends FirebaseRecyclerAdapter<Buscar, buscarAdaptador.ViewHolder> {
    private Context mContext;

    public buscarAdaptador(@NonNull FirebaseRecyclerOptions<Buscar> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Buscar buscarModel) {
        String img = buscarModel.getImgUsuario();
        Picasso.with(mContext).load(img).into(holder.imgPerfil);
        holder.txtCliente.setText(buscarModel.getAliasUsuario());
        holder.txtEstado.setText(buscarModel.getEstado());
        String time = RelativeTime.getTimeAgo(buscarModel.getTimestamp(), mContext);
        holder.txtTiempo.setText(time);
        if (time.equals("Hace 15 minutos")){
            holder.mBuscarProvider.actualizarEstado(buscarModel.getIdUsuario(), buscarModel.getId()).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_busquedas_pendientes, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgPerfil;
        private TextView txtCliente, txtEstado, txtTiempo;
        private FirebaseAuth mAuth;
        private buscarProvider mBuscarProvider;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.civPerfil);
            txtCliente = view.findViewById(R.id.tvEmpresario);
            txtEstado = view.findViewById(R.id.tvEstado);
            txtTiempo = view.findViewById(R.id.tvTiempo);
            mAuth = FirebaseAuth.getInstance();

            mBuscarProvider = new buscarProvider();
        }

    }
}
