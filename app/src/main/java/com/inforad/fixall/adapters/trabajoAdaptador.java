package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.model.Trabajo;
import com.inforad.fixall.pantallasDetalles.detalleTrabajo;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class trabajoAdaptador extends FirebaseRecyclerAdapter<Trabajo, trabajoAdaptador.ViewHolder> {
    private Context mContext;

    public trabajoAdaptador(@NonNull FirebaseRecyclerOptions<Trabajo> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Trabajo trabajoModel) {
        String img = trabajoModel.getImagen();
        holder.txtCentroTrabajo.setText(trabajoModel.getNomEmpresa());
        holder.txtDescripcion.setText(trabajoModel.getDescripcion());
        Picasso.with(mContext).load(img).into(holder.imgTrabajo);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, detalleTrabajo.class);
                intent.putExtra("idTrabajo", trabajoModel.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_reflaboral, parent,false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgTrabajo;
        private TextView txtCentroTrabajo, txtDescripcion;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;

            imgTrabajo = view.findViewById(R.id.civFoto);
            txtCentroTrabajo = view.findViewById(R.id.tvLugar);
            txtDescripcion = view.findViewById(R.id.tvDescripcion);

        }

    }
}
