package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.viewAceptarProfesionalE;
import com.inforad.fixall.empresario.viewPerfilPro;
import com.inforad.fixall.model.Invitacion;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.model.PalabraClave;
import com.inforad.fixall.provider.busquedaProvider;
import com.inforad.fixall.provider.invitacionProvider;
import com.inforad.fixall.provider.userProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ofertaEnviadaAdaptador extends FirebaseRecyclerAdapter<PalabraClave, ofertaEnviadaAdaptador.ViewHolder> {
    private Context mContext;

    public ofertaEnviadaAdaptador(@NonNull FirebaseRecyclerOptions<PalabraClave> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull PalabraClave palabraModel) {

        String img = palabraModel.getImgProfesional();
        holder.txtAlias.setText(palabraModel.getAliasProfesional());
        Picasso.with(mContext).load(img).into(holder.imgPerfil);

        holder.mBusquedaProvider.obtenerBusqueda(palabraModel.getPalabra(), holder.idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    holder.descripcion = snapshot.child("descripcion").getValue().toString();
                    holder.imgDescripcion = snapshot.child("imagenDescripcion").getValue().toString();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        holder.btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.mUserProvider.obtenerUsuario(holder.idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()){
                            holder.mInvitacion = new Invitacion(holder.idUsuario, snapshot.child("imagen").getValue().toString(), snapshot.child("alias").getValue().toString(), holder.idUsuario, holder.descripcion, holder.imgDescripcion, palabraModel.getPalabra(), palabraModel.getIdProfesional(), palabraModel.getAliasProfesional(), palabraModel.getImgProfesional(), "PENDIENTE");
                            holder.mInvitacionProvider.enviarInvitacion(holder.mInvitacion, "Invitacion_Empresario", holder.idUsuario, palabraModel.getIdProfesional()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    holder.mInvitacionProvider.enviarInvitacion(holder.mInvitacion, "Invitacion_Profesional", palabraModel.getIdProfesional(), holder.idUsuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            holder.mBusquedaProvider.actualizarBusqueda(palabraModel.getPalabra(), holder.idUsuario, "", "", "", "", 0.00, "INVITACION", "fecha").addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Toast.makeText(mContext, "Invitación registrada", Toast.LENGTH_SHORT).show();
                                                    /*
                                                    Intent intent = new Intent(mContext, viewAceptarProfesionalE.class);
                                                    //intent.putExtra("idBusqueda", palabraModel.getIdBusqueda());
                                                    intent.putExtra("idProfesional", palabraModel.getIdProfesional());
                                                    intent.putExtra("palabraClave", palabraModel.getPalabra());
                                                    //intent.putExtra("costo", String.valueOf(palabraModel.getCosto()));
                                                    mContext.startActivity(intent);
                                                     */
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                    }
                });
            }
        });

        holder.btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, viewPerfilPro.class);
                intent.putExtra("idProfesional", palabraModel.getIdProfesional());
                mContext.startActivity(intent);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listaresultados, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private TextView txtAlias;
        private Button btnAceptar, btnPerfil;
        private invitacionProvider mInvitacionProvider;
        private Invitacion mInvitacion;
        private busquedaProvider mBusquedaProvider;
        private FirebaseAuth mAuth;
        private String idUsuario, descripcion, imgDescripcion;
        private userProvider mUserProvider;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.ivPerfilPro);
            txtAlias = view.findViewById(R.id.tvAliasPros);
            btnAceptar = view.findViewById(R.id.bAceptar);
            btnPerfil = view.findViewById(R.id.bPerfil);

            mBusquedaProvider = new busquedaProvider();
            mInvitacionProvider = new invitacionProvider();
            mUserProvider = new userProvider();
            mAuth = FirebaseAuth.getInstance();

            idUsuario = mAuth.getCurrentUser().getUid();

        }
    }
}
