package com.inforad.fixall.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.model.Iso;
import com.inforad.fixall.pantallasDetalles.detalleIso;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class isoAdaptador extends FirebaseRecyclerAdapter<Iso, isoAdaptador.ViewHolder> {
    private Context mContext;

    public isoAdaptador(@NonNull FirebaseRecyclerOptions<Iso> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Iso isoModel) {
        String img = isoModel.getImagen();
        Picasso.with(mContext).load(img).into(holder.imgIso);
        holder.txtPalabra.setText(isoModel.getCodigo());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, detalleIso.class);
                intent.putExtra("idIso", isoModel.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_iso, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgIso;
        private TextView txtPalabra;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            imgIso = view.findViewById(R.id.civFoto);
            txtPalabra = view.findViewById(R.id.tvIso);
        }
    }

}
