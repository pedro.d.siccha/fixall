package com.inforad.fixall.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.model.User;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class solicitudesAdaptador extends FirebaseRecyclerAdapter<User, solicitudesAdaptador.ViewHolder> {
    private Context mContext;

    public solicitudesAdaptador(@NonNull FirebaseRecyclerOptions<User> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull User usuarioModel) {
        holder.txtNombre.setText(usuarioModel.getAlias());
        holder.txtTelefono.setText(usuarioModel.getTelefono());
        String img = usuarioModel.getImagen();
        Picasso.with(mContext).load(img).into(holder.imgPerfil);
        holder.btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        holder.btnRechasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private AppCompatImageButton btnRechasar, btnAceptar;
        private TextView txtNombre, txtTelefono;
        private View mView;


        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.civFoto);
            btnRechasar = view.findViewById(R.id.bRechasar);
            btnAceptar = view.findViewById(R.id.bAceptar);
            txtNombre = view.findViewById(R.id.tvNombre);
            txtTelefono = view.findViewById(R.id.tvNumTelefono);
        }
    }

}
