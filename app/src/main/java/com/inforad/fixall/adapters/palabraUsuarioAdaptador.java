package com.inforad.fixall.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.fixall.R;
import com.inforad.fixall.model.Busqueda;
import com.inforad.fixall.model.UsuarioPalabra;
import com.inforad.fixall.profesional.viewCitaPendiente;

public class palabraUsuarioAdaptador extends FirebaseRecyclerAdapter<UsuarioPalabra, palabraUsuarioAdaptador.ViewHolder> {
    private Context mContext;

    public palabraUsuarioAdaptador(@NonNull FirebaseRecyclerOptions<UsuarioPalabra> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull UsuarioPalabra palabraModel) {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        holder.recyclerListaOferta.setLayoutManager(linearLayoutManager);
        //Toast.makeText(mContext, "Ver: " + palabraModel.getId(), Toast.LENGTH_SHORT).show();
        Query query = holder.mDatabase.child("Busqueda").child(palabraModel.getId()).orderByChild("estado").equalTo("PENDIENTE");
        //Query query = holder.mDatabase.child("Busqueda").child(palabraModel.getId()).child(palabraModel.getId());
        FirebaseRecyclerOptions<Busqueda> options = new FirebaseRecyclerOptions.Builder<Busqueda>().setQuery(query, Busqueda.class).build();
        holder.mOfertaAdapter = new ofertaPendienteAdapter(options, mContext);
        holder.recyclerListaOferta.setAdapter(holder.mOfertaAdapter);
        holder.mOfertaAdapter.startListening();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_oferta_pendiente, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private View mView;
        private RecyclerView recyclerListaOferta;
        private DatabaseReference mDatabase;
        private FirebaseAuth mAuth;
        private String idUsuario;
        private ofertaPendienteAdapter mOfertaAdapter;

        public ViewHolder(View view){
            super(view);
            mView = view;
            recyclerListaOferta = view.findViewById(R.id.rvListaOferta);

            mDatabase = FirebaseDatabase.getInstance().getReference();
            mAuth = FirebaseAuth.getInstance();

        }
    }
}
