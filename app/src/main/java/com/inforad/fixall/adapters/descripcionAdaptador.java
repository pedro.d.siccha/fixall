package com.inforad.fixall.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.inforad.fixall.R;
import com.inforad.fixall.model.Descripcion;

public class descripcionAdaptador extends FirebaseRecyclerAdapter<Descripcion, descripcionAdaptador.ViewHolder> {
    private Context mContext;

    public descripcionAdaptador(@NonNull FirebaseRecyclerOptions<Descripcion> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Descripcion descripcionModel) {
        holder.txtDescripcion.setText(descripcionModel.getDescripcion());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_descripcion, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtDescripcion;
        private View mView;

        public ViewHolder(View view){
            super(view);
            mView = view;
            txtDescripcion = view.findViewById(R.id.tvDescripcion);
        }
    }

}
