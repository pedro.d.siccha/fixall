package com.inforad.fixall.procesos;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class GeofireProvider {

    private DatabaseReference mDataBase;
    private GeoFire mGeoFire;

    public GeofireProvider(){
        mDataBase = FirebaseDatabase.getInstance().getReference().child("direccion");
        mGeoFire = new GeoFire(mDataBase);
    }

    public void saveLocation(String idUsuario, LatLng LatLng){
        mGeoFire.setLocation(idUsuario, new GeoLocation(LatLng.latitude, LatLng.longitude));
    }

    public void removeLocation(String idUsuario){
        mGeoFire.removeLocation(idUsuario);
    }

}
