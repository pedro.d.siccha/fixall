package com.inforad.fixall.procesos;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.R;
import com.inforad.fixall.empresario.clubEmpresario;
import com.inforad.fixall.empresario.panelcontrolEmpresario;
import com.inforad.fixall.empresario.viewHistprial;
import com.inforad.fixall.empresario.viewListaProfesionalesLlego;
import com.inforad.fixall.empresario.viewOfertasRecibidas;
import com.inforad.fixall.empresario.viewPerfilEmpresario;
import com.inforad.fixall.model.Direccion;
import com.inforad.fixall.model.Oferta;
import com.inforad.fixall.profesional.viewListaContacto;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.profesional.viewPerfilProfesional;
import com.inforad.fixall.provider.direccionProvider;
import com.inforad.fixall.provider.ofertaProvider;
import com.inforad.fixall.provider.planProvider;
import com.inforad.fixall.provider.userProvider;
import com.inforad.fixall.viewBilletera;
import com.inforad.fixall.viewConfigDirecciones;
import com.inforad.fixall.viewSubscripcion;
import com.inforad.fixall.viewTipoUsuario;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class procesador {

    private FirebaseAuth mAuth;
    private String idUsuario = "";
    private DatabaseReference mDatabase;
    private userProvider mUserProvider;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private planProvider mPlanProvider;
    private direccionProvider mDireccionProvider;
    private ofertaProvider mOfertaProvider;

    public procesador() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider = new userProvider();
        mPlanProvider = new planProvider();
        mDireccionProvider = new direccionProvider();
        mOfertaProvider = new ofertaProvider();
    }

    public String obtenerId(){
        return mAuth.getInstance().getCurrentUser().getUid();
    }

    public boolean existeSesion(){
        boolean existe = false;

        if (mAuth.getCurrentUser() != null){
            existe = true;
        }
        return existe;
    }

    public String obtenerCodigo(String dni){
        String codigo = dni.substring(dni.length() - 6);
        return codigo;
    }

    public void caracteristicasPlanConstraint(ConstraintLayout fondo, Context mContext) {
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String plan = snapshot.child("plan").getValue().toString();
                    switch (plan){
                        case "BASICO":
                            bitmap = BitmapFactory.decodeResource(fondo.getResources(), R.drawable.fondo);
                            background = new BitmapDrawable(fondo.getResources(), bitmap);
                            fondo.setBackground(background);
                            break;
                        case "GOLD":
                            bitmap = BitmapFactory.decodeResource(fondo.getResources(), R.drawable.fondogold);
                            background = new BitmapDrawable(fondo.getResources(), bitmap);
                            fondo.setBackground(background);
                            break;
                        case "BLACK":
                            bitmap = BitmapFactory.decodeResource(fondo.getResources(), R.drawable.fondoblack);
                            background = new BitmapDrawable(fondo.getResources(), bitmap);
                            fondo.setBackground(background);
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(mContext, "Error de plan ConstraintLayout: " + error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void caracteristicasPlanDrawer(DrawerLayout fondo, Context mContext) {

        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String plan = snapshot.child("plan").getValue().toString();
                    switch (plan){
                        case "BASICO":
                            bitmap = BitmapFactory.decodeResource(fondo.getResources(), R.drawable.fondo);
                            background = new BitmapDrawable(fondo.getResources(), bitmap);
                            fondo.setBackground(background);
                            break;
                        case "GOLD":
                            bitmap = BitmapFactory.decodeResource(fondo.getResources(), R.drawable.fondogold);
                            background = new BitmapDrawable(fondo.getResources(), bitmap);
                            fondo.setBackground(background);
                            break;
                        case "BLACK":
                            bitmap = BitmapFactory.decodeResource(fondo.getResources(), R.drawable.fondoblack);
                            background = new BitmapDrawable(fondo.getResources(), bitmap);
                            fondo.setBackground(background);
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(mContext, "Error de plan DrawerLayout: " + error, Toast.LENGTH_SHORT).show();
            }
        });
    }



    public String fecActual(){
        String fecha = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        return fecha;
    }

    public String fecUnMes(){
        String fecActual = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Date dt = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.add(Calendar.MONTH, 1);
        String fecFinal = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        return fecFinal;
    }

    public void carcateristicaMenu(NavigationView menu){
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String plan = snapshot.child("plan").getValue().toString();

                    switch (plan){
                        case "BASICO":
                            menu.getMenu().findItem(R.id.nav_club).setVisible(false);
                            menu.getMenu().findItem(R.id.nav_favoritos).setVisible(false);
                            menu.getMenu().findItem(R.id.nav_banco).setVisible(false);
                            break;
                        case "GOLD":
                            menu.getMenu().findItem(R.id.nav_club).setTitle("Club Gold");
                            menu.getMenu().findItem(R.id.nav_club).setVisible(true);
                            menu.getMenu().findItem(R.id.nav_favoritos).setVisible(false);
                            menu.getMenu().findItem(R.id.nav_banco).setVisible(false);
                            calcCantidadClub(menu);
                            break;
                        case "BLACK":
                            menu.getMenu().findItem(R.id.nav_club).setTitle("Club Black");
                            menu.getMenu().findItem(R.id.nav_club).setVisible(true);
                            menu.getMenu().findItem(R.id.nav_favoritos).setVisible(true);
                            menu.getMenu().findItem(R.id.nav_banco).setVisible(true);
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void carcateristicaMenuPro(NavigationView menu){
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String plan = snapshot.child("plan").getValue().toString();

                    switch (plan){
                        case "BASICO":
                            menu.getMenu().findItem(R.id.nav_club).setVisible(false);
                            break;
                        case "GOLD":
                            menu.getMenu().findItem(R.id.nav_club).setTitle("Club Gold");
                            menu.getMenu().findItem(R.id.nav_club).setVisible(true);
                            calcCantidadClub(menu);
                            break;
                        case "BLACK":
                            menu.getMenu().findItem(R.id.nav_club).setTitle("Club Black");
                            menu.getMenu().findItem(R.id.nav_club).setVisible(true);
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void calcCantidadClub(NavigationView menu){
        mPlanProvider.obtenerPlan(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    int accesos = Integer.parseInt(snapshot.child("club").getValue().toString());
                    if ( accesos > 0 ){
                        menu.getMenu().findItem(R.id.nav_club).setTitle("Club Gold");
                        menu.getMenu().findItem(R.id.nav_club).setVisible(true);
                    }else {
                        menu.getMenu().findItem(R.id.nav_club).setTitle("Club Gold");
                        menu.getMenu().findItem(R.id.nav_club).setVisible(false);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void accesoClub(Context mContext){
        mPlanProvider.obtenerPlan(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    int acceso = Integer.parseInt(snapshot.child("club").getValue().toString());
                    int nuevoAcceso = acceso - 1;
                    mPlanProvider.actualizarAcceso(idUsuario, nuevoAcceso).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(mContext, "Acceso actualizado", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void caracteristicasBono(Button btnBono, Button btnPrecio, double precio){

        mPlanProvider.obtenerPlan(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){

                    double Bono = Double.parseDouble(snapshot.child("bono").getValue().toString());
                    int Cantidad = Integer.parseInt(snapshot.child("cantBono").getValue().toString());
                    if (Cantidad > 6){
                        btnBono.setVisibility(View.VISIBLE);

                        if (btnBono.getText().toString().equals("Aplicar Bono")){

                            double nuevoBono = Bono*precio;
                            btnPrecio.setText(String.valueOf(nuevoBono));

                        }else {
                            btnBono.setText("Quitar Bono");
                            btnPrecio.setText(String.valueOf(precio));
                        }

                    }else {
                        btnBono.setVisibility(View.GONE);
                        btnPrecio.setText(String.valueOf(precio));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void verBanco(CheckBox banco){
        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    if (Integer.parseInt(snapshot.child("banco").getValue().toString()) == 1 ){
                        banco.isChecked();
                    }else {

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void mostrarComboDireccion(Spinner combo, Context mContext){
        final List<Direccion> direccion = new ArrayList<>();
        mDireccionProvider.obtenerDireccion(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    for (DataSnapshot ds: snapshot.getChildren()){
                        String id = ds.getKey();
                        String nomDir = ds.child("ciudad").getValue().toString();
                        String nomCiudad = ds.child("region").getValue().toString();
                        String nomPais = ds.child("pais").getValue().toString();
                        String nombre = ds.child("nombre").getValue().toString();
                        String estado = ds.child("estado").getValue().toString();
                        String nivel = ds.child("nivel").getValue().toString();
                        double dirLat = Double.parseDouble(ds.child("dirLat").getValue().toString());
                        double dirLong = Double.parseDouble(ds.child("dirLong").getValue().toString());
                        direccion.add(new Direccion(id, nomDir, nomCiudad, nomPais, nombre, estado, nivel, dirLat, dirLong));
                    }
                    ArrayAdapter<Direccion> arrayAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_dropdown_item_1line, direccion);
                    combo.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void mostrarComboOfertas(Spinner combo, Context mContext){
        final List<Oferta> oferta = new ArrayList<>();
        mOfertaProvider.obtenerOferta(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    for (DataSnapshot ds: snapshot.getChildren()){
                        String id = ds.getKey();
                        String palabraClave = ds.child("palabraClave").getValue().toString();
                        String idProfesional = ds.child("idProfesional").getValue().toString();
                        String alias = "";
                        String imgProfesional = "";
                        String idBusqueda = ds.child("idBusqueda").getValue().toString();
                        String descripcion = ds.child("descripcion").getValue().toString();
                        String estado = ds.child("estado").getValue().toString();
                        String distancia = ds.child("distancia").getValue().toString();
                        double costo = Double.valueOf(ds.child("costo").getValue().toString());
                        oferta.add(new Oferta(id, palabraClave, idProfesional, alias, imgProfesional, idBusqueda, descripcion, estado, distancia, costo, new Date().getTime()));
                    }
                    ArrayAdapter<Oferta> arrayAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_dropdown_item_1line, oferta);
                    combo.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}
