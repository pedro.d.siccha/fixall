package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.model.FCMBody;
import com.inforad.fixall.model.FCMResponse;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.notificacionProvider;
import com.inforad.fixall.provider.tokenProvider;
import com.inforad.fixall.provider.userProvider;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class viewVerifNumTelefono extends AppCompatActivity {

    private PinView pinUsuario;
    private TextView txtNumTelefono;
    private Button btnSiguiente;
    private String nombre, apellido, alias, numTelefono, codigoSistema, codigo;
    private userProvider mUserProvider;
    private tokenProvider mToken;
    private notificacionProvider mNotificacionProvider;
    private FirebaseAuth mAuth;
    private procesador mProceso;
    private ConstraintLayout Fondo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_verif_num_telefono);
        Iniciar();

        mProceso.caracteristicasPlanConstraint(Fondo, viewVerifNumTelefono.this);

        txtNumTelefono.setText("Ingrese el número de verificacíon enviado al teléfono " + numTelefono);

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pinUsuario.getText().toString().equals(codigo)){
                    Intent intent = new Intent(viewVerifNumTelefono.this, seleccionarAvatar.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else {
                    Toast.makeText(viewVerifNumTelefono.this, "Código de verificación equivocado", Toast.LENGTH_SHORT).show();
                    pinUsuario.setText("");
                    pinUsuario.setFocusable(true);
                }
            }
        });
    }

    private void Iniciar() {
        mAuth = FirebaseAuth.getInstance();
        pinUsuario = findViewById(R.id.pinVerificacion);
        txtNumTelefono = findViewById(R.id.tvNumTelefono);
        btnSiguiente = findViewById(R.id.btnNextCodVerificacion);
        Fondo = findViewById(R.id.clFondo);
        obtenerDatos();
        mUserProvider = new userProvider();
        mToken = new tokenProvider();
        mNotificacionProvider = new notificacionProvider();
        mProceso = new procesador();
        mUserProvider.obtenerUsuario(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    codigo = mProceso.obtenerCodigo(snapshot.child("dni").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void obtenerDatos() {
        nombre = getIntent().getStringExtra("nombre");
        apellido = getIntent().getStringExtra("apellido");
        alias = getIntent().getStringExtra("alias");
        numTelefono = getIntent().getStringExtra("numero");
    }
}