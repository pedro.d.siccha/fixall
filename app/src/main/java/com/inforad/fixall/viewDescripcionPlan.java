package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.fixall.model.Plan;
import com.inforad.fixall.procesos.procesador;
import com.inforad.fixall.provider.planProvider;
import com.inforad.fixall.provider.userProvider;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class viewDescripcionPlan extends AppCompatActivity {

    private CircleImageView imgPlan;
    private TextView txtPlan, txtDescripion;
    private Button btnPagar;
    private String plan, direccion, idUsuario;
    private Bitmap bitmap;
    private BitmapDrawable background;
    private DrawerLayout drawerLayout;
    private userProvider mUserProvider;
    private FirebaseAuth mAuth;
    private procesador mProcesador;
    private planProvider mPlanProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_descripcion_plan);
        Iniciar();
        mostrar();
        mProcesador.caracteristicasPlanDrawer(drawerLayout, viewDescripcionPlan.this);

        btnPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                destacar();
            }
        });
    }

    private void Iniciar() {
        drawerLayout = findViewById(R.id.dlPanelPro);
        imgPlan = findViewById(R.id.civIconoPlan);
        txtPlan = findViewById(R.id.tvPlan);
        txtDescripion = findViewById(R.id.tvDescripcion);
        btnPagar = findViewById(R.id.bPagar);

        mUserProvider = new userProvider();
        mAuth = FirebaseAuth.getInstance();
        mProcesador = new procesador();
        mPlanProvider = new planProvider();

        idUsuario = mAuth.getCurrentUser().getUid();
    }

    private void mostrar(){
        plan = getIntent().getStringExtra("plan");
        direccion = getIntent().getStringExtra("direccion");

        if (plan.equals("GOLD")){
            Picasso.with(viewDescripcionPlan.this).load(R.drawable.fondogold).into(imgPlan);
            txtPlan.setText("FIX ALL GOLD");
            txtDescripion.setText("DESCRICPION FIX ALL GOLD");
            btnPagar.setText("PAGAR");

            bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.fondogold);
            background = new BitmapDrawable(getResources(), bitmap);
            drawerLayout.setBackground(background);
        }else {
            Picasso.with(viewDescripcionPlan.this).load(R.drawable.fondoblack).into(imgPlan);
            txtPlan.setText("FIX ALL BLACK");
            txtDescripion.setText("DESCRICPION FIX ALL BLACK");
            btnPagar.setText("PAGAR");

            bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.fondoblack);
            background = new BitmapDrawable(getResources(), bitmap);
            drawerLayout.setBackground(background);
        }
    }

    private void destacar(){

        mUserProvider.actualizarPlan(idUsuario, plan).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Plan nPlan = new Plan(idUsuario, plan, mProcesador.fecActual(), mProcesador.fecUnMes(), 0.00, 6, 0.05, 12, 2);
                mPlanProvider.registrarPlan(nPlan, idUsuario).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent intent = new Intent(viewDescripcionPlan.this, pagarDestacarte.class);
                        intent.putExtra("plan", plan);
                        intent.putExtra("direccion", direccion);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
            }
        });
    }
}