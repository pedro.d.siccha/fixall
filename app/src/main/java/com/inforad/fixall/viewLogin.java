package com.inforad.fixall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.firebase.ui.auth.AuthMethodPickerLayout;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.common.Scopes;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.inforad.fixall.empresario.viewPerfilEmpresario;
import com.inforad.fixall.profesional.newConfigPerfil;
import com.firebase.ui.*;
import com.inforad.fixall.profesional.viewPerfilP;
import com.inforad.fixall.provider.userProvider;

import java.util.Arrays;

public class viewLogin extends AppCompatActivity {

    private static final int RC_SING_IN = 123;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private DatabaseReference mdataBase;
    private LottieAnimationView mAnimacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_login);
        Iniciar();

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user != null){
                    String id_registro = user.getUid();

                    mdataBase.child("Users").child(id_registro).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()){
                                Toast.makeText(viewLogin.this, "Bienvenido " + user.getDisplayName(), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(viewLogin.this, viewTipoUsuario.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }else {

                                Intent intent = new Intent(viewLogin.this, viewDatosPersonales.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            //Toast.makeText(viewLogin.this, "Error: " + databaseError, Toast.LENGTH_SHORT).show();
                        }
                    });

                }else {

                   AuthUI.IdpConfig facebookidp = new AuthUI.IdpConfig.FacebookBuilder().setPermissions(Arrays.asList("user_friends", "user_gender")).build();
                   AuthUI.IdpConfig googleidp = new AuthUI.IdpConfig.GoogleBuilder().build();
                   AuthMethodPickerLayout customLayout = new AuthMethodPickerLayout.Builder(R.layout.activity_view_pregunta_acceso).setEmailButtonId(R.id.btnCorreoPa).setGoogleButtonId(R.id.btnGooglePa).setFacebookButtonId(R.id.btnFacebookPa).setTosAndPrivacyPolicyId(R.id.tvPoliticas).build();

                   startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().setIsSmartLockEnabled(false).setTosAndPrivacyPolicyUrls("http://databaseremote.esy.es/RegisterLite/html/privacidad.html", "http://databaseremote.esy.es/RegisterLite/html/privacidad.html").setAvailableProviders(Arrays.asList(new AuthUI.IdpConfig.EmailBuilder().build(), facebookidp, googleidp)).setAuthMethodPickerLayout(customLayout).build(), RC_SING_IN);

                }
            }
        };
    }

    private void Iniciar() {
        mAuth = FirebaseAuth.getInstance();

        mdataBase = FirebaseDatabase.getInstance().getReference();
        mAnimacion = findViewById(R.id.animacionCarga);
        mAnimacion.playAnimation();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RC_SING_IN){
            if (resultCode == RESULT_OK){
                //Toast.makeText(viewLogin.this,  "Bienvendio", Toast.LENGTH_LONG).show();
            }else {
                //Toast.makeText(viewLogin.this, "Algo Salio mal " + RESULT_OK, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAuthStateListener != null){
            mAuth.removeAuthStateListener(mAuthStateListener);
        }
    }
}